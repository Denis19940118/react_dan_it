import React from 'react';
import ContainerCard from '../../components/ContainerCard/ContainerCard';

const Home = props => {
	const { cards, favorite, setFavorite, basket, setBasket } = props;
	return (
		<section>
			<ContainerCard
				cards={cards}
				favorite={favorite}
				setFavorite={setFavorite}
				basket={basket}
				setBasket={setBasket}
			/>
		</section>
	);
};

export default Home;
