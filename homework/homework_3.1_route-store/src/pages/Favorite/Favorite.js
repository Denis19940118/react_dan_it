import React from 'react';
import ContainerCard from '../../components/ContainerCard/ContainerCard';

const Favorite = props => {
	const { cards, favorite, setFavorite, basket, setBasket } = props;

	// const favoriteCards = JSON.parse(localStorage.getItem('favorite'));
	// const allFavorite = favoriteCards.map(item => item.id = (
	// 	<ContainerCard card={item} basketRemove={false} />
	// ));
	// const ListToFavoriteCards = cards.filter(({ id }) => favorite.includes(id));
	const ListToFavoriteCards = [];
	cards.some(card =>
		favorite.forEach(item =>
			card.id === item.id ? ListToFavoriteCards.push(card) : ''
		)
	);

	return (
		<>
			<ContainerCard
				cards={ListToFavoriteCards}
				favorite={favorite}
				setFavorite={setFavorite}
				basket={basket}
				setBasket={setBasket}
				favoriteTrue={true}
			/>
		</>
	);
};

export default Favorite;
