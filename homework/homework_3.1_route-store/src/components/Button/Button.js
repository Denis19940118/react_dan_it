import React, { PureComponent } from 'react';
import propTypes from 'prop-types';
import './Button.scss';

const Button = props => {
	const {
		name,
		type,
		className,
		onClick,
		color,
		fontSize,
		backgroundColor,
		size,
		func,
	} = props;

	const style = {
		width: size,
		backgroundColor: backgroundColor,
		fontSize: fontSize,
		color: color,
	};

	return (
		<>
			<button className={className} type={type} style={style} onClick={onClick}>
				{name}
				{func}
			</button>
		</>
	);
};

Button.propTypes = {
	name: propTypes.string,
	type: propTypes.string,
	className: propTypes.string,
	onClick: propTypes.func,
	color: propTypes.string,
	fontSize: propTypes.number,
	backgroundColor: propTypes.string.isRequired,
	size: propTypes.number,
};

Button.defaultProps = {
	types: 'button',
	fontSize: 16,
	onClick: undefined,
	className: 'btn-default',
	backgroundColor: '',
	size: 130,
};

export default Button;
