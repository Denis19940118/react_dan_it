import axios from 'axios';
import React, { useEffect, useState } from 'react';
import {
	BrowserRouter as Router,
	Redirect,
	Route,
	Switch,
} from 'react-router-dom';
import Header from '../components/Header/Header';
import Basket from '../pages/Basket/Basket';
import Favorite from '../pages/Favorite/Favorite';
import Home from '../pages/Home/Home';

const AppRouter = () => {
	const [cardsBody, setCardBody] = useState([]);
	const [favorite, setFavorite] = useState(null);
	const [basket, setBasket] = useState(
		JSON.parse(localStorage.getItem('basket')) || null
	);

	useEffect(() => {
		axios.get('/products.json').then(res => setCardBody(res.data));

		if (JSON.parse(localStorage.getItem('favorite')) === null) {
			// localStorage.setItem('favorite', JSON.stringify([]));
			setFavorite([]);
		} else {
			setFavorite(JSON.parse(localStorage.getItem('favorite')));
		}

		if (JSON.parse(localStorage.getItem('basket')) === null) {
			// localStorage.setItem('basket', JSON.stringify([]));
			setBasket([]);
		} else {
			setBasket(JSON.parse(localStorage.getItem('basket')));
		}
	}, []);

	return (
		<div>
			{cardsBody !== null && favorite !== null && basket !== null && (
				<Router>
					<Header />
					<Switch>
						<Redirect exact from="/" to="/home" />
						<Route exact path={'/home'}>
							<Home
								cards={cardsBody}
								favorite={favorite}
								setFavorite={setFavorite}
								basket={basket}
								setBasket={setBasket}
							/>
						</Route>
						<Route exact path={'/favorite'}>
							<Favorite
								cards={cardsBody}
								favorite={favorite}
								setFavorite={setFavorite}
								basket={basket}
								setBasket={setBasket}
							/>
						</Route>
						<Route exact path={'/basket'}>
							<Basket
								cards={cardsBody}
								favorite={favorite}
								setFavorite={setFavorite}
								basket={basket}
								setBasket={setBasket}
							/>
						</Route>
					</Switch>
				</Router>
			)}
		</div>
	);
};

export default AppRouter;
