import React, { PureComponent } from 'react';

import Button from '../Button/Button';
import './NameModal.scss';
import '../Button/Button.scss';

class NameModal extends PureComponent {
	render() {
		const { title, close, ics } = this.props;
		console.log('Header -> render -> ics', ics);

		return (
			<>
				<section className="header">
					<p>{title}</p>
					{ics === true ? (
						<Button name={'X'} className="btn_close" onClick={close} />
					) : (
						''
					)}
				</section>
			</>
		);
	}

	clickClose = () => {};
}
export default NameModal;
