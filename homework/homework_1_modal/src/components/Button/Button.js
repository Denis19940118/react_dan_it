import React, { PureComponent } from 'react';
import propTypes from 'prop-types';
import './Button.scss';

class Button extends PureComponent {
	render() {
		const {
			name,
			types,
			backgroundColor,
			fontSize,
			onClick,
			className,
			size,
		} = this.props;

		const style = {
			width: size,
			backgroundColor: backgroundColor,
			fontSize: fontSize,
		};

		return (
			<>
				<button
					className={className}
					style={style}
					type={types}
					onClick={onClick}
				>
					{name}
				</button>
			</>
		);
	}
}

Button.propTypes = {
	name: propTypes.string,
	types: propTypes.string,
	backgroundColor: propTypes.string.isRequired,
	fontSize: propTypes.number,
	onClick: propTypes.func,
	className: propTypes.string,
	size: propTypes.number,
};

Button.defaultProps = {
	types: 'button',
	// backgroundColor: 'black',
	fontSize: 16,
	onClick: undefined,
	// this.close
	className: 'btn',
	size: 160,
};

export default Button;

// close = () => {
// 	const { openedOn } = this.state;
// 	this.setState({ openedOn: !openedOn });
// };
