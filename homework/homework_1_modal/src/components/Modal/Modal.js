import React, { PureComponent } from 'react';
// import Button from '../Button/Button';
import NameModal from '../NameModal/NameModal';
import './Modal.scss';
// import ReactDOM from 'react-dom';

class Modal extends PureComponent {
	render() {
		const { title, actions, text, close, onClick, ics, id } = this.props;

		return (
			<>
				<div className="container__box" onClick={onClick} id={id}>
					<div className="container" onClick={e => e.stopPropagation()}>
						<NameModal title={title} close={close} ics={ics} />
						<p>{text}</p>
						<div className="container-btn">{actions}</div>
					</div>
				</div>
			</>
		);
	}
}

export default Modal;
