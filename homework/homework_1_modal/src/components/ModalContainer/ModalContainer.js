import React, { PureComponent } from 'react';
import Modal from '../Modal/Modal';

import Button from '../Button/Button';

class ModalContainer extends PureComponent {
	state = {
		opened: false,
	};

	clickDoIt = () => {
		const { opened } = this.state;
		this.setState({ opened: !opened });
	};

	render() {
		const { clickDoIt } = this;
		const { opened } = this.state;

		const { name, text, title, bgright, bgleft, ics, bg } = this.props;

		return (
			<>
				<div>
					<Button
						backgroundColor={bg}
						name={name}
						onClick={clickDoIt}
						// className={dn ? 'dn' : ''}
					/>
				</div>
				{opened && (
					<Modal
						ics={ics}
						name={name}
						title={title}
						text={text}
						onClick={clickDoIt}
						close={clickDoIt}
						actions={[
							<Button
								name={'Ok'}
								onClick={clickDoIt}
								backgroundColor={bgright}
							/>,
							<Button
								name={'Cencel'}
								onClick={clickDoIt}
								backgroundColor={bgleft}
							/>,
						]}
					/>
				)}
			</>
		);
	}
}
export default ModalContainer;

// const { name, text, title } = modal[0].modalItem;
// const modals = modal.forEach((item) =>  key={item.modalItem.name} {...item});

// return (
// 	<>
// 		<div>
// 			<Button
// 				name={name}
// 				onClick={clickDoIt}
// 				// className={dn ? 'dn' : ''}
// 			/>
// 		</div>
// 		{opened && (
// 			<Modal
// 				name={name}
// 				title={title}
// 				text={text}
// 				opened={opened}
// 				onClick={clickDoIt}
// 				close={clickDoIt}
// 				actions={[
// 					<Button name={'Ok'} onClick={clickDoIt} />,
// 					<Button name={'Cencel'} onClick={clickDoIt} />,
// 				]}
// 			></Modal>
// 		)}
// 	</>
// );
