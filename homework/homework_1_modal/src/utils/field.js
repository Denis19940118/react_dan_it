// import React from 'react';
// import Modal from './Modal/Modal';
// import Button from './Button/Button';

export const modal = [
	{
		name: 'Open first modal',
		title: 'Do you want to delete this file?',
		text: `Once you delete this file,
				 it won’t be possible to undo this action. Are you sure you want to delete it?`,
		bgright: 'blue',
		bgleft: 'yellow',
		ics: true,
		bg: 'blue',
		id: 1,
	},
	{
		name: 'Open second modal',
		title: 'Do you want to download this file?',
		text: `Download this file,
				 it you most this do it!!!!`,
		bgright: 'green',
		bgleft: 'pink',
		ics: false,
		bg: 'pink',
		id: 2,
	},
];

// {
// 		modal: (
// 			<Modal
// 				name={'Open second modal'}
// 				title={'Do you want to download this file?'}
// 				text={`Download this file,
// 				 it you most this do it!!!!`}
// 				actions={[<Button name={'Ok'} />, <Button name={'Cencel'} />]}
// 			/>
// 		),
// 	},
