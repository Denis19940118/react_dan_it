import React, { Component } from 'react';
import './App.css';

import field, { modal } from './utils/field';
import ModalContainer from './components/ModalContainer/ModalContainer';

class App extends Component {
	render() {
		const modals = modal.map(item => (
			<ModalContainer key={item.id} {...item} />
		));
		// const { openModalOne, openModalSecond } = this.state;
		return <div className="App">{modals}</div>;
	}
}

export default App;

// state = {
// 	openModalOne: (
// 		<Modal
// 			name={'Open first modal'}
// 			title={'Do you want to delete this file?'}
// 			text={`Once you delete this file,
// 			 it won’t be possible to undo this action. Are you sure you want to delete it?`}
// 		/>
// 	),
// 	openModalSecond: false,
// };

// renderModal = modal => {
// 	this.setState({ modal: modal });
// };

// clickButton = event => {
// 	const { openModalOne } = this.state;
// 	this.setState({ openModalOne: !openModalOne });
// };
