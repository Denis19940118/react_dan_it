import React, { PureComponent } from 'react';
import Card from '../Card/Card';
import './Body.scss';
import axios from 'axios';

class Body extends PureComponent {
	state = {
		favorite:
			JSON.parse(localStorage.getItem('favorite')) ||
			localStorage.setItem('favorite', JSON.stringify([])),
		basket:
			JSON.parse(localStorage.getItem('basket')) ||
			localStorage.setItem('basket', JSON.stringify([])),
		cards: [],

		// unName: undefined,
	};

	componentDidMount = () => {
		axios('/products.json').then(res => this.setState({ cards: res.data }));
		// if (JSON.parse(localStorage.getItem('basket')) === null) {
		// 	this.setState({ basket: [] });
		// 	// this.isBasket();
		// } else {
		// 	this.setState({
		// 		basket: JSON.parse(localStorage.getItem('basket')),
		// 	});
		// 	// this.isBasket();
		// }
	};

	// isBasket = () => {
	// 	const { basket } = this.state;

	// 	localStorage.setItem('basket', JSON.stringify(basket));
	// };

	saveClick = (id, opened, box, nameBox, cliked) => {
		box.push({ id });

		localStorage.setItem(`${nameBox}`, JSON.stringify(box));
		nameBox === 'basket' ? cliked() : (cliked = undefined);
	};

	favoriteClick = (id, opened, box, nameBox) => {
		const fav = [];
		if (!opened) {
			// localStorage.setItem(`${id} addFavorite`, !opened);
			box.push({ id });
			this.setState({ favorite: box });
			localStorage.setItem(`${nameBox}`, !opened ? JSON.stringify(box) : '');
		} else {
			// localStorage.setItem(`${id} addFavorite`, !opened);
			const favorite = JSON.parse(localStorage.getItem(`${nameBox}`));
			localStorage.removeItem(`${nameBox}`);

			favorite.forEach(item => (item.id !== id ? fav.push(item) : ''));

			// this.setState({
			// 	unName: favorite.filter(item => {
			// 		if (item.id !== id) {
			// 			fav.push(item);
			// 		}
			// 	}),
			// });

			this.setState({ favorite: fav });
			localStorage.setItem(`${nameBox}`, JSON.stringify(fav));
		}
	};

	data = () => {};
	render() {
		const { basket, favorite, cards } = this.state;
		const allCards = cards.map(item => (
			<Card
				card={item}
				favorite={favorite}
				basket={basket}
				basketClick={this.saveClick}
				favoriteClick={this.favoriteClick}
				key={item.id}
			/>
		));
		return <div className={'container__cards'}>{allCards}</div>;
	}
}

export default Body;
