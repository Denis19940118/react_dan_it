import React from 'react';
import './Icon.scss';
import * as icons from '../../theme/icons';

const Icon = props => {
	const { type, color, size, filled, onClick, id } = props;

	const iconJsx = icons[type];

	if (!iconJsx) {
		return null;
	}

	return (
		<>
			<p className={`icon ${size ? `icon--${size}` : ''}`}>
				{iconJsx(color, filled, onClick, id)}
			</p>
		</>
	);
};
export default Icon;
