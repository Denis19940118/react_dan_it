import React, { PureComponent } from 'react';
import './Header.scss';

class Header extends PureComponent {
	state = {
		favorite: JSON.parse(localStorage.getItem('favorite')),
		basket: JSON.parse(localStorage.getItem('basket')),
	};

	// setFavorite = () => {
	// 	const { favorite } = this.state;
	// 	if (favorite === null) {
	// 		this.setState({ favorite: [] });
	// 		return favorite;
	// 	} else {
	// 		// this.setState({ favorite: JSON.parse(localStorage.getItem('favorite')) });
	// 		return favorite;
	// 	}
	// };

	// setBasket = () => {
	// 	const { basket } = this.state;
	// 	if (basket === null) {
	// 		this.setState({ basket: [] });
	// 		return basket;
	// 	} else {
	// 		// this.setState({ basket: JSON.parse(localStorage.getItem('basket')) });
	// 		return basket;
	// 	}
	// };

	render() {
		const { favorite, basket } = this.state;
		return (
			<section className={'header'}>
				<p>Favorite :{favorite === null ? [].length : favorite.length}</p>
				<p>Basket :{basket === null ? [].length : basket.length}</p>
			</section>
		);
	}
}
export default Header;
