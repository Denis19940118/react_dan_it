import React, { PureComponent } from 'react';
import propTypes from 'prop-types';
import './Modal.scss';

class Modal extends PureComponent {
	render() {
		const { key, title, onClick, actions } = this.props;
		return (
			<>
				<div key={key} className={'container-box'} onClick={onClick}>
					<div className={'container'} onClick={e => e.stopPropagation()}>
						<p>{title}</p>
						<div className="container-btn">{actions}</div>
					</div>
				</div>
			</>
		);
	}
}

Modal.propTypes = {
	title: propTypes.string.isRequired,
	onClick: propTypes.func,
	actions: propTypes.array,
};

Modal.defaultTypes = {
	onClick: undefined,
	actions: 'Buttons',
};
export default Modal;
