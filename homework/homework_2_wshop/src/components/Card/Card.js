import React, { PureComponent } from 'react';
import Modal from '../Modal/Modal';
import Button from '../Button/Button';
import { modal } from '../../utils/field';
import propTypes from 'prop-types';
import './Card.scss';
import Icon from '../Icon/Icon';

class Card extends PureComponent {
	state = {
		isActive: false,
		opened: false,
		addFavorite: false,
	};

	componentDidMount = () => {
		const { isActive } = this.state;
		const { card } = this.props;
		let favorite = JSON.parse(localStorage.getItem(`favorite`));
		favorite.some(item =>
			item.id === card.id ? this.setState({ isActive: !isActive }) : ''
		);
		// this.setState({
		// 	isActive:
		// 		JSON.parse(localStorage.getItem(`${card.id} addFavorite`)) || false,
		// });
	};

	cliked = () => {
		const { opened } = this.state;
		this.setState({ opened: !opened });
	};

	changeColor = () => {
		const { isActive } = this.state;
		this.setState({
			isActive: !isActive,
		});
	};

	render() {
		const {
			favorite,
			basket,
			key,
			card,
			basketClick,
			favoriteClick,
		} = this.props;

		const { name, price, picture, article, id } = card;
		const nameCard = name;

		const { opened, isActive } = this.state;
		const { cliked } = this;

		const { nameModal, title } = modal;
		const nameButton = nameModal;

		return (
			<div className={'card'} key={key}>
				<p>{article}</p>
				<img className={'img'} src={picture} alt={picture}></img>
				<span className={'card-name'}>{nameCard}</span>

				<div>
					<Icon
						type={'star'}
						color={'red'}
						filled={isActive}
						id={key}
						onClick={() => {
							this.changeColor();
							favoriteClick(id, isActive, favorite, 'favorite');
						}}
					/>
				</div>

				<div className={'card__box-price'}>
					<span>{price} $Uan</span>
				</div>
				<div>
					<Button name={nameButton} onClick={cliked} />
				</div>
				{opened && (
					<>
						<Modal
							key={key}
							title={title}
							onClick={cliked}
							actions={[
								<Button
									key={key}
									name={'Ok'}
									onClick={() =>
										basketClick(id, !opened, basket, 'basket', cliked)
									}
								/>,
								<Button name={'Cancel'} onClick={cliked} />,
							]}
						/>
					</>
				)}
			</div>
		);
	}
}

Card.propTypes = {
	name: propTypes.string,
	price: propTypes.string,
	picture: propTypes.string,
	articule: propTypes.string,
	title: propTypes.string,
	nameCard: propTypes.string,
	star: propTypes.func,
};
export default Card;
