import React, { useState } from 'react';
import '../Basket/Basket.scss';
import { useEffect } from 'react';

import Button from '../../components/Button/Button';
import Body from '../../components/Body/Body';

const Basket = () => {
	const [place, setPlace] = useState([]);
	const [cards, setCards] = useState(
		// []
		JSON.parse(localStorage.getItem('basket') || [])
	);

	useEffect(() => {
		setCards(JSON.parse(localStorage.getItem('basket')));
	}, []);

	const removeCard = (card, opened, nameBox, setOpened) => {
		// setOpened(!opened);

		const box = cards.filter(item =>
			item.id !== card.id ? place.push(item) : ''
		);
		localStorage.removeItem(`${'basket'}`);
		localStorage.setItem(`${'basket'}`, JSON.stringify(place));
		// place.splice(0, place.length);
	};

	// const cardBasket = cards.map(item => (

	// ));

	return (
		<div>
			<h2>Basket</h2>
			<div className="card__wrapper">
				<Body
					card={item}
					// favorite={favorite}
					// basket={basket}

					key={item.id}
					nameCard={item.name}
					id={item.id}
					picture={item.picture}
					price={item.price}
					article={item.article}
				/>
				<Button
					className="card__wrapper-delete"
					name={'X'}
					onClick={() => removeCard(item)}
				/>
			</div>
		</div>
	);
};

export default Basket;
