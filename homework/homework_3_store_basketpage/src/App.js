import React, { useState } from 'react';
import './App.css';
import Body from './components/Body/Body';
import Header from './components/Header/Header';
import AppRouter from './routes/AppRouter';

const App = () => {
	return (
		<div className="App">
			<AppRouter />
			{/* <Body /> */}
		</div>
	);
};

export default App;
