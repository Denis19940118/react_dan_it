import React, { useState, useEffect } from 'react';
import {
	Redirect,
	Route,
	BrowserRouter as Router,
	Switch,
} from 'react-router-dom';
import axios from 'axios';
// import Body from '../components/Body/Body';
import Header from '../components/Header/Header';

import Basket from '../pages/Basket/Basket';
import Favorite from '../pages/Favorite/Favorite';
import Homepage from '../pages/Homepage/Homepage';

const AppRouter = () => {
	const [favorite, setFavorite] = useState(
		JSON.parse(localStorage.getItem('favorite')) || null
	);
	const [basket, setBasket] = useState(
		JSON.parse(localStorage.getItem('basket')) || null
	);
	const [cards, setCards] = useState([]);
	const [fav, setFav] = useState([]);
	const [unName, setUnName] = useState(undefined);

	useEffect(() => {
		axios('/products.json').then(res => setCards(res.data));
		JSON.parse(localStorage.getItem('favorite')) === null
			? setFavorite([])
			: JSON.parse(localStorage.getItem('favorite'))(
					JSON.parse(localStorage.getItem('basket')) === null
						? setBasket([])
						: JSON.parse(localStorage.getItem('basket'))
			  );
	}, []);

	const saveBasket = (card, opened, box, nameBox, setOpened) => {
		box.push(card);

		localStorage.setItem(`${nameBox}`, JSON.stringify(box));
		nameBox === 'basket' ? setOpened() : (opened = false);
	};
	// addFavorite = () => {};

	// removeFavorite = () => {};

	// removeItemBasket = () => {};

	return (
		<>
			<Router>
				<Header />
				<Switch>
					<Redirect exact from="/" to="/homepage" />
					<Route path="/homepage">
						<Homepage
							basket={basket}
							setBasket={setBasket}
							favorite={favorite}
							setFavorite={setFavorite}
							cardsNative={cards}
						/>
					</Route>
					{/* <Route exact path="/body" component={Body} /> */}
					<Route path="/basket">
						<Basket
							basket={basket}
							setBasket={setBasket}
							favorite={favorite}
							setFavorite={setFavorite}
							cardsNative={cards}
						/>
					</Route>
					<Route path="/favorite">
						<Favorite
							basket={basket}
							setBasket={setBasket}
							favorite={favorite}
							setFavorite={setFavorite}
							cardsNative={cards}
						/>
					</Route>
				</Switch>
			</Router>
		</>
	);
};
export default AppRouter;
