import React from 'react';
import { NavLink } from 'react-router-dom';
import './Header.scss';

const Header = props => {
	return (
		<nav>
			<NavLink exact to="/">
				Homepage
			</NavLink>
			{/* <NavLink to="/body">Body</NavLink> */}
			<NavLink to="/basket">Basket</NavLink>
			<NavLink to="/favorite">Favorite</NavLink>
		</nav>
	);
};
export default Header;
