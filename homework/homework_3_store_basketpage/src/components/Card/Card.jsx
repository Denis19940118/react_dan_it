import React, { useEffect, useState } from 'react';
import { modal } from '../field';
import Modal from '../Modal/Modal';
import Button from '../Button/Button';
import './Card.scss';
import Icon from '../Icon/Icon';
import propTypes from 'prop-types';

const Card = props => {
	const [opened, setOpened] = useState(false);
	const [isActive, setIsActive] = useState(false);
	const [colorStar, setColorStar] = useState(false);

	const { name, title } = modal;
	const {
		nameCard,
		price,
		picture,
		article,
		favorite,
		basket,
		id,
		card,
		basketClick,
		favoriteClick,
		basketRemove,
	} = props;

	// const saveClick = (nameCard, article) => {
	// 	basket.push({ nameCard, article });
	// 	localStorage.setItem(`${'basket'}`, JSON.stringify(basket));
	// 	setOpened(!opened);
	// };

	useEffect(() => {
		setIsActive(JSON.parse(localStorage.getItem(`${id} addFavorite`)) || false);
	}, [isActive]);

	const changeColor = () => {
		setIsActive(!isActive);
	};

	return (
		<div className={'card'} id={id}>
			<p>{article}</p>
			<img className={'img'} src={picture} alt={picture}></img>
			<span className={'card-name'}>{nameCard}</span>

			<div>
				<Icon
					type={'star'}
					color={'red'}
					filled={isActive}
					id={id}
					onClick={() => {
						changeColor();
						favoriteClick(card, isActive, favorite, 'favorite');
					}}
				/>
			</div>
			<div className={'card__box-price'}>
				<span>{price} $Uan</span>
			</div>
			<div>
				<Button name={name} onClick={() => setOpened(!opened)} />
			</div>
			{opened && (
				<div>
					<Modal
						title={title}
						onClick={() => setOpened(!opened)}
						actions={[
							<Button
								name={'Ok'}
								onClick={() =>
									basketClick(card, !opened, basket, 'basket', setOpened)
								}
								// () => saveClick(nameCard, article)}
							/>,
							<Button name={'Cancel'} onClick={() => setOpened(!opened)} />,
						]}
					/>
				</div>
			)}

			{/* {basketRemove && (
				<Button
					name={'X'}
					onClick={basketClick(card, !opened, basket, 'basket', setOpened)}
				/>
			)} */}
		</div>
	);
};

Card.propTypes = {
	name: propTypes.string.isRequired,
	price: propTypes.string.isRequired,
	picture: propTypes.string.isRequired,
	articule: propTypes.string.isRequired,
	title: propTypes.string.isRequired,
	nameCard: propTypes.string.isRequired,
	star: propTypes.func.isRequired,
};
export default Card;

// const saveClickStar = (nameCard, articule, id, e) => {
// 	setColorStar(true);
// 	if (colorStar) {
// 		localStorage.removeItem(`${'favorite'}`, JSON.stringify(favorite));
// 		favorite.filter(item => {
// 			const { articule } = item;
// 			e.target.id !== articule
// 				? favorite.push(nameCard, articule, id)
// 				: console.log(item);
// 		});
// 		localStorage.setItem(`${'favorite'}`, JSON.stringify(favorite));
// 	} else {
// 		favorite.push({ nameCard, articule, id });
// 		localStorage.setItem(`${'favorite'}`, JSON.stringify(favorite));
// 	}
// };
