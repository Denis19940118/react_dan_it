import React from 'react';
import propTypes from 'prop-types';

const Button = props => {
	const {
		name,
		color,
		type,
		size,
		fontSize,
		backgroundColor,
		className,
		onClick,
	} = props;

	const style = {
		width: size,
		backgroundColor: backgroundColor,
		fontSize: fontSize,
		color: color,
	};

	return (
		<>
			<button className={className} type={type} style={style} onClick={onClick}>
				{name}
			</button>
		</>
	);
};

Button.propTypes = {
	name: propTypes.string,
	type: propTypes.string,
	size: propTypes.number,
	fontSize: propTypes.number,
	backgroundColor: propTypes.string,
	className: propTypes.string,
	onClick: propTypes.func,
	color: propTypes.string,
};

Button.defaultProps = {
	types: 'button',
	fontSize: 16,
	onClick: undefined,
	className: 'btn-default',
	backgroundColor: '',
	size: 130,
};
export default Button;
