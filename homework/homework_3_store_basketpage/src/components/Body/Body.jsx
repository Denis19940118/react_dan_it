import React, { useEffect, useState } from 'react';
import axios from 'axios';
import './Body.scss';
import Card from '../Card/Card';

const Body = props => {
	// const [favorite, setFavorite] = useState(
	// 	JSON.parse(localStorage.getItem('favorite')) || []
	// );
	// const [basket, setBasket] = useState(
	// 	JSON.parse(localStorage.getItem('basket')) || []
	// );
	// const [cards, setCards] = useState([]);
	const [fav, setFav] = useState([]);
	const [unName, setUnName] = useState(undefined);
	const { basket, favorite, setBasket, setFavorite, cardsNative } = props;
	// useEffect(() => {
	// 	axios('/products.json').then(res => setCards(res.data));
	// }, []);

	// useEffect(() => {}, []);
	const saveBasket = (card, opened, box, nameBox, setOpened) => {
		box.push(card);

		localStorage.setItem(`${nameBox}`, JSON.stringify(box));
		nameBox === 'basket' ? setOpened() : (opened = false);
	};
	// addFavorite = () => {};

	// removeFavorite = () => {};

	// removeItemBasket = () => {};

	const cardsRender = cardsNative.map(item => (
		<li>
			<Card
				card={item}
				favorite={favorite}
				basket={basket}
				basketClick={saveBasket}
				// favoriteClick={favoriteClick}
				key={item.id}
				nameCard={item.name}
				id={item.id}
				picture={item.picture}
				price={item.price}
				article={item.article}
			/>
		</li>
	));

	return <div className={'container__cards'}>{cardsRender}</div>;
};

export default Body;
