import React from 'react';
import propTypes from 'prop-types';
import './Modal.scss';
import Button from '../Button/Button';

const Modal = props => {
	const { title, setOpened, actions } = props;
	console.log('title', title);
	return (
		<>
			<div className={'container-box'} onClick={setOpened}>
				<div className={'container'} onClick={e => e.stopPropagation()}>
					<p>{title}</p>
					<div className="container-btn">{actions}</div>
				</div>
			</div>
		</>
	);
};

Modal.propTypes = {
	title: propTypes.string.isRequired,
	onClick: propTypes.func,
	actions: propTypes.array,
};

Modal.defaultTypes = {
	onClick: undefined,
	actions: 'Buttons',
};
export default Modal;
