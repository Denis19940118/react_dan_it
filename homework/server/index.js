const express = require('express');
const app = express();
const PORT = process.env.PORT || 8080;
const { v4: uuidv4 } = require('uuid');

const USERS = [
	{
		login: 'admin@g.g',
		password: '123123',
	},
];

const CARDS = [
	{
		id: '1',
		name: 'Margherita',
		price: '100',
		picture: './picture/Margherita.jpg',
		articule: '123425678',
		color: 'yellow',
	},
	{
		id: '2',
		name: 'Sea',
		price: '180',
		picture: './picture/Sea.jpg',
		articule: '098765345',
		color: 'yellow',
	},
	{
		id: '3',
		name: 'Mushroom',
		price: '140',
		picture: './picture/Mushroom.jpg',
		articule: '098766789',
		color: 'yellow',
	},
	{
		id: '4',
		name: 'Meating',
		price: '140',
		picture: './picture/Meat.jpg',
		articule: '123451234',
		color: 'yellow',
	},
	{
		id: '5',
		name: '4Cheese',
		price: '170',
		picture: './picture/4Cheese.jpg',
		articule: '234567809',
		color: 'yellow',
	},
	{
		id: '6',
		name: 'Papperoni',
		price: '160',
		picture: './picture/Paperoni.jpg',
		articule: '987650234',
		color: 'yellow',
	},
	{
		id: '7',
		name: 'Pesto',
		price: '155',
		picture: './picture/Pesto.jpg',
		articule: '678345098',
		color: 'yellow',
	},
	{
		id: '8',
		name: 'Four Seasons',
		price: '150',
		picture: './picture/4Seasons.jpg',
		articule: '123456734',
		color: 'yellow',
	},
	{
		id: '9',
		name: 'Caesar',
		price: '160',
		picture: './picture/Caesar.jpg',
		articule: '678901232',
		color: 'yellow',
	},
	{
		id: '10',
		name: 'Salmon',
		price: '190',
		picture: './picture/Salmons.jpg',
		articule: '987623454',
		color: 'yellow',
	},
	{
		id: '11',
		name: 'Vegeterian',
		price: '145',
		picture: './picture/Vegeterian.jpg',
		articule: '876545412',
		color: 'yellow',
	},
];

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.post('/api/users/', (req, res) => {
	const user = req.body;
	USERS.push(user);
	res.send(user);
});

app.post('/api/users/login', (req, res) => {
	const user = req.body;
	const userFromDB = USERS.find(
		u => u.login === user.login && u.password === user.password
	);

	res.send({
		status: !!userFromDB,
		data: userFromDB,
	});
});

app.get('/api/cards', (req, res) => {
	res.send(HOME);
});

app.get('/api/cards/:folder', (req, res) => {
	const { folder } = req.params;

	res.send(HOME[folder]);
});

app.get('/api/card/:cardID', (req, res) => {
	const { cardID } = req.params;
	let requestedCard = null;

	Object.keys(CARDS).forEach(box => {
		const find = CARDS[box].find(card => card.id === cardID);

		if (find) {
			requestedCard = find;
		}
	});
	res.send(requestedCard);
});

app.post('/api/cards/:folder', (req, res) => {
	const { folder } = req.params;
	const newCard = req.body;

	newCard.id = uuidv4();

	CARDS[folder].push(newCard);

	res.send({
		message: 'email saved successfully',
		data: newCard,
	});
});

app.listen(PORT, () => {
	console.log('server is listening on port ' + PORT);
});
