import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import {
	BrowserRouter as Router,
	Redirect,
	Route,
	Switch,
} from 'react-router-dom';
import Header from '../components/Header/Header';
import LoginForm from '../components/LoginForm/LoginForm';
import Basket from '../pages/Basket/Basket';
import Favorite from '../pages/Favorite/Favorite';
import Home from '../pages/Home/Home';
import Mail from '../pages/Mail/Mail';
import Page404 from '../pages/Page404/Page404';
import { saveAllCardsAction } from '../redux/action';
import { selectIsAuth } from '../redux/selectors';
import { ProtectedRoute } from './ProtectedRoute';

const AppRouter = ({ saveAllCards, ...props }) => {
	const [cardsBody, setCardBody] = useState([]);
	const [favorite, setFavorite] = useState(null);
	const [basket, setBasket] = useState(
		JSON.parse(localStorage.getItem('basket')) || null
	);
	const { isAuth, saveUserAction } = props;

	useEffect(() => {
		axios
			.get('/products.json')
			// .then(res => setCardBody(res.data));
			.then(res => {
				// console.log(' res', res.data);

				saveAllCards(res.data);
				console.log('saveAllCards(res.data)', saveAllCards(res.data));
			});

		if (JSON.parse(localStorage.getItem('favorite')) === null) {
			// localStorage.setItem('favorite', JSON.stringify([]));
			setFavorite([]);
		} else {
			setFavorite(JSON.parse(localStorage.getItem('favorite')));
		}

		if (JSON.parse(localStorage.getItem('basket')) === null) {
			// localStorage.setItem('basket', JSON.stringify([]));
			setBasket([]);
		} else {
			setBasket(JSON.parse(localStorage.getItem('basket')));
		}
	}, []);

	return (
		<>
			{/* {cardsBody !== null && favorite !== null && basket !== null && ( */}
			{/* <Router> */}
			<ProtectedRoute
				path="/"
				isAuth={isAuth}
				protectedRender={() => <Header />}
			/>
			<Switch>
				<Route
					exact
					path="/login"
					render={routerProps => {
						if (isAuth) {
							return <Redirect to="/home" />;
						}
						return <LoginForm {...routerProps} />;
					}}
				/>

				<ProtectedRoute
					exact
					path={'/home'}
					isAuth={isAuth}
					protectedRender={() => (
						<Home
							isAuth={isAuth}
							cards={cardsBody}
							favorite={favorite}
							setFavorite={setFavorite}
							basket={basket}
							setBasket={setBasket}
						/>
					)}
				/>

				<ProtectedRoute
					exact
					path={'/favorite'}
					isAuth={isAuth}
					protectedRender={() => (
						<Favorite
							isAuth={isAuth}
							cards={cardsBody}
							favorite={favorite}
							setFavorite={setFavorite}
							basket={basket}
							setBasket={setBasket}
						/>
					)}
				/>
				<ProtectedRoute
					exact
					path={'/basket'}
					isAuth={isAuth}
					protectedRender={() => (
						<Basket
							isAuth={isAuth}
							cards={cardsBody}
							favorite={favorite}
							setFavorite={setFavorite}
							basket={basket}
							setBasket={setBasket}
						/>
					)}
				/>

				<ProtectedRoute
					exact
					path={'/mail'}
					isAuth={isAuth}
					protectedRender={() => (
						<Mail
							cards={cardsBody}
							favorite={favorite}
							setFavorite={setFavorite}
							basket={basket}
							setBasket={setBasket}
						/>
					)}
				/>

				<Route path={'/*'} component={Page404} />
			</Switch>
			{/* </Router> */}
			{/* )} */}
		</>
	);
};

const mapStateToProps = currentStore => ({
	// isAuth: !!(currentStore.login && currentStore.password),
	isAuth: selectIsAuth(currentStore),
});

const mapDispachToProps = dispatch => ({
	saveAllCards: cards => dispatch(saveAllCardsAction(cards)),
});

export default connect(mapStateToProps, mapDispachToProps)(AppRouter);
