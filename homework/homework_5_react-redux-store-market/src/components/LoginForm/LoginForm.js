import React, { useRef } from 'react';
import { connect } from 'react-redux';
import { saveUserAction } from '../../redux/action';
import { SAVE_USER } from '../../redux/actionTypes';
import './LoginForm.scss';

const credentials = {
	login: 'admin',
	password: '123',
};

const LoginForm = ({
	history,
	location,
	match,
	// auth,
	dispatch,
	saveUser,
}) => {
	let loginRef = useRef();
	let passwordRef = useRef();

	const handleSubmit = event => {
		event.preventDefault();

		if (
			loginRef.value === credentials.login &&
			passwordRef.value === credentials.password
		) {
			console.log(' loginRef.value ----', loginRef.value);
			const newUser = { login: loginRef.value, password: passwordRef.value };
			// auth(newUser);
			// dispatch({ type: 'SAVE_USER', payload: newUser });
			saveUser(newUser);
		}
	};

	const mapInputLogin = loginInput => {
		loginRef = loginInput;
	};

	const mapPasswordInput = password => {
		passwordRef = password;
		console.log('🚀 ~ file: LoginForm.js ~ line 48 ~ passwordRef', passwordRef);
	};

	return (
		<form onSubmit={event => handleSubmit(event)} className="login" action="">
			<input
				ref={loginInput => mapInputLogin(loginInput)}
				type="text"
				className="login__input"
				placeholder="Enter login"
			/>
			<input
				ref={password => mapPasswordInput(password)}
				type="password"
				className="login__input"
				placeholder="Enter password"
			/>
			<button type="submit" className="header__login-btn">
				Login
			</button>
		</form>
	);
};
// const mapStateToProps = reduxState => ({ user: reduxState.user });
// const mapDispatchToProps = dispatch => ({
// 	saveUser: user => dispatch(userActions.saveUser(user)),
// });
const mapDispatchToProps = dispatch => ({
	// saveUser: user => dispatch({ type: SAVE_USER, payload: user }),
	saveUser: user => dispatch(saveUserAction(user)),
});

// export default connect(() => null)(LoginForm);
export default connect(() => ({}), mapDispatchToProps)(LoginForm);
