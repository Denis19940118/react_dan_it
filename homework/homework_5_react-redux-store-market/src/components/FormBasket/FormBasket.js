import React from 'react';
import { connect } from 'react-redux';
import Button from '../Button/Button';
import './FormBasket.scss';

const FormBasket = () => {
	const handlerSubmitBuy = () => {
		const productBasket = JSON.parse(localStorage.getItem('basket'));
		localStorage.removeItem(`${'basket'}`);

		localStorage.setItem('basket', JSON.stringify([]));
		console.log('You buy product');
	};

	return (
		<form className="basket__form">
			<input
				type="text"
				placeholder="Enter name"
				className="basket__form-name"
			/>
			<input
				type="text"
				placeholder="Enter second name"
				className="basket__form-name"
			/>
			<input
				type="mail"
				placeholder="Enter email"
				className="basket__form-name"
			/>
			<input
				type="number"
				placeholder="Enter age"
				className="basket__form-name"
			/>
			<input
				type="text"
				placeholder="Enter adress"
				className="basket__form-name"
			/>
			<input
				type="phone"
				placeholder="Enter phone"
				className="basket__form-name"
			/>
			<Button
				type="button"
				className="basket__form-btn"
				name="Buy product"
				onClick={() => handlerSubmitBuy()}
			/>
		</form>
	);
};
export default connect()(FormBasket);
