import React, { PureComponent } from 'react';
import Button from '../Button/Button';
import Modal from '../Modal/Modal';
import propTypes from 'prop-types';
import './ModalContainer.scss';

class ModalContainer extends PureComponent {
	state = {
		opened: false,
		card: JSON.parse(localStorage.getItem('card')),
		colorStar: false,
	};

	cliked = () => {
		const { opened } = this.state;
		this.setState({ opened: !opened });
	};

	saveClick = (nameCard, articule) => {
		const { basket } = this.props;
		basket.push({ nameCard, articule });
		this.setState({ nameCard });
		localStorage.setItem(`${'basket'}`, JSON.stringify(basket));
		const { opened } = this.state;
		this.setState({ opened: !opened });
	};

	saveClickStar = (nameCard, articule, id, e) => {
		const { colorStar } = this.state;
		const { favorite } = this.props;

		this.setState({
			colorStar: !colorStar,
			favorite: favorite,
		});
		if (colorStar) {
			localStorage.removeItem(`${'favorite'}`, JSON.stringify(favorite));

			favorite.forEach(item => {
				const { articule } = item;

				if (e.target.id === item.articule) {
					console.log('ModalContainer -> saveClickStar -> item', item);
					// const card = item;
					favorite.splice(item, 1);
				}
			});

			localStorage.setItem(`${'favorite'}`, JSON.stringify(favorite));
		} else {
			// const fav = favorite;
			favorite.push({ nameCard, articule, id });

			localStorage.setItem(`${'favorite'}`, JSON.stringify(favorite));
		}
	};

	render() {
		const { opened, colorStar } = this.state;
		const { cliked, saveClick, saveClickStar } = this;
		const {
			name,
			title,
			price,
			nameCard,
			picture,
			articule,
			star,
			id,
		} = this.props;

		return (
			<>
				<img className={'img'} src={picture} alt={picture}></img>
				<p className={'card-name'}>
					{nameCard}
					{star(
						'red',
						colorStar,
						e => saveClickStar(nameCard, articule, id, e),
						articule
					)}
				</p>

				<div className={'card__box-price'}>
					<span>{price} $Uan</span>
				</div>
				<div>
					<Button name={name} onClick={cliked} />
				</div>
				{opened && (
					<>
						<Modal
							title={title}
							onClick={cliked}
							actions={[
								<Button
									name={'Ok'}
									onClick={() => saveClick(nameCard, articule)}
								/>,
								<Button name={'Cancel'} onClick={cliked} />,
							]}
						/>
					</>
				)}
			</>
		);
	}
}

ModalContainer.propTypes = {
	name: propTypes.string.isRequired,
	title: propTypes.string.isRequired,
	price: propTypes.string.isRequired,
	nameCard: propTypes.string.isRequired,
	picture: propTypes.string.isRequired,
	articule: propTypes.string.isRequired,
	star: propTypes.func.isRequired,
};

export default ModalContainer;
