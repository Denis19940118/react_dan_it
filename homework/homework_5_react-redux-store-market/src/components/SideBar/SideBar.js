import React from 'react';
import { NavLink } from 'react-router-dom';
import './SideBar.scss';

const SideBar = props => {
	const { items } = props;
	const allLink = items.map((item, index) => (
		<li key={index} className="emails-sidebar__item">
			<NavLink
				activeClassName="emails-sidebar__item--active"
				to={`/${item.toLowerCase()}`}
			>
				{item.toUpperCase()}
			</NavLink>
		</li>
	));

	return (
		<aside className="emails-sidebar">
			<ul className="emails-sidebar__list">{allLink}</ul>
		</aside>
	);
};

export default SideBar;
