import React from 'react';
import propTypes from 'prop-types';
import './Modal.scss';

const Modal = props => {
	const { key, title, onClick, actions } = props;
	return (
		<>
			<div key={key} className={'container-box'} onClick={onClick}>
				<div className={'container'} onClick={e => e.stopPropagation()}>
					<p>{title}</p>
					<div className="container-btn">{actions}</div>
				</div>
			</div>
		</>
	);
};

Modal.propTypes = {
	title: propTypes.string.isRequired,
	onClick: propTypes.func,
	actions: propTypes.array,
};

Modal.defaultTypes = {
	onClick: undefined,
	actions: 'Buttons',
};
export default Modal;
