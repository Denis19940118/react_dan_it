import React from 'react';
import { connect } from 'react-redux';
import ContainerCard from '../../components/ContainerCard/ContainerCard';
import { selectCards } from '../../redux/selectors';

const Home = props => {
	const { cards, favorite, setFavorite, basket, setBasket } = props;
	console.dir('🚀 ~ file: Home.js ~ line 5 ~ selectCards', cards);

	return (
		<section>
			<ContainerCard
				cards={cards}
				favorite={favorite}
				setFavorite={setFavorite}
				basket={basket}
				setBasket={setBasket}
			/>
		</section>
	);
};

const mapStateToProps = currentStore => ({
	cards: selectCards(currentStore),
	// headingText: reduxState.textProp,
});

// export default Home;
export default connect(mapStateToProps)(Home);
