import React from 'react';
import { connect } from 'react-redux';
import ContainerCard from '../../components/ContainerCard/ContainerCard';
import { selectCards } from '../../redux/selectors';
// import { selectFavorite } from '../../redux/selectors';

const Favorite = props => {
	const { cards, favorite, setFavorite, basket, setBasket } = props;
	console.log('🚀 ~ file: Favorite.js ~ line 9 ~ cards', cards);
	console.log('🚀 ~ file: Favorite.js ~ line 9 ~ favorite', favorite);

	// const favoriteCards = JSON.parse(localStorage.getItem('favorite'));
	// const allFavorite = favoriteCards.map(item => item.id = (
	// 	<ContainerCard card={item} basketRemove={false} />
	// ));
	// const ListToFavoriteCards = cards.filter(({ id }) => favorite.includes(id));
	const ListToFavoriteCards = [];
	cards.some(card =>
		favorite.forEach(item =>
			card.id === item.id ? ListToFavoriteCards.push(card) : ''
		)
	);

	return (
		<>
			<ContainerCard
				cards={ListToFavoriteCards}
				favorite={favorite}
				setFavorite={setFavorite}
				basket={basket}
				setBasket={setBasket}
				favoriteTrue={true}
			/>
		</>
	);
};

// const mapStateToProps = reduxState => ({
// 	favorite: selectFavorite(reduxState),
// });
const mapStateToProps = currentStore => ({
	cards: selectCards(currentStore),
	// headingText: reduxState.textProp,
});
export default connect(mapStateToProps)(Favorite);
// export default connect(mapStateToProps)(Favorite);
