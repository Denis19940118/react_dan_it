import { SAVE_USER, SAVE_ALL_CARDS } from '../actionTypes';

export const saveUserAction = user => ({
	type: SAVE_USER,
	payload: user,
});
export const saveAllCardsAction = cards => ({
	type: SAVE_ALL_CARDS,
	payload: { cards: cards },
});
