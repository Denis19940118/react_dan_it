import React, { useState } from 'react';
import Card from '../Card/Card';
import './ContainerCard.scss';
import Button from '../Button/Button';
import Modal from '../Modal/Modal';
import { useEffect } from 'react';
import { connect } from 'react-redux';
const ContainerCard = props => {
	// const [cardsBody, setCardBody] = useState([]);
	// const [cardState, setCardState] = useState(null);
	const [open, setOpen] = useState(false);
	const [stateFavorite, setStateFavorite] = useState(false);
	const {
		cards,
		basketRemove,
		favorite,
		setFavorite,
		basket,
		setBasket,
		favoriteTrue,
	} = props;
	console.log('🚀 ~ file: ContainerCard.js ~ line 21 ~ cards', cards);

	const [basketCard, setBasketCard] = useState(basketRemove === true || false);

	const saveClick = (id, opened, box, nameBox, cliked) => {
		box.push({ id });

		localStorage.setItem(`${nameBox}`, JSON.stringify(box));
		nameBox === 'basket' ? cliked() : (cliked = undefined);
	};

	const favoriteClick = (id, opened, box, nameBox) => {
		const fav = [];
		if (!opened) {
			// localStorage.setItem(`${id} addFavorite`, !opened);
			box.push({ id });
			setFavorite(box);
			localStorage.setItem(`${nameBox}`, !opened ? JSON.stringify(box) : '');
		} else {
			// localStorage.setItem(`${id} addFavorite`, !opened);
			const favorite = JSON.parse(localStorage.getItem(`${nameBox}`));
			localStorage.removeItem(`${nameBox}`);

			favorite.forEach(item => (item.id !== id ? fav.push(item) : ''));

			// this.setState({
			// 	unName: favorite.filter(item => {
			// 		if (item.id !== id) {
			// 			fav.push(item);
			// 		}
			// 	}),
			// });
			setFavorite(fav);
			// this.setState({ favorite: fav });
			localStorage.setItem(`${nameBox}`, JSON.stringify(fav));
		}
	};

	// const removeCard = (card, opened, nameBox, setOpened) => {
	// 	setBasketCard(basketRemove);
	// 	const place = [];
	// 	const basket = JSON.parse(localStorage.getItem('basket'));

	// 	basket.filter(item => (item.id !== card.id ? place.push(item) : ''));
	// 	// favorite.forEach(item => (item.id !== id ? fav.push(item) : ''));
	// 	localStorage.removeItem(`${'basket'}`);
	// 	localStorage.setItem(`${'basket'}`, JSON.stringify(place));
	// 	btnDelete(card);
	// 	// place.splice(0, place.length);
	// };

	// const inFavoriteHandler = () => {
	// 	let fav = JSON.parse(localStorage.getItem('favorite'));
	// 	fav.map(item);
	// };

	// const btnDelete = () => {
	// 	setOpen(!open);
	// };

	const allCards = cards.map(item => (
		<li key={item.id}>
			<Card
				card={item}
				favorite={favorite}
				basket={basket}
				basketClick={saveClick}
				favoriteClick={favoriteClick}
				key={item.id}
				setFavorite={setFavorite}
				setBasket={setBasket}
				basketCard={basketCard}
				favoriteTrue={favoriteTrue}
			/>
		</li>
	));

	{
		/* {basketCard && <Button name={'Delete'} onClick={btnDelete} />}
			{open && (
				<>
					<Modal
						title={'Delete card right now?'}
						actions={[
							<Button name={'Ok'} onClick={() => removeCard(item)} />,
							<Button name={'Cancel'} onClick={btnDelete} />,
						]}
					/>
				</>
			)} */
	}
	// cards.map(cardItem =>
	// 	cardItem.id === item.id ? (
	// FavoriteTrue && (
	// 	<li id={item.id} key={item.id}>
	// 		<Card
	// 			card={item}
	// 			favorite={favorite}
	// 			basket={basket}
	// 			basketClick={saveClick}
	// 			favoriteClick={favoriteClick}
	// 			key={item.id}
	// 		/>
	// 	</li>
	// )
	// basketRemove && (
	// 	<Button name={'X'} onClick={() => removeCard(item)}>
	// 		<Button name={'X'} onClick={setOpened(!opened)} />
	// 		{opened && (
	// 			<>
	// 				<Modal
	// 					key={cards.id}
	// 					title={'remove?'}
	// 					onClick={setOpened(!opened)}
	// 					actions={[
	// 						<Button
	// 							className="card__wrapper-delete"
	// 							name={'Ok'}
	// 							onClick={() => removeCard(item)}
	// 						/>,
	// 						<Button name={'Cancel'} onClick={setOpened(!opened)} />,
	// 					]}
	// 				/>
	// 			</>
	// 		)}
	// 	</Button>
	// )

	return (
		<>
			<div className={'container__cards'}>{allCards}</div>
		</>
	);
};
export default ContainerCard;
// export default connect()(ContainerCard);

// class ContainerCard extends PureComponent {
// 	state = {
// 		favorite:
// 			JSON.parse(localStorage.getItem('favorite')) ||
// 			localStorage.setItem('favorite', JSON.stringify([])),
// 		basket:
// 			JSON.parse(localStorage.getItem('basket')) ||
// 			localStorage.setItem('basket', JSON.stringify([])),
// 		cardsBody: [],
// 		cardState: null,
// 		opened: false,
// 	};

// 	// const [card, setCard] = useState([]);
// 	// const [favorite, setFavorite] = useState(
// 	// 	JSON.parse(localStorage.getItem('favorite')) ||
// 	// 		localStorage.setItem('favorite', JSON.stringify([]))
// 	// );

// 	// const [basket, setBasket] = useState(
// 	// 	JSON.parse(localStorage.getItem('basket')) ||
// 	// 		localStorage.setItem('basket', JSON.stringify([]))
// 	// );

// 	componentDidMount = () => {
// 		axios('/products.json').then(res => this.setState({ cardsBody: res.data }));
// 	};

// 	saveClick = (id, opened, box, nameBox, cliked) => {
// 		box.push({ id });

// 		localStorage.setItem(`${nameBox}`, JSON.stringify(box));
// 		nameBox === 'basket' ? cliked() : (cliked = undefined);
// 	};

// 	favoriteClick = (id, opened, box, nameBox) => {
// 		const fav = [];
// 		if (!opened) {
// 			// localStorage.setItem(`${id} addFavorite`, !opened);
// 			box.push({ id });
// 			this.setState({ favorite: box });
// 			localStorage.setItem(`${nameBox}`, !opened ? JSON.stringify(box) : '');
// 		} else {
// 			// localStorage.setItem(`${id} addFavorite`, !opened);
// 			const favorite = JSON.parse(localStorage.getItem(`${nameBox}`));
// 			localStorage.removeItem(`${nameBox}`);

// 			favorite.forEach(item => (item.id !== id ? fav.push(item) : ''));

// 			// this.setState({
// 			// 	unName: favorite.filter(item => {
// 			// 		if (item.id !== id) {
// 			// 			fav.push(item);
// 			// 		}
// 			// 	}),
// 			// });

// 			this.setState({ favorite: fav });
// 			localStorage.setItem(`${nameBox}`, JSON.stringify(fav));
// 		}
// 	};
// 	removeCard = (card, opened, nameBox, setOpened) => {
// 		// setOpened(!opened);
// 		const place = [];
// 		const basket = JSON.parse(localStorage.getItem('basket'));

// 		basket.filter(item => (item.id !== card.id ? place.push(item) : ''));
// 		// favorite.forEach(item => (item.id !== id ? fav.push(item) : ''));
// 		localStorage.removeItem(`${'basket'}`);
// 		localStorage.setItem(`${'basket'}`, JSON.stringify(place));
// 		// place.splice(0, place.length);
// 	};

// 	cliked = () => {
// 		const { opened } = this.state;
// 		this.setState({ opened: !opened });
// 	};

// 	render() {
// 		const { basket, favorite, cardsBody, opened } = this.state;
// 		const { card, basketRemove } = this.props;

// 		const { cliked } = this;

// 		const allCards = cardsBody.map(item =>
// 			card === undefined ? (
// 				<Card
// 					card={item}
// 					favorite={favorite}
// 					basket={basket}
// 					basketClick={this.saveClick}
// 					favoriteClick={this.favoriteClick}
// 					key={item.id}
// 				/>
// 			) : (
// 				card.map(cardItem =>
// 					cardItem.id === item.id ? (
// 						<div>
// 							<Card
// 								card={item}
// 								favorite={favorite}
// 								basket={basket}
// 								basketClick={this.saveClick}
// 								favoriteClick={this.favoriteClick}
// 								key={item.id}
// 							/>
// 							{basketRemove && (
// 								<Button name={'X'} onClick={() => this.removeCard(item)}>
// 									<Button name={'X'} onClick={cliked} />
// 									{opened && (
// 										<>
// 											<Modal
// 												key={card.id}
// 												title={'remove?'}
// 												onClick={cliked}
// 												actions={[
// 													<Button
// 														className="card__wrapper-delete"
// 														name={'Ok'}
// 														onClick={() => this.removeCard(item)}
// 													/>,
// 													<Button name={'Cancel'} onClick={cliked} />,
// 												]}
// 											/>
// 										</>
// 									)}
// 								</Button>
// 							)}
// 						</div>
// 					) : (
// 						''
// 					)
// 				)
// 			)
// 		);

// 		return (
// 			<>
// 				<div className={'container__cards'}>{allCards}</div>
// 			</>
// 		);
// 	}
// }
// export default ContainerCard;
