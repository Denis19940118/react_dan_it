import React, { PureComponent, useState } from 'react';
import Modal from '../Modal/Modal';
import Button from '../Button/Button';
import { modal } from '../../utils/field';
import propTypes from 'prop-types';
import './Card.scss';
import Icon from '../Icon/Icon';
import { useEffect } from 'react';

const Card = props => {
	const {
		favorite,
		basket,
		key,
		card,
		basketClick,
		favoriteClick,
		setFavorite,
		favoriteTrue,
		basketCard,
		// setBasket,
	} = props;
	// const [isActive, setIsActive] = useState(false);
	const [opened, setOpened] = useState(false);
	const [addFavorite, setAddFavorite] = useState(false);
	const [isFavorite, setIsFavorite] = useState(false);
	const [isBasket, setIsBasket] = useState(false);
	// const [favoriteTrue, setFavoriteTrue] = useState(
	// 	addFavorite.forEach(cardFav => (cardFav.id === card.id ? true : false))
	// );

	const { name, price, picture, article, id } = card;
	const nameCard = name;

	const { nameModal, title } = modal;
	const nameButton = nameModal;

	useEffect(() => {
		// addFavorite.forEach(cardFav =>
		// 	cardFav.id === card.id ? setIsFavorite(true) : setIsFavorite(false)
		// );
		setIsFavorite(favoriteTrue);
	}, [isFavorite]);

	const basketHandler = () => {
		setIsBasket(!isBasket);
	};
	const favoriteHandler = () => {
		setIsFavorite(!isFavorite);

		// setIsFavorite(!isFavorite);
		// if (!isFavorite) {
		// 	setFavorite([...favorite, id]);
		// 	localStorage.setItem('favorite', JSON.stringify([...favorite, id]));
		// } else {
		// 	setFavorite(favorite.filter(elem => elem !== id));
		// 	localStorage.setItem(
		// 		'favorite',
		// 		JSON.stringify(favorite.filter(elem => elem !== id))
		// 	);
		// }
	};
	const componenty = () => {
		setIsFavorite(!isFavorite);

		let favoriteItem = JSON.parse(localStorage.getItem(`favorite`));
		favoriteItem.some(item =>
			item.id === card.id ? setIsFavorite(favoriteTrue) : ''
		);
	};

	const cliked = () => {
		setOpened(!opened);
	};

	const removeCard = (card, opened, nameBox, setOpened) => {
		const place = [];
		const basket = JSON.parse(localStorage.getItem('basket'));

		basket.filter(item => (item.id !== card.id ? place.push(item) : ''));
		// favorite.forEach(item => (item.id !== id ? fav.push(item) : ''));
		localStorage.removeItem(`${'basket'}`);
		localStorage.setItem(`${'basket'}`, JSON.stringify(place));
		basketHandler();
		// place.splice(0, place.length);
	};

	return (
		<div className={'card'} key={key}>
			<p>{article}</p>
			<img className={'img'} src={picture} alt={picture}></img>
			<span className={'card-name'}>{nameCard}</span>

			<div>
				<Icon
					type={'star'}
					color={'red'}
					id={key}
					filled={isFavorite}
					onClick={
						(() => componenty(),
						(favoriteHandler,
						() => favoriteClick(id, isFavorite, favorite, 'favorite')))
					}
				/>
			</div>

			<div className={'card__box-price'}>
				<span>{price} $Uan</span>
			</div>
			<div>
				<Button name={nameButton} onClick={cliked} />
			</div>
			{opened && (
				<>
					<Modal
						key={key}
						title={title}
						onClick={cliked}
						actions={[
							<Button
								key={key}
								name={'Ok'}
								onClick={
									(basketHandler,
									() => basketClick(id, !opened, basket, 'basket', cliked))
								}
							/>,
							<Button name={'Cancel'} onClick={cliked} />,
						]}
					/>
				</>
			)}
			{basketCard && <Button name={'Delete'} onClick={basketHandler} />}
			{isBasket && (
				<>
					<Modal
						title={'Delete card right now?'}
						actions={[
							<Button
								name={'Ok'}
								onClick={(basketHandler, () => removeCard(card))}
							/>,
							<Button name={'Cancel'} onClick={basketHandler} />,
						]}
					/>
				</>
			)}
		</div>
	);
};

Card.propTypes = {
	name: propTypes.string,
	price: propTypes.string,
	picture: propTypes.string,
	articule: propTypes.string,
	title: propTypes.string,
	nameCard: propTypes.string,
	star: propTypes.func,
};
export default Card;

// class Card extends PureComponent {
// 	state = {
// 		isActive: false,
// 		opened: false,
// 		addFavorite: false,
// 	};
// 	componentDidMount = () => {
// 		const { isActive } = this.state;
// 		const { card } = this.props;
// 		let favorite = JSON.parse(localStorage.getItem(`favorite`));
// 		favorite.some(item =>
// 			item.id === card.id ? this.setState({ isActive: !isActive }) : ''
// 		);
// 		// this.setState({
// 		// 	isActive:
// 		// 		JSON.parse(localStorage.getItem(`${card.id} addFavorite`)) || false,
// 		// });
// 	};
// 	cliked = () => {
// 		const { opened } = this.state;
// 		this.setState({ opened: !opened });
// 	};
// 	changeColor = () => {
// 		const { isActive } = this.state;
// 		this.setState({
// 			isActive: !isActive,
// 		});
// 	};
// 	render() {
// 		const {
// 			favorite,
// 			basket,
// 			key,
// 			card,
// 			basketClick,
// 			favoriteClick,
// 			// basketRemove,
// 			// removeCard,
// 			// setOpened,
// 		} = this.props;
// 		const { name, price, picture, article, id } = card;
// 		const nameCard = name;
// 		const { opened, isActive } = this.state;
// 		const { cliked } = this;
// 		const { nameModal, title } = modal;
// 		const nameButton = nameModal;
// 		return (
// 			<div className={'card'} key={key}>
// 				<p>{article}</p>
// 				<img className={'img'} src={picture} alt={picture}></img>
// 				<span className={'card-name'}>{nameCard}</span>
// 				<div>
// 					<Icon
// 						type={'star'}
// 						color={'red'}
// 						id={key}
// 						onClick={() => {
// 							this.changeColor();
// 							console.log(
// 								'🚀 ~ file: Card.js ~ line 99 ~ Card ~ render ~ isActive',
// 								isActive
// 							);
// 							favoriteClick(id, isActive, favorite, 'favorite');
// 						}}
// 						filled={isActive}
// 					/>
// 				</div>
// 				<div className={'card__box-price'}>
// 					<span>{price} $Uan</span>
// 				</div>
// 				<div>
// 					<Button name={nameButton} onClick={cliked} />
// 				</div>
// 				{opened && (
// 					<>
// 						<Modal
// 							key={key}
// 							title={title}
// 							onClick={cliked}
// 							actions={[
// 								<Button
// 									key={key}
// 									name={'Ok'}
// 									onClick={() =>
// 										basketClick(id, !opened, basket, 'basket', cliked)
// 									}
// 								/>,
// 								<Button name={'Cancel'} onClick={cliked} />,
// 							]}
// 						/>
// 					</>
// 				)}
// 				{/* {basketRemove && (
// 						<Button name={'X'} onClick={() => removeCard(card)}>
// 							<Button name={'X'} onClick={setOpened(!opened)} />
// 							{opened && (
// 								<>
// 									<Modal
// 										key={card.id}
// 										title={'remove?'}
// 										onClick={setOpened(!opened)}
// 										actions={[
// 											<Button
// 												className="card__wrapper-delete"
// 												name={'Ok'}
// 												onClick={() => removeCard(card)}
// 											/>,
// 											<Button name={'Cancel'} onClick={setOpened(!opened)} />,
// 										]}
// 									/>
// 								</>
// 							)}
// 						</Button>
// 					)} */}
// 			</div>
// 		);
// 	}
// }
// Card.propTypes = {
// 	name: propTypes.string,
// 	price: propTypes.string,
// 	picture: propTypes.string,
// 	articule: propTypes.string,
// 	title: propTypes.string,
// 	nameCard: propTypes.string,
// 	star: propTypes.func,
// };
// export default Card;
