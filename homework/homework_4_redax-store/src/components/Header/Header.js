import React, { useState } from 'react';
import SideBar from '../SideBar/SideBar';
import './Header.scss';

const Header = () => {
	const [favorite, setFavorite] = useState(
		JSON.parse(localStorage.getItem('favorite'))
	);
	const [basket, setBasket] = useState(
		JSON.parse(localStorage.getItem('basket'))
	);

	// useEffect(() => {
	// 	setBasket(JSON.parse(localStorage.getItem('basket')));
	// }, [basket]);

	// setFavorit e = () => {
	// 	const { favorite } = this.state;
	// 	if (favorite === null) {
	// 		this.setState({ favorite: [] });
	// 		return favorite;
	// 	} else {
	// 		// this.setState({ favorite: JSON.parse(localStorage.getItem('favorite')) });
	// 		return favorite;
	// 	}
	// };

	// setBasket = () => {
	// 	const { basket } = this.state;
	// 	if (basket === null) {
	// 		this.setState({ basket: [] });
	// 		return basket;
	// 	} else {
	// 		// this.setState({ basket: JSON.parse(localStorage.getItem('basket')) });
	// 		return basket;
	// 	}
	// };

	return (
		<section className={'header'}>
			<SideBar items={['Login', 'Home', 'Favorite', 'Basket', 'Mail']} />
			<p>Favorite :{favorite === null ? [].length : favorite.length}</p>
			<p>Basket :{basket === null ? [].length : basket.length}</p>
			<p>Mail :{favorite === null ? [].length : favorite.length}</p>
		</section>
	);
};
export default Header;
