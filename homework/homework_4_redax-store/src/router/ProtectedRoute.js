import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import PropTypes from 'prop-types';

export const ProtectedRoute = props => {
	const {
		component: ComponentToRoute,
		isAuth,
		protectedRender,
		...restProps
	} = props;

	return (
		<Route
			{...restProps}
			render={routerProps => {
				if (!isAuth) {
					return <Redirect to="/login" />;
				} else if (ComponentToRoute) {
					return <ComponentToRoute {...routerProps} />;
				} else if (protectedRender) {
					return protectedRender(routerProps);
				} else {
					throw new TypeError('No any component to render');
				}
			}}
		/>
	);
};
ProtectedRoute.propTypes = {
	isAuth: PropTypes.bool.isRequired,
	render: PropTypes.func,
};
