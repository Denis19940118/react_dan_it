import { createStore } from 'redux';
import { SAVE_ALL_CARDS, SAVE_USER } from './actionTypes';

const initialStore = {
	user: null,
	cards: [],
	home: [],
	basket: [],
	favorite: [],
	mail: [],
	testProp: 'here we are in redux, finally!',
};

const reducer = (currentStore = initialStore, action) => {
	switch (action.type) {
		case SAVE_USER:
			return {
				...currentStore,
				user: action.payload,
			};
		case SAVE_ALL_CARDS:
			return {
				...createStore,
				...action.payload,
			};

		default:
			return currentStore;
	}
};

export default reducer;
