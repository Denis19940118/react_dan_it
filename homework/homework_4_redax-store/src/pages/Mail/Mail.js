import React from 'react';
import { connect } from 'react-redux';
import ContainerCard from '../../components/ContainerCard/ContainerCard';
// import { selectCards, selectMail } from '../../redux/selectors';
import { selectCards } from '../../redux/selectors';

const Mail = props => {
	const { cards, favorite, setFavorite, basket, setBasket, dispatch } = props;

	const ListToFavoriteCards = [];
	cards.some(card =>
		favorite.forEach(item =>
			card.id === item.id ? ListToFavoriteCards.push(card) : ''
		)
	);
	// const newUser = {login:}
	// dispatch({type:"SAVE_USER", payload: })
	return (
		<>
			{/* <h1>{props.headingText}</h1> */}
			<ContainerCard
				cards={ListToFavoriteCards}
				favorite={favorite}
				setFavorite={setFavorite}
				basket={basket}
				setBasket={setBasket}
				favoriteTrue={true}
			/>
		</>
	);
};
// const mapStateToProps = reduxState => {
// 	return {
// 		headingText: reduxState.testProp,
// 	};
// };
const mapStateToProps = currentStore => ({
	cards: selectCards(currentStore),
	// headingText: reduxState.textProp,
});
// const mapStateToProps = reduxState => ({ mail: selectMail(reduxState) });

// export default Mail;
export default connect(mapStateToProps)(Mail);
