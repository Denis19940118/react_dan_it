import React from 'react';
import { connect } from 'react-redux';
import { selectCards } from '../../redux/selectors';

import ContainerCard from '../../components/ContainerCard/ContainerCard';
// import { selectBasket } from '../../redux/selectors';

const Basket = props => {
	const { cards, favorite, setFavorite, basket, setBasket } = props;

	const ListBasketCard = [];

	cards.some(card =>
		basket.forEach(item =>
			card.id === item.id ? ListBasketCard.push(card) : ''
		)
	);

	// const ListBasketCard = cards.filter(
	// 	({ id }) =>
	// 		console.log(
	// 			'🚀 ~ file: Basket.js ~ line 11 ~ basket.includes(id)',
	// 			// basket.includes(id)
	// 			// id
	// 			basket
	// 		)
	// 	// cards.includes(id));
	// );

	// const [place, setPlace] = useState([]);
	// const [cards, setCards] = useState(
	// 	// []
	// 	JSON.parse(localStorage.getItem('basket') || [])
	// );

	// useEffect(() => {
	// 	setCards();
	// }, [cards]);

	// const removeCard = (card, opened, nameBox, setOpened) => {
	// 	// setOpened(!opened);
	// 	// localStorage.removeItem(`${'basket'}`);
	// 	cards.filter(item => (item.id !== card.id ? place.push(item) : ''));
	// 	// favorite.forEach(item => (item.id !== id ? fav.push(item) : ''));

	// 	localStorage.setItem(`${'basket'}`, JSON.stringify(place));np
	// 	// place.splice(0, place.length);
	// };

	// const basket = JSON.parse(localStorage.getItem('basket'));
	// console.log('🚀 ~ file: Basket.js ~ line 28 ~ Basket ~ basket', basket);

	// const allBasket = basket.map(item => (
	// 	<div>
	// 		<ContainerCard card={item} basketRemove={true} />
	// 		{/* <Button
	// 			className="card__wrapper-delete"
	// 			name={'X'}
	// 			onClick={() => removeCard(item)}
	// 		/> */}
	// 	</div>
	// ));

	return (
		<>
			<ContainerCard
				cards={ListBasketCard}
				favorite={favorite}
				setFavorite={setFavorite}
				basket={basket}
				setBasket={setBasket}
				basketRemove={true}
			/>
		</>
	);
};

// const mapStateToProps = reduxState => ({
// 	basket: selectBasket(reduxState),
// });
const mapStateToProps = currentStore => ({
	cards: selectCards(currentStore),
	// headingText: reduxState.textProp,
});
// export default Basket;
export default connect(mapStateToProps)(Basket);
