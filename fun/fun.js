const arrk = [1, -5, 6, 100, 30, 77, -9, 36];
const arr2 = [1, -5, 6, 100, 30, 77, -9, 36];
const arr3 = [1, -5, 6, 100, 30, 77, -9, 36];

Array.prototype.myMap = function (callback) {
	const result = [];
	const thisArray = this;
	for (let i = 0; i < thisArray.length; i++) {
		result.push(callback(thisArray[i], i, thisArray));
	}
	return result;
};

console.log(
	arrk.myMap((el, i, array) => {
		return el.toString();
	})
);
console.log(
	arr2.map((el, i, array) => {
		return el.toString();
	})
);
console.log(
	arr2.sort((a, b) => {
		return a - b;
	})
);
