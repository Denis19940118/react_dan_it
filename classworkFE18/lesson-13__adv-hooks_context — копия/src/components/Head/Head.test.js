import Head from "./Head";
import {fireEvent} from '@testing-library/react';

jest.mock("./NavList/NavList", () => ({items}) => (<>
  {items.map((item, k) => <a data-testid="list" key={k} href='#'>{item}</a>)}
</>));
jest.mock("../../theme/icons/Icons", () => () => (<>Icon</>));
jest.mock("../NavButton/NavButton", () => () => (<button>Button</button>));
jest.mock("react-router-dom", () => ({
  Link: 'a',
  withRouter: (Component) => (props) => <Component {...props}/>
}));

describe("testing head", () => {

  test("smoke Head", () => {
    const goBackMock = jest.fn();
    const {
      getByText,
      getByTestId
    } = render(<Head history={{
      goBack: goBackMock
    }}/>);

    expect(getByText('Button')).toBeInTheDocument();
    expect(getByText('Icon')).toBeInTheDocument();

    fireEvent.click(getByText('go back'));

    expect(goBackMock).toBeCalled();
  })
});