import NavList from "./NavList";

jest.mock("../../NavLink/NavLink", () => ({ text }) => <a href="#">{text}</a>);

test("NavList rendered correctly", () => {
  render(<NavList items={["gogi"]} />);
});

test("NavList rendered all of the links", () => {
  const links = ["1", "2", "3", "4", "5", "6"];
  const { getByText } = render(<NavList items={links} />);

  links.forEach((l) => {
    expect(getByText(l)).toBeDefined();
    expect(getByText(l)).toHaveAttribute("href", "#");
  });
});

test("NavList snapshot", () => {
  const { container } = render(<NavList items={["gogi"]} />);
  expect(container).toMatchSnapshot();
});
test("NavList inline snapshot", () => {
  const { container } = render(<NavList items={["gogi"]} />);
  expect(container).toMatchInlineSnapshot(`
    <div>
      <ul
        class="header__nav-list"
      >
        <li
          class="header__nav-item"
        >
          <a
            href="#"
          >
            gogi
          </a>
        </li>
      </ul>
    </div>
  `);
});
