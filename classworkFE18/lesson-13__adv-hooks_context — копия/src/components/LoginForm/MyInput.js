import React from 'react';

const MyInput = ({input, meta, ...rest}) => {
  const {touched, error} = meta;

  return (
    <div>
      <input {...input} {...rest}/>
      {
        error && touched && <span>{error}</span>
      }
    </div>
  );
};

export default MyInput;