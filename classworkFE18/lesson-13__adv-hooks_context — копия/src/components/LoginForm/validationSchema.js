import * as yup from 'yup';

const loginSchema = yup.object().shape({
  login: yup
    .string()
    .email('this is not a valid email, dummy!')
    .required('this field is required'),
  password: yup
    .string()
    .required('this field is required')
    .min(5, 'Min length is 5'),
  confirmPassword: yup
    .string()
    .required('this field is required')
    .min(5, 'Min length is 5')
});

const registrationSchema = yup.object().shape({
  emailField: yup
    .string()
    .email('this is not a valid email, dummy!')
    .required('this field is required'),
  password: yup
    .string()
    .required('this field is required')
    .min(5, 'Min length is 5'),
  confirmPassword: yup
    .string()
    .required('this field is required')
    .min(5, 'Min length is 5'),
  gender: yup
    .string()
    .required()
    .oneOf(['male', 'female', 'other'], 'wrong gender!!!!'),
  age: yup
    .number()
    .required()
    .min(18, 'to young'),
  about: yup
    .string()
    .max(200, 'Too many symbols!')
});

export default {
  loginSchema,
  registrationSchema
}