import {shallow} from 'enzyme';
import createMockStore from 'redux-mock-store';
import {fireEvent} from '@testing-library/react';
import NewEmail from "./NewEmail";
import thunk from "redux-thunk";
import {Provider} from "react-redux";

jest.mock('react-router-dom', () => ({
  withRouter: Component => props => <Component {...props}/>
}));

const store = createMockStore([thunk])({});

test("NewEmail rendered with empty redux", () => {
  const submitHandler = jest.fn();

  const {
    getByText
  } = render(<Provider store={store}><NewEmail/></Provider>);
  const sendBtn = getByText('Send');

  expect(sendBtn).toBeInTheDocument();

  fireEvent.click(sendBtn);
});