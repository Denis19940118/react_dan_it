import React, {useContext} from 'react';
import ThemeContext from "../../context/theme/themeContext";

const ThemeChangeForm = () => {
  const [currentTheme, setTheme] = useContext(ThemeContext);

  const handleSubmit = (e) => {
    e.preventDefault();
    const colors = new FormData(e.target);
    setTheme({
      primary: colors.get('primary'),
      secondary: colors.get('secondary')
    });
  };

  return (
    <form onSubmit={handleSubmit}>
      <input name='primary' type="color"/>
      <input name='secondary' type="color"/>
      <input type="submit" value={'Change theme'}/>
    </form>
  );
};

export default ThemeChangeForm;