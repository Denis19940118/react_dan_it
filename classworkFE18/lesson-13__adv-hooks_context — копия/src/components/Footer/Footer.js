//react stateless component - rsc
import React, {memo, useState} from 'react';

const Footer = ({name="Anonymous", changeAuthorName, handleClickProp}) => {
  // console.log('Footer state - ',useState('gogi'));
  // console.log('footer context',this);
  const handleClick = () => {
    const newName = prompt("new person name");
    changeAuthorName(newName);
  };

  return (
    <footer>
      All rights are stolen by - <button onClick={handleClickProp || handleClick} data-testid={'userNameBtn'}>{name}</button>
    </footer>
  );
};

export default memo(Footer);