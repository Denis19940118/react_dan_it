import Footer from "./Footer";
import {fireEvent} from '@testing-library/react';
import {unmountComponentAtNode} from "react-dom";

test('Footer rendered successfully', () => {
  render(<Footer name="Gogi"/>);
});

test('Footer contains user name', () => {
  const userName = "Gogi";
  const {
    getByText
  } = render(<Footer name={userName}/>);

  expect(getByText(userName)).toBeDefined();
});

test('Footer rendered default user name', () => {
  const {
    getByTestId
  } = render(<Footer/>);
  const userBtn = getByTestId('userNameBtn');

  expect(userBtn).toBeDefined();
  expect(userBtn.textContent).toBe("Anonymous");
});

test("Footer btn called click handler", () => {
  const mockedFunction = jest.fn();
  const {
    getByTestId
  } = render(<Footer handleClickProp={mockedFunction}/>);
  const btn = getByTestId('userNameBtn');

  fireEvent.click(btn);

  expect(mockedFunction).toBeCalled();
});