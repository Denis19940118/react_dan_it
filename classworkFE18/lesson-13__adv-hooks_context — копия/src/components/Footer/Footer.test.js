import Footer from "./Footer";
import {unmountComponentAtNode, render} from "react-dom";

let container = null;

beforeEach(() => {
  container = document.createElement('div');
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

test('Footer rendered successfully', () => {
  act(() => {
    render(<Footer name="Gogi"/>, container);
  });
});

test('Footer contains user name', () => {
  act(() => {
    render(<Footer name="Gogi"/>, container);
  });

  const buttonUserName = container.getElementsByTagName('button')[0];

  expect(buttonUserName.textContent).toBe("Gogi");
});

test('Footer rendered default user name', () => {
  act(() => {
    render(<Footer/>, container);
  });
  const buttonUserName = container.getElementsByTagName('button')[0];

  expect(buttonUserName.textContent).toBe("Anonymous");
});