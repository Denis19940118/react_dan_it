import React from 'react';
import {ErrorMessage, Field, Form, withFormik} from "formik";
import schemas from "../LoginForm/validationSchema";

const RegisterForm = ({isSubmitting, isValid}) => {
  return (
    <Form className="login">
      <Field
        component="input"
        type="email"
        name="emailField"
        placeholder="Enter email"
      />
      <ErrorMessage name="email"/>
      <Field
        component="input"
        type="password"
        name="password"
        placeholder="Enter password"
      />
      <ErrorMessage name="password"/>
      <Field
        component="input"
        type="password"
        name="confirmPassword"
        placeholder="Enter password again"
      />
      <ErrorMessage name="confirmPassword"/>
      <Field
        component="input"
        type="text"
        name="gender"
        placeholder="Enter your gender"
      />
      <ErrorMessage name="gender"/>
      <Field
        component="input"
        type="number"
        name="age"
        placeholder="Enter your age"
      />
      <ErrorMessage name="age"/>
      <Field
        component="textarea"
        name="about"
        placeholder="Write about yourself"
      />
      <ErrorMessage name="about"/>
      <button type='submit' disabled={!isValid || isSubmitting}>register</button>
    </Form>
  );
};

export default withFormik({
  mapPropsToValues: props => ({
    emailField: 'ddd',
    password: '',
    confirmPassword: '',
    gender: '',
    age: '',
    about: ''
  }),
  validationSchema: schemas.registrationSchema,
  onSubmit: (...args) => {
    console.log(args);
  }
})(RegisterForm);