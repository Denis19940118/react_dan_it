import {useEffect, useState} from "react";

const useToggle = (initialValue) => {
  const [isOpen, setIsOpen] = useState(initialValue);

  const makeToggle = () => setIsOpen(!isOpen);

  return [isOpen, makeToggle, setIsOpen];
};

export default useToggle;