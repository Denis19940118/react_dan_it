import './App.scss';
import './components/Email/Emails.scss'
import Head from "./components/Head/Head";
import React, {useCallback, useEffect, useReducer} from "react";
import AppRoutes from "./routes/AppRoutes";
import Footer from "./components/Footer/Footer";
import {connect, useDispatch, useSelector} from "react-redux";
import NewEmail from "./components/NewEmail/NewEmail";
import {emailsActions, emailsSelectors} from "./redux/emails";
import useWinSize from "./utils/useWinSize";

const authorNameReducer = (state, action) => {
  switch(action.type) {
    case 'SET_AUTHOR_NAME':
      return action.payload;
    default:
      return state
  }
};

const App = ({isEmailModalOpen}) => {
  const [authorName, dispatchAuthorName] = useReducer(authorNameReducer, "Huan Carlos Garsiza Venom");
  const winSize = useWinSize();

  const handleAuthorNameChange = useCallback((newName) => dispatchAuthorName({
    type: 'SET_AUTHOR_NAME',
    payload: newName
  }), [dispatchAuthorName]);

  return (
    <>
      <Head/>

      {
        winSize.width > 768 && <h1>desktop</h1>
      }
      {
        winSize.width < 768 && <h1>tablet</h1>
      }

      {isEmailModalOpen && <NewEmail />}

      <div className="container emails">
        <AppRoutes/>
      </div>

      <Footer name={authorName} changeAuthorName={handleAuthorNameChange}/>

    </>
  );
};

const mapStateToProps = reduxState => ({
  isEmailModalOpen: emailsSelectors.isEmailModalOpen(reduxState)
});

export default connect(mapStateToProps)(App);