import React, {useEffect} from 'react';
import EmailsList from "../../components/EmailsList/EmailsList";
import {useDispatch, useSelector} from "react-redux";
import {emailsActions, emailsSelectors} from "../../redux/emails";
import {fetchEmails} from "../../utils/API";
import ThemeChangeForm from "../../components/ThemeChangeForm/ThemeChangeForm";

const InboxPage = () => {
  const dispatch = useDispatch();
  const inboxEmails = useSelector(emailsSelectors.inbox);

  useEffect(() => {
    dispatch(emailsActions.saveCollectionAsync('inbox'));
  }, []);

  return (<>
    <ThemeChangeForm/>
    <EmailsList emails={inboxEmails}/>
  </>);
};

export default InboxPage;