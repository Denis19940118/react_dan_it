import React from 'react';
import * as AllIcons from './index';

const Icons = ({type, color, classes, ...restProps}) => {
  const currentIcon = AllIcons[type];

  if (!currentIcon) {
    return null
  }

  return (<div className={classes} {...restProps}>
    {currentIcon(color, classes)}
  </div>);
};

export default Icons;