import {allEmailsReducer, toggleModalReducer, initialState} from "./reducers";
import types from './types';

describe('allEmailsReducer tests', () => {
  test('allEmailsUser default case', () => {
    const init = {gogi: 'gogi'};

    const reducerReturns = allEmailsReducer(init, {type: "GOGI_MURDERED_BY_POLICE"});

    expect(reducerReturns).toEqual(init);
  });

  test(`${types.SAVE_ALL_MAILS} saved all emails`, () => {
    const allEmails = {...initialState.all, inbox: [1,2,3]};
    const reducerReturns = allEmailsReducer(initialState.all, {
      type: types.SAVE_ALL_MAILS,
      payload: allEmails
    });

    expect(reducerReturns).toEqual(allEmails);
  });

  test(`${types.SAVE_ALL_MAILS} checked props of payload`, () => {
    const allEmails = {
      dummyEmails: 'dummyEmail'
    };
    const reducerReturns = allEmailsReducer(initialState.all, {
      type: types.SAVE_ALL_MAILS,
      payload: allEmails
    });

    expect(reducerReturns).toHaveProperty('inbox', []);
    expect(reducerReturns).toHaveProperty('sent', []);
    expect(reducerReturns).toHaveProperty('draft', []);
    expect(reducerReturns).toHaveProperty('spam', []);
  });
});