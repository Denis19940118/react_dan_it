const selectInbox = currentStore => currentStore.emails.all.inbox;
const selectSent = currentStore => currentStore.emails.all.sent;
const selectDraft = currentStore => currentStore.emails.all.draft;
const selectSpam = currentStore => currentStore.emails.all.spam;
const selectIsEmailModalOpen = currentStore => currentStore.emails.isEmailModalOpen;

export default {
  inbox: selectInbox,
  sent: selectSent,
  draft: selectDraft,
  spam: selectSpam,
  isEmailModalOpen: selectIsEmailModalOpen
}