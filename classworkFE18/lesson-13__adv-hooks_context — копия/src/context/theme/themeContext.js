import {createContext} from "react";
import themes from "./themes";

const ThemeContext = createContext(themes.light);

export default ThemeContext;

export const ThemeProvider = ThemeContext.Provider;
export const ThemeConsumer = ThemeContext.Consumer;