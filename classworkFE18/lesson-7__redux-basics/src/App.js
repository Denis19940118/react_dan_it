import './App.scss';
import './components/Email/Emails.scss';
import Head from './components/Head/Head';
import React, { useState, useEffect } from 'react';
import SideBar from './components/SideBar/SideBar';
import AppRoutes from './routes/AppRoutes';
import Footer from './components/Footer/Footer';

const App = () => {
	const [currentUser, setCurrentUser] = useState(null);
	const [inbox, setInbox] = useState([]);
	const [sent, setSent] = useState([]);
	const [draft, setDraft] = useState([]);
	const [spam, setSpam] = useState([]);

	const saveUser = user => {
		setCurrentUser({ currentUser: user });
	};

	useEffect(() => {
		fetch('/api/emails', {
			headers: {
				'Content-Type': 'application/json',
			},
		})
			.then(r => {
				console.log('response', r);
				return r.json();
			})
			.then(data => {
				console.log(data);
				setInbox(data.inbox);
				setSent(data.sent);
				setDraft(data.draft);
				setSpam(data.spam);
			});

		return () => {
			console.log('App will unmount');
		};
	}, []);

	return (
		<>
			<Head />

			<div className="container emails">
				<AppRoutes
					user={currentUser}
					auth={u => saveUser(u)}
					inbox={inbox}
					sent={sent}
					draft={draft}
					spam={spam}
				/>
				{/*<EmailsList emails={sent}/>*/}'
			</div>

			<Footer name={'Huan Carlos Garsiza Venom'} />
		</>
	);
};

export default App;

const Random = () => <h1>Random component for example</h1>;
