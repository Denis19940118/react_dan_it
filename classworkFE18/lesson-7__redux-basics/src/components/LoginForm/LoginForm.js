import React, { Component, useRef } from 'react';
import { connect } from 'react-redux';
import { SAVE_USER } from '../../redux/actionTypes';

const credentials = {
	login: 'admin',
	password: '123',
};

const LoginForm = ({
	history,
	location,
	match,
	saveUser,
	// auth, dispatch
}) => {
	let loginRef = useRef();
	let passwordRef = useRef();

	const handleSubmit = event => {
		event.preventDefault();

		if (
			loginRef.value === credentials.login &&
			passwordRef.value === credentials.password
		) {
			const newUser = { login: loginRef.value, password: passwordRef.value };
			// auth({ newUser });
			// dispatch({ type: 'SAVE_USER', payload: newUser });
			saveUser(newUser);
		}
	};

	const mapInputLogin = loginInput => {
		loginRef = loginInput;
	};

	const mapPasswordInput = password => {
		passwordRef = password;
	};

	return (
		<form onSubmit={event => handleSubmit(event)} className="login" action="">
			<input
				ref={loginInput => mapInputLogin(loginInput)}
				type="text"
				className="login__input"
				placeholder="Enter login"
			/>
			<input
				ref={password => mapPasswordInput(password)}
				type="password"
				className="login__input"
				placeholder="Enter password"
			/>
			<button type="submit" className="header__login-btn">
				Login
			</button>
		</form>
	);
};

// const mapStateToProps = reduxState => ({ user: reduxState.user });

const mapDispatchToProps = dispatch => ({
	saveUser: user => dispatch(saveUserAction(user)),
});
export default connect(() => ({}), mapDispatchToProps)(LoginForm);
