import { SAVE_USER } from '../actionTypes';

export const saveUserAction = user => ({
	type: SAVE_USER,
	payload: user,
});
