const initialStore = {
	user: null,
	inbox: [],
	sent: [],
	draft: [],
	spam: [],
	testProp: 'here we are in redux, finally!',
};

const reducer = (currentStore = initialStore, action) => {
	switch (action.type) {
		case 'SAVE_USER':
			return {
				...currentStore,
				user: action.payload,
			};
		default:
			return currentStore;
	}
};

export default reducer;
