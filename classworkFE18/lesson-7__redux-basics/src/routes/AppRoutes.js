import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import EmailPage from '../pages/EmailPage/EmailPage';
import Page404 from '../pages/Page404/Page404';
import InboxPage from '../pages/InboxPage/InboxPage';
import SentPage from '../pages/SentPage/SentPage';
import DraftPage from '../pages/DraftPage/DraftPage';
import SpamPage from '../pages/SpamPage/SpamPage';
import LoginForm from '../components/LoginForm/LoginForm';
import ProtectedRoute from './ProtectedRoute';
import SideBar from '../components/SideBar/SideBar';
import { connect } from 'react-redux';

class AppRoutes extends Component {
	render() {
		const { inbox, sent, draft, spam, isAuth } = this.props;
		// const isAuth = !!user;

		return (
			<>
				<ProtectedRoute
					path="/"
					isAuth={isAuth}
					protectedRender={() => (
						<SideBar
							items={['Inbox', 'Sent', 'Draft', 'Spam', 'Random/4848415']}
						/>
					)}
				/>
				<Switch>
					<Route
						exact
						path="/login"
						render={routerProps => {
							if (isAuth) {
								return <Redirect to="/inbox" />;
							}
							return <LoginForm {...routerProps} />;
						}}
					/>
					<ProtectedRoute
						exact
						path="/inbox"
						isAuth={isAuth}
						protectedRender={() => <InboxPage inboxEmails={inbox} />}
					/>
					<ProtectedRoute
						exact
						path="/sent"
						isAuth={isAuth}
						protectedRender={() => <SentPage sentEmails={sent} />}
					/>
					<ProtectedRoute
						exact
						path="/draft"
						isAuth={isAuth}
						protectedRender={() => <DraftPage draftEmails={draft} />}
					/>
					<ProtectedRoute
						exact
						path="/spam"
						isAuth={isAuth}
						protectedRender={() => <SpamPage spamEmails={spam} />}
					/>
					{/*<Route exact path="/emails/:emailID" component={EmailPage}/>*/}
					<ProtectedRoute
						exact
						isAuth={isAuth}
						path="/emails/:emailID"
						component={EmailPage}
					/>
					{/*<Route exact path="/emails/:emailID" render={(props) => <EmailPageDummy allEmails={allEmails} {...props}/>}/>*/}
					<Route path="*" component={Page404} />
				</Switch>
				{/*<Route path="/random/:counterOfRandom" component={Random}/>*/}
			</>
		);
	}
}

const mapStateToProps = currentStore => ({ isAuth: !!currentStore.user });

export default connect(mapStateToProps)(AppRoutes);
