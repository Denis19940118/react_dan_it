import React, {Component} from 'react';
import EmailsList from "../../components/EmailsList/EmailsList";
import PropTypes from "prop-types";

class DraftPage extends Component {
  render() {
    const {draftEmails} = this.props;

    return (<EmailsList emails={draftEmails}/>);
  }
}

export default DraftPage;

DraftPage.propTypes = {
  draftEmails: PropTypes.array.isRequired
};