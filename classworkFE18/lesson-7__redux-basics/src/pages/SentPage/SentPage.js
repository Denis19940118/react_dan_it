import React, {Component} from 'react';
import EmailsList from "../../components/EmailsList/EmailsList";
import PropTypes from "prop-types";

class SentPage extends Component {
  render() {
    const {sentEmails} = this.props;

    return (<EmailsList emails={sentEmails}/>);
  }
}

export default SentPage;

SentPage.propTypes = {
  sentEmails: PropTypes.array.isRequired
};