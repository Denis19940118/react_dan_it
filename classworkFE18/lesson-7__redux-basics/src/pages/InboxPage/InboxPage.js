import React, { Component } from 'react';
import EmailsList from '../../components/EmailsList/EmailsList';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class InboxPage extends Component {
	render() {
		const { inboxEmails } = this.props;

		return (
			<>
				{/* <h1>{this.props.headingText}</h1> */}

				<EmailsList emails={inboxEmails} />
			</>
		);
	}
}

const mapStateToProps = reduxState => {
	return {
		headingText: reduxState.testProp,
	};
};
export default connect()(InboxPage);

InboxPage.propTypes = {
	inboxEmails: PropTypes.array.isRequired,
};
