import React, {Component} from 'react';
import EmailsList from "../../components/EmailsList/EmailsList";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {emailsSelectors} from "../../redux/emails";

class DraftPage extends Component {
  render() {
    const {draftEmails} = this.props;

    return (<EmailsList emails={draftEmails}/>);
  }
}

const mapStateToProps = reduxState => ({
  draftEmails: emailsSelectors.draft(reduxState)
});

export default connect(mapStateToProps)(DraftPage);

DraftPage.propTypes = {
  draftEmails: PropTypes.array.isRequired
};