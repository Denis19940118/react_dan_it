import React, {Component} from 'react';
import EmailsList from "../../components/EmailsList/EmailsList";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {emailsSelectors} from "../../redux/emails";

class SentPage extends Component {
  render() {
    const {sentEmails} = this.props;

    return (<EmailsList emails={sentEmails}/>);
  }
}

const mapStateToProps = (reduxState) => ({
  sentEmails: emailsSelectors.sent(reduxState)
});

export default connect(mapStateToProps)(SentPage);

SentPage.propTypes = {
  sentEmails: PropTypes.array.isRequired
};