import React, {Component} from 'react';
import EmailsList from "../../components/EmailsList/EmailsList";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {emailsSelectors} from "../../redux/emails";

class InboxPage extends Component {
  render() {
    const {inboxEmails} = this.props;

    return (<>
      <EmailsList emails={inboxEmails}/>
    </>);
  }
}

const mapStateToProps = (reduxState) => {
  return {
    inboxEmails: emailsSelectors.inbox(reduxState),
  }
};

export default connect(mapStateToProps)(InboxPage);

InboxPage.propTypes = {
  inboxEmails: PropTypes.array.isRequired
};