import React, {Component} from 'react';
import Email from "../../components/Email/Email";
import {withRouter} from "react-router-dom";

class EmailPage extends Component {
  state = {
    email: {}
  };

  componentDidMount() {
    fetch('/emails.json')
      .then(r => r.json())
      .then(allEmails => {
        const currentEmail = Object.values(allEmails)
          .flat()
          .find(email => email.id === this.props.match.params.emailID);

        this.setState({email: currentEmail})
      })
  }

  render() {
    const {email} = this.state;

    return (email && <Email self={email}/>);
  }
}

export default withRouter(EmailPage);