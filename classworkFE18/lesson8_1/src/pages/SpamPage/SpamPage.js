import React, {Component} from 'react';
import EmailsList from "../../components/EmailsList/EmailsList";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {emailsSelectors} from "../../redux/emails";

class SpamPage extends Component {
  render() {
    const {spamEmails} = this.props;

    return (<EmailsList emails={spamEmails}/>);
  }
}

const mapStateToProps = reduxState => ({
 spamEmails: emailsSelectors.spam(reduxState)
});

export default connect(mapStateToProps)(SpamPage);

SpamPage.propTypes = {
  spamEmails: PropTypes.array.isRequired
};