import './App.scss';
import './components/Email/Emails.scss'
import Head from "./components/Head/Head";
import React, {useEffect} from "react";
import AppRoutes from "./routes/AppRoutes";
import Footer from "./components/Footer/Footer";
import {connect} from "react-redux";
import NewEmail from "./components/NewEmail";
import {emailsActions, emailsSelectors} from "./redux/emails";

const App = ({saveAllEmails, isEmailModalOpen}) => {

  useEffect(() => {
    fetch('/api/emails', {
      headers: {
        "Content-Type": 'application/json'
      }
    })
      .then(r => {
        console.log('response', r);
        return r.json()
      })
      .then(data => {
        saveAllEmails(data);
      });

    return () => {
      console.log('App will unmount');
    }
  }, []);

  return (
    <>
      <Head/>

      {isEmailModalOpen && <NewEmail />}

      <div className="container emails">
        <AppRoutes/>
      </div>

      <Footer name={"Huan Carlos Garsiza Venom"}/>

    </>
  );
};

const mapDispatchToProps = dispatch => ({
  saveAllEmails: allEmails => dispatch(emailsActions.saveAllEmails(allEmails))
});

const mapStateToProps = reduxState => ({
  isEmailModalOpen: emailsSelectors.isEmailModalOpen(reduxState)
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
