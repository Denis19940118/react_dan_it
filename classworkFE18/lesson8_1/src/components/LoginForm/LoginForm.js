import React, {useRef} from 'react';
import {connect} from "react-redux";
import {userActions} from "../../redux/user";

const credentials = {
  login: 'admin',
  password: '123',
};

const LoginForm = ({history, location, match, saveUser}) => {
  let loginRef = useRef();
  let passwordRef = useRef();

  const handleSubmit = (event) => {
    event.preventDefault();

    if (loginRef.value === credentials.login &&
      passwordRef.value === credentials.password) {
      const newUser = {login: loginRef.value, password: passwordRef.value};
      saveUser(newUser);
    }
  };

  const mapInputLogin = (loginInput) => {
    loginRef = loginInput;
  };

  const mapPasswordInput = (password) => {
    passwordRef = password;
  };

  return (
    <form onSubmit={(event) => handleSubmit(event)} className="login" action="">
      <input ref={loginInput => mapInputLogin(loginInput)} type="text" className="login__input"
             placeholder="Enter login"/>
      <input ref={password => mapPasswordInput(password)} type="password" className="login__input"
             placeholder="Enter password"/>
      <button type="submit" className="header__login-btn">Login</button>
    </form>
  );
};

const mapDispatchToProps = (dispatch) => ({
  saveUser: (user) => dispatch(userActions.saveUser(user))
});

export default connect(() => ({}), mapDispatchToProps)(LoginForm);