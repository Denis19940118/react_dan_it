import React, {useRef} from 'react';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {emailsActions, emailsSelectors} from "../redux/emails";

let NewEmail = ({history, closeEmailModal, isEmailModalOpen}) => {
  let subject = useRef(null);
  let to = useRef(null);
  let text = useRef(null);

  const sendNewEmail = (newEmail, form) => {
    fetch('/api/emails/sent', {
      method: 'POST',
      headers: {
        "Content-Type": 'application/json'
      },
      body: JSON.stringify(newEmail)
    })
      .then(r => r.json())
      .then((response) => {
        form.reset();
        closeEmailModal(isEmailModalOpen);
        history.push('/sent');
      })
  };

  const handleSubmit = event => {
    event.preventDefault();
    const newEmail = {
      subject: subject.value,
      to: to.value,
      text: text.value
    };
    sendNewEmail(newEmail, event.target);
  };

  return (
    <form onSubmit={handleSubmit}>
      <input ref={ref => subject = ref} type='text' placeholder='subject'/>
      <input ref={ref => to = ref} type='text' placeholder='to'/>
      <textarea ref={ref => text = ref} placeholder='subject'/>
      <button type='submit'>Send</button>
    </form>
  );
};

NewEmail = withRouter(NewEmail);

const mapStateToProps = reduxState => ({
  isEmailModalOpen: emailsSelectors.isEmailModalOpen(reduxState)
});

const mapDispatchToProps = dispatch => ({
  closeEmailModal: (currentValue) => dispatch(emailsActions.toggleEmailsModal(currentValue))
});

export default connect(mapStateToProps, mapDispatchToProps)(NewEmail);