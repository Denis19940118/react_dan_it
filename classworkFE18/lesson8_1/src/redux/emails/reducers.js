import {combineReducers} from "redux";
import types from './types';

const initialState = {
  all: {
    inbox: [],
    sent: [],
    draft: [],
    spam: [],
  },
  isEmailModalOpen: false,
};

const allEmailsReducer = (currentState = initialState.all, action) => {
  switch (action.type) {
    case types.SAVE_ALL_MAILS:
      return {
        ...action.payload
      };
    default:
      return currentState
  }
};

const toggleModalReducer = (currentState = initialState.isEmailModalOpen, action) => {
  switch (action.type) {
    case types.TOGGLE_EMAIL_MODAL:
      return action.payload;
    default:
      return currentState
  }
};

export default combineReducers({
  all: allEmailsReducer,
  isEmailModalOpen: toggleModalReducer
})
