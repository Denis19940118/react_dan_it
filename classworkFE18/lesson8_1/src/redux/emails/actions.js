import types from './types';

const saveAllEmailsAction = emails => ({
  type: types.SAVE_ALL_MAILS,
  payload: emails
});

const toggleMailModalAction = isEmailModalOpen => ({
  type: types.TOGGLE_EMAIL_MODAL,
  payload: !isEmailModalOpen
});

export default {
  saveAllEmails: saveAllEmailsAction,
  toggleEmailsModal: toggleMailModalAction
}