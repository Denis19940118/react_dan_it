import types from './types'

const saveUserAction = (user) => ({
  type: types.SAVE_USER,
  payload: user
});

export default {
  saveUser: saveUserAction
}