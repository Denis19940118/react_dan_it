import {combineReducers} from "redux";
import user from './user/index'
import emails from './emails/index'

export default combineReducers({
  user,
  emails
});