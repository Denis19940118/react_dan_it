import React from 'react';
import { Link } from 'react-router-dom';
import classes from './CssModules.module.scss';
import styled, { css } from 'styled-components';

export const CssModules = () => {
	return (
		<div
			className={[classes.block, classes['block-padding'], 'my-container'].join(
				' '
			)}
		>
			<h4>Lorem ipsum dolor sit</h4>
			<MyLink to="/" className="classes.link">
				Go here
			</MyLink>
			<MyButton isActive={true}>Gogi button styled</MyButton>
		</div>
	);
};

const MyLink = styled(Link)`
	font-size: 48px;
`;

const MyButton = styled.button`
	padding: 25;
	border: none;
	background-color: #9c9c9c;
	color: #fff;

	${props =>
		props.isActive &&
		css`
			background-color: deeppink;
		`}
	@media (max-width: 768) {
		font-size: 12px;
	}
`;
