import React, {useState, useEffect} from 'react';
import {useDispatch} from "react-redux";
import {userActions} from "../../redux/user";
import {reduxForm} from "redux-form";
import MyInput from "./MyInput";
import schemas from './validationSchema';
import {Formik, Form, Field, ErrorMessage} from "formik";
import MyInputFormik from "./MyInputFormik";

const LoginForm = ({history, location, match, isRegister}) => {
  const dispatch = useDispatch();

  const handleSubmit = (values, {setSubmitting}) => {
    if (isRegister) {
      dispatch(userActions.register(values));
    } else {
      dispatch(userActions.login(values, setSubmitting));
    }
  };

  return (
    <Formik
      initialValues={{
        login: 'admin',
        password: '',
        confirmPassword: ''
      }}
      validate={validateForm}
      validationSchema={schemas.loginSchema}
      onSubmit={handleSubmit}
    >
      {formikProps => {
        console.log(formikProps);
        return <Form noValidate className="login" action="">
          <Field component={MyInputFormik}
                 type='email'
                 name='login'
                 placeholder='enter login'/>
          <Field component={MyInputFormik}
                 type='password'
                 name='password'
                 placeholder='enter password'/>
          <Field component={MyInputFormik}
                 type='password'
                 name='confirmPassword'
                 placeholder='enter password'/>
          <button type="submit" disabled={!formikProps.isValid || formikProps.isSubmitting} className="header__login-btn">
            {
              isRegister ? 'save' : 'login'
            }
          </button>
        </Form>
      }}
    </Formik>
  );
};

const validateForm = (values) => {
  const errors = {};
  if (!values.confirmPassword) {
    errors.confirmPassword = 'Required'
  } else if (values.password !== values.confirmPassword) {
    errors.confirmPassword = 'passwords are not matched'
  }
  return errors
};

export default LoginForm;