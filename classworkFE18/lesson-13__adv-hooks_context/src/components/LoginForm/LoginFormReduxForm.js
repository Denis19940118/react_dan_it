import React, {useState, useEffect} from 'react';
import {useDispatch} from "react-redux";
import {userActions} from "../../redux/user";
import {Field, reduxForm} from "redux-form";
import MyInput from "./MyInput";

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
const requiredInput = (input) => !input && 'Field required';
const isEmail = (input) => !EMAIL_REGEX.test(input) && 'Please enter a valid email';
const passwordMatch = (input, values) => input !== values.password && 'Passwords do not match';

const LoginForm = ({history, location, match, isRegister, handleSubmit, onSubmit}) => {
  const dispatch = useDispatch();
  
  return (
    <form noValidate onSubmit={handleSubmit} className="login" action="">
      <Field component={MyInput}
             type='email'
             name='login'
             validate={[requiredInput, isEmail]}
             placeholder='enter login'/>
      <Field component={MyInput}
             type='password'
             name='password'
             validate={[requiredInput]}
             placeholder='enter password'/>
      <Field component={MyInput}
             type='password'
             name='confirmPassword'
             validate={[requiredInput, passwordMatch]}
             placeholder='enter password'/>
      <button type="submit" className="header__login-btn">
        {
          isRegister ? 'save' : 'login'
        }
      </button>
    </form>
  );
};

// const validateForm = (values) => {
//   const errors = {};
//   if (!values.login) {
//     errors.login = 'Required'
//   } else if (values.login.length < 5) {
//     errors.login = 'too short'
//   }
//   if (!values.password) {
//     errors.password = 'Required'
//   } else if (values.password.length < 5) {
//     errors.password = 'too short'
//   }
//   return errors
// }

export default reduxForm({
  form: 'login',
  initialValues: {
    login: 'admin@go.com'
  },
  // validate: validateForm
})(LoginForm);