import React, {useState, useEffect} from 'react';
import {useDispatch} from "react-redux";
import {userActions} from "../../redux/user";

const LoginForm = ({history, location, match, isRegister}) => {
  const dispatch = useDispatch();
  const [values, setValues] = useState({
    login: '',
    password: ''
  });
  const [errors, setErrors] = useState({
    login: false,
    password: false
  });
  const [touched, setTouched] = useState({
    login: false,
    password: false
  })
  
  const handleSubmit = (event) => {
    event.preventDefault();
    
    if (isRegister && validateValues()) {
      dispatch(userActions.register(values));
    } else {
      dispatch(userActions.login(values));
    }
  };
  
  const handleChange = (name, e) => {
    setValues({
      ...values,
      [name]: e.target.value
    })
  }
  
  const validateValues = () => {
    const newErrors = {}
    Object.keys(values).forEach(v => {
      newErrors[v] = values[v].length < 5
    });
    
    setErrors(newErrors);
  }
  
  const handleTouched = (name, e) => {
    setTouched({
      ...touched,
      [name]: true
    })
  }
  
  useEffect(() => {
    validateValues()
  }, [values])
  
  return (
    <form onSubmit={(event) => handleSubmit(event)} className="login" action="">
      <input type="text"
             className="login__input"
             onChange={(e) => handleChange('login',e)}
             onBlur={e => handleTouched('login',e)}
             value={values.login}
             placeholder="Enter login"/>
      {errors.login && touched.login && <span>wrong data</span>}
      <input type="password"
             className="login__input"
             onChange={(e) => handleChange('password',e)}
             onBlur={e => handleTouched('password',e)}
             value={values.password}
             placeholder="Enter password"/>
      {errors.password && touched.password && <span>wrong data</span>}
      <button type="submit" className="header__login-btn">
        {
          isRegister ? 'save' : 'login'
        }
      </button>
    </form>
  );
};

export default LoginForm;