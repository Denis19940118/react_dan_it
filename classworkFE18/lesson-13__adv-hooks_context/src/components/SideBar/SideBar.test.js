import {shallow} from 'enzyme';
import configureStore from 'redux-mock-store';
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import SideBar from "./SideBar";

const store = configureStore([thunk])({
  toggleEmailsModal: () => null
});

jest.mock("../../redux/emails", () => ({
  emailsActions: {
    toggleEmailsModal: () => null
  },
  emailsSelectors: {
    isAuth: () => false,
    isEmailModalOpen: () => false
  }
}));

jest.mock("react-router-dom", () => ({
  NavLink: 'a'
}));

test("NewEmail rendered with empty redux", () => {
console.log(store);

  const container = shallow(<Provider store={store}>
    <SideBar items={['1', '2']}/>
  </Provider>);
  console.log(container.html());
});