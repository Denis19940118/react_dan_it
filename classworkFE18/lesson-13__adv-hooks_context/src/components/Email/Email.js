import React, {Component, useCallback, useState} from 'react';
import './Emails.scss'
import useToggle from "../../utils/useToggle";

const Email = ({self}) => {
  const [isOpenText, setIsOpenText] = useToggle(false);
  const {subject, to, from, text} = self;
  const address = from || to;

  return (
    <div className="emails-list__item" onClick={() => setIsOpenText()}>
      {<span className={`emails-list__link-${from ? 'from' : 'to'}`}>{address}</span>}
      <h4 className="emails-list__subject">{subject}</h4>
      {/*<p className="emails-list__text">{text}</p>*/}
      {
        isOpenText && <p className="emails-list__text">{text}</p>
      }
    </div>
  );
};

export default Email;