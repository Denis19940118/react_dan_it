import React, {Component, useContext} from 'react';
import Email from "../Email/Email";
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";
import ThemeContext, {ThemeProvider} from "../../context/theme/themeContext";
import themes from "../../context/theme/themes";

const EmailsList = ({emails = []}) => {
    const [currentTheme] = useContext(ThemeContext);
    const {primary, secondary} = currentTheme;

    const emailsComponents = emails.map((email, index) => {
      // <Link key={index} to={`/emails/${email.id}`}><Email  self={email}/></Link>
      return <Email self={email}/>
    });

    return (
      <section className="emails-list" style={{background: secondary}}>
        <h2 style={{color: primary}}>Emails List</h2>
        {emailsComponents}
      </section>
    );
};

export default EmailsList;

const EmailType = PropTypes.shape({
  subject: PropTypes.string,
  from: PropTypes.string,
  to: PropTypes.string,
  text: PropTypes.string
});

EmailsList.propTypes = {
  emails: PropTypes.arrayOf(EmailType)
};