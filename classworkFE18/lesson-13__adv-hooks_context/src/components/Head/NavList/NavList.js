import React, {Component, useMemo} from 'react';
import NavLink from "../../NavLink/NavLink";

const NavList = ({items}) => {
  const itemsArray = useMemo(() => items.map((item, index) => {
    return <li key={index} className="header__nav-item">
      <NavLink text={item}/>
    </li>
  }), [items]);

  return (
    <ul className="header__nav-list">
      {itemsArray}
    </ul>
  );
};

export default NavList;