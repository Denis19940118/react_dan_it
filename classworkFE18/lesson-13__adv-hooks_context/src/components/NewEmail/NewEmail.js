import React, {useRef} from 'react';
import {withRouter} from "react-router-dom";
import {connect, useDispatch} from "react-redux";
import {emailsActions, emailsSelectors} from "../../redux/emails";

let NewEmail = ({handleSubmitProp, history, location, closeEmailModal, isEmailModalOpen}) => {
  let subject = useRef(null);
  let to = useRef(null);
  let text = useRef(null);
  const dispatch = useDispatch();

  const handleSubmit = event => {
    event.preventDefault();
    const newEmail = {
      subject: subject.value,
      to: to.value,
      text: text.value
    };
    dispatch(emailsActions.saveNewEmail(newEmail, event.target));
  };

  return (
    <form onSubmit={handleSubmitProp || handleSubmit}>
      <input ref={ref => subject = ref} type='text' placeholder='subject'/>
      <input ref={ref => to = ref} type='text' placeholder='to'/>
      <textarea ref={ref => text = ref} placeholder='subject'/>
      <button className={'.sbmt'} type='submit'>Send</button>
    </form>
  );
};

NewEmail = withRouter(NewEmail);

export default NewEmail;