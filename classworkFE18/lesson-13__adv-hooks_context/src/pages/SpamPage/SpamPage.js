import React, {Component, useEffect} from 'react';
import EmailsList from "../../components/EmailsList/EmailsList";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {emailsActions, emailsSelectors} from "../../redux/emails";
import {fetchEmails} from "../../utils/API";

const SpamPage = ({spamEmails, setSpamEmails}) => {
  useEffect(() => {
    fetchEmails('spam', setSpamEmails);
  }, []);
  
  return (<EmailsList emails={spamEmails}/>);
}

const mapStateToProps = reduxState => ({
  spamEmails: emailsSelectors.spam(reduxState)
});
const mapDispatchToProps = (dispatch) => ({
  setSpamEmails: (newSentEmails) => dispatch(
    emailsActions.saveCollection('spam', newSentEmails)
  )
});

export default connect(mapStateToProps, mapDispatchToProps)(SpamPage);

SpamPage.propTypes = {
  spamEmails: PropTypes.array.isRequired
};