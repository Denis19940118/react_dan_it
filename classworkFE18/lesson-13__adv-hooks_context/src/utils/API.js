export const fetchEmails = (folder) => (
  fetch(`/api/emails/${folder}`, {
    headers: {
      "Content-Type": 'application/json'
    }
  })
    .then(r => r.json())
);

export const fetchLogin = (user) => (
  fetch('/api/users/login', {
    method: 'POST',
    headers: {
      "Content-Type": 'application/json'
    },
    body: JSON.stringify(user)
  })
    .then(r => r.json())
);

export const fetchRegister = user => (
  fetch('/api/users/', {
    method: 'POST',
    headers: {
      "Content-Type": 'application/json'
    },
    body: JSON.stringify(user)
  })
    .then(r => r.json())
);

export const fetchNewEmail = email => (
  fetch('/api/emails/sent', {
    method: 'POST',
    headers: {
      "Content-Type": 'application/json'
    },
    body: JSON.stringify(email)
  }).then(r => r.json())
);