import {combineReducers} from "redux";
import user from './user/index';
import emails from './emails/index';
import {reducer as formReducer} from 'redux-form';

export default combineReducers({
  user,
  emails,
  form: formReducer
});