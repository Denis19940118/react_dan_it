import types from './types';
import {fetchEmails, fetchNewEmail} from "../../utils/API";
import {emailsSelectors} from "./index";

const saveAllEmailsAction = emails => ({
  type: types.SAVE_ALL_MAILS,
  payload: emails
});

const toggleMailModalAction = isEmailModalOpen => ({
  type: types.TOGGLE_EMAIL_MODAL,
  payload: !isEmailModalOpen
});

const saveEmailsCollection = (folder, collection) => ({
  type: types.SAVE_EMAIL_COLLECTION,
  payload: {
    folder,
    collection
  }
});

const saveCollectionAsync = (folder) => {
  return dispatch => {
    fetch(`/api/emails/${folder}`, {
      headers: {
        "Content-Type": 'application/json'
      }
    })
      .then(r => r.json())
      .then(result => {
        dispatch(saveEmailsCollection(folder, result))
      })
  };
};

const saveNewEmail = (email, form) => (dispatch, getState) => {
  fetchNewEmail(email)
    .then((response) => {
      if (response) {
        form.reset();
        const isEmailModalOpen = emailsSelectors.isEmailModalOpen(getState());
        dispatch(toggleMailModalAction(isEmailModalOpen));
        dispatch(saveCollectionAsync('sent'));
      }
    })
};

export default {
  saveAllEmails: saveAllEmailsAction,
  toggleEmailsModal: toggleMailModalAction,
  saveCollection: saveEmailsCollection,
  saveCollectionAsync,
  saveNewEmail
}