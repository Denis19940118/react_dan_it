import {combineReducers} from "redux";
import types from './types';

export const initialState = {
  all: {
    inbox: [],
    sent: [],
    draft: [],
    spam: [],
  },
  isEmailModalOpen: false,
};

export const allEmailsReducer = (currentState = initialState.all, action) => {
  switch (action.type) {
    case types.SAVE_ALL_MAILS:
      if (action.payload.hasOwnProperty('inbox')
        && action.payload.hasOwnProperty('sent')
        && action.payload.hasOwnProperty('draft')
        && action.payload.hasOwnProperty('spam')
      ) {
        return {
          ...action.payload
        };
      } else {
        return initialState.all;
      }
    case types.SAVE_EMAIL_COLLECTION:
      const {folder, collection} = action.payload;
      return {
        ...currentState,
        [folder]: collection
      };
    default:
      return currentState
  }
};

export const toggleModalReducer = (currentState = initialState.isEmailModalOpen, action) => {
  switch (action.type) {
    case types.TOGGLE_EMAIL_MODAL:
      return action.payload;
    default:
      return currentState
  }
};

export default combineReducers({
  all: allEmailsReducer,
  isEmailModalOpen: toggleModalReducer
})
