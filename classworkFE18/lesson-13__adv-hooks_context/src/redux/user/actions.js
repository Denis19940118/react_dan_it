import types from './types'
import {fetchLogin, fetchRegister} from "../../utils/API";

const saveUserAction = (user) => ({
  type: types.SAVE_USER,
  payload: user
});
const saveUserError = () => ({
  type: types.SAVE_USER_ERROR,
});

const loginUser = (user, setSubmitting) => dispatch => {
  fetchLogin(user)
    .then(result => {
      if(result.status) {
        console.log(result);
        dispatch(saveUserAction(user));
        setSubmitting(false);
      } else {
        dispatch(saveUserError())
      }
    })
};

const registerUser = user => dispatch => {
  fetchRegister(user)
    .then(resultUser => {
      dispatch(loginUser(resultUser))
    })
};

export default {
  saveUser: saveUserAction,
  login: loginUser,
  register: registerUser
}