import React, {Component} from 'react';
import {Redirect, Route} from "react-router-dom";
import PropTypes from 'prop-types';

class ProtectedRoute extends Component {
  render() {
    const {component: ComponentToRoute, protectedRender, isAuth, ...restProps} = this.props;

    return (
      <Route {...restProps} render={(routerProps) => {
        if(!isAuth) {
          return <Redirect to="/login"/>
        } else if (ComponentToRoute) {
          return <ComponentToRoute {...routerProps}/>
        } else if (protectedRender) {
          return protectedRender(routerProps)
        } else {
          throw new TypeError('No any component to render')
        }
      }}/>
    );
  }
}

export default ProtectedRoute;

ProtectedRoute.propTypes = {
  isAuth: PropTypes.bool.isRequired,
  render: PropTypes.func
};