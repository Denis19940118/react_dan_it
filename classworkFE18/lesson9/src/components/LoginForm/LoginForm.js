import React, {useRef} from 'react';
import {connect, useDispatch} from "react-redux";
import {userActions} from "../../redux/user";

const credentials = {
  login: 'admin',
  password: '123',
};

const LoginForm = ({history, location, match, isRegister}) => {
  const dispatch = useDispatch();
  let loginRef = useRef();
  let passwordRef = useRef();

  const handleSubmit = (event) => {
    event.preventDefault();
    const newUser = {
      login: loginRef.value,
      password: passwordRef.value
    };

    if (isRegister) {
      dispatch(userActions.register(newUser));
    } else {
      dispatch(userActions.login(newUser));
    }
  };

  const mapInputLogin = (loginInput) => {
    loginRef = loginInput;
  };

  const mapPasswordInput = (password) => {
    passwordRef = password;
  };

  return (
    <form onSubmit={(event) => handleSubmit(event)} className="login" action="">
      <input ref={loginInput => mapInputLogin(loginInput)} type="text" className="login__input"
             placeholder="Enter login"/>
      <input ref={password => mapPasswordInput(password)} type="password" className="login__input"
             placeholder="Enter password"/>
      <button type="submit" className="header__login-btn">
        {
          isRegister ? 'save' : 'login'
        }
      </button>
    </form>
  );
};

export default LoginForm;