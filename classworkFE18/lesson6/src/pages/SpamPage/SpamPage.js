import React, {Component} from 'react';
import EmailsList from "../../components/EmailsList/EmailsList";
import PropTypes from "prop-types";

class SpamPage extends Component {
  render() {
    const {spamEmails} = this.props;

    return (<EmailsList emails={spamEmails}/>);
  }
}

export default SpamPage;

SpamPage.propTypes = {
  spamEmails: PropTypes.array.isRequired
};