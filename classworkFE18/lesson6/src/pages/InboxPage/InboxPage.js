import React, {Component} from 'react';
import EmailsList from "../../components/EmailsList/EmailsList";
import PropTypes from "prop-types";

class InboxPage extends Component {
  render() {
    const {inboxEmails} = this.props;

    return (<EmailsList emails={inboxEmails}/>);
  }
}

export default InboxPage;

InboxPage.propTypes = {
  inboxEmails: PropTypes.array.isRequired
};