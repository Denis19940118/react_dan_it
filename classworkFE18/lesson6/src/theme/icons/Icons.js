import React from 'react';
import * as AllIcons from './index';

export const Icons = (type, w, h, color, classes) => {
	const currentIcon = AllIcons[type];

	if (!currentIcon) {
		return null;
	}

	return <div className={classes}>{currentIcon(color, classes)}</div>;
};
