import './App.scss';
import './components/Email/Emails.scss';
import Head from './components/Head/Head';
import React, { Component, useEffect, useState } from 'react';
import SideBar from './components/SideBar/SideBar';
import AppRoutes from './routes/AppRoutes';
import { Footer } from './components/Footer/Footer';

const App = () => {
	const [currentUser, setCurrentUser] = useState();
	const [inbox, setInbox] = useState([]);
	const [sent, setSent] = useState([]);
	const [draft, setDraft] = useState([]);
	const [spam, setSpam] = useState([]);
	let [counter, setCounter] = useState(null);

	const saveUser = user => {
		setCurrentUser({ currentUser: user });
	};

	// componentDidMount() {
	// 	fetch('emails.json')
	// 		.then(r => r.json())
	// 		.then(data => {
	// 			this.setState({ ...data });
	// 		});
	// }

	useEffect(() => {
		fetch('emails.json')
			.then(r => r.json())
			.then(data => {
				setInbox(data.inbox);
				setSent(data.sent);
				setSpam(data.spam);
				setDraft(data.draft);
			});
		console.log('counter', counter);
		return () => {
			console.log('unmount');
		};
	}, []);

	return (
		<>
			<Head />

			<div className="container emails">
				<AppRoutes
					user={currentUser}
					auth={u => this.saveUser(u)}
					inbox={inbox}
					sent={sent}
					draft={draft}
					spam={spam}
				/>
				{/*<EmailsList emails={sent}/>*/}'
			</div>

			<Footer name={'Huan Carlos Garsiza Venom'} />
		</>
	);
};

export default App;

const Random = () => <h1>Random component for example</h1>;
