import React, {Component} from 'react';
import {Link} from "react-router-dom";

class NavButton extends Component {
  render() {
    return (
      <Link to="/login" className="header__login-btn">Login</Link>
    );
  }
}

export default NavButton;