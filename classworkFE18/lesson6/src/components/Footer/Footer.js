import React, { useState } from 'react';

export const Footer = ({ name }) => {
	console.log('this', this);
	const [nameState, setNameState] = useState(`bad-bad-name, ${name}`);

	function handleClick() {
		const newName = prompt('new person name');
		setNameState(newName);
	}

	return (
		<footer>
			All rights are stolen by -{' '}
			<button onClick={handleClick}>{nameState}</button>
		</footer>
	);
};
