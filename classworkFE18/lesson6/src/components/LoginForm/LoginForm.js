import React, { Component, createRef, useRef } from 'react';

const credentials = {
	login: 'admin',
	password: '123',
};

const LoginForm = (history, location, match, auth) => {
	const loginRef = useRef();
	const passwordRef = useRef();

	const handleSubmit = event => {
		event.preventDefault();
		// const { history, auth } = props;

		if (
			loginRef.value === credentials.login &&
			passwordRef.value === credentials.password
		) {
			auth({ login: loginRef.value, password: passwordRef.value });
		}
	};

	const mapInputLogin = loginInput => {
		loginRef = loginInput;
	};

	const mapPasswordInput = password => {
		passwordRef = password;
	};

	return (
		<form onSubmit={event => handleSubmit(event)} className="login" action="">
			<input
				ref={loginInput => mapInputLogin(loginInput)}
				type="text"
				className="login__input"
				placeholder="Enter login"
			/>
			<input
				ref={password => mapPasswordInput(password)}
				type="password"
				className="login__input"
				placeholder="Enter password"
			/>
			<button type="submit" className="header__login-btn">
				Login
			</button>
		</form>
	);
};

export default LoginForm;
