import React, {Component, useEffect} from 'react';
import EmailsList from "../../components/EmailsList/EmailsList";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {emailsActions, emailsSelectors} from "../../redux/emails";
import {fetchEmails} from "../../utils/API";

const DraftPage = ({draftEmails, setDraftEmails}) => {
  useEffect(() => {
    fetchEmails('draft', setDraftEmails)
  }, []);
  
  return (<EmailsList emails={draftEmails}/>);
}

const mapStateToProps = reduxState => ({
  draftEmails: emailsSelectors.draft(reduxState)
});

const mapDispatchToProps = (dispatch) => ({
  setDraftEmails: (newSentEmails) => dispatch(
    emailsActions.saveCollection('draft', newSentEmails)
  )
});

export default connect(mapStateToProps, mapDispatchToProps)(DraftPage);

DraftPage.propTypes = {
  draftEmails: PropTypes.array.isRequired
};