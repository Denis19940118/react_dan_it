import './App.scss';
import './components/Email/Emails.scss'
import Head from "./components/Head/Head";
import React, {useEffect} from "react";
import AppRoutes from "./routes/AppRoutes";
import Footer from "./components/Footer/Footer";
import {connect, useDispatch, useSelector} from "react-redux";
import NewEmail from "./components/NewEmail";
import {emailsActions, emailsSelectors} from "./redux/emails";

const App = ({isEmailModalOpen}) => {
  return (
    <>
      <Head/>

      {isEmailModalOpen && <NewEmail />}

      <div className="container emails">
        <AppRoutes/>
      </div>

      <Footer name={"Huan Carlos Garsiza Venom"}/>

    </>
  );
};

const mapStateToProps = reduxState => ({
  isEmailModalOpen: emailsSelectors.isEmailModalOpen(reduxState)
});

export default connect(mapStateToProps)(App);