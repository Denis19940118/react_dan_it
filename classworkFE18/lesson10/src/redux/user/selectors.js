const selectIsAuth = currentStore => !!currentStore.user;
const selectUser = currentStore => currentStore.user;

export default {
  isAuth: selectIsAuth,
  user: selectUser
}