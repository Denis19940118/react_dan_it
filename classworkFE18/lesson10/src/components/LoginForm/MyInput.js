import React from 'react';

const MyInput = ({input, meta : {touched, error}, ...rest}) => {
  
  
  return (
    <div>
      <input {...input} {...rest}/>
      {
        error && touched && <span>{error}</span>
      }
    </div>
  );
};

export default MyInput;