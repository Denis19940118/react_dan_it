import React, { Component } from 'react';
import NavList from './NavList/NavList';
import NavButton from '../NavButton/NavButton';
import { withRouter } from 'react-router-dom';

class Head extends Component {
	state = {
		links: [],
	};

	componentDidMount() {
		this.setState({ links: ['home', 'portfolio', 'contacts', 'gallery'] });
	}

	goBack() {
		this.props.history.goBack();
	}

	render() {
		const { links, isModalOpen } = this.state;
		const { openLogin } = this.props;

		return (
			<header className="container header-wrapper">
				<img
					src="https://picsum.photos/200/300"
					alt="logo"
					className="header__company-logo"
				/>

				<NavList items={links} />

				<button onClick={() => this.goBack()}>go back</button>

				<NavButton handleClick={() => openLogin('login')} />
			</header>
		);
	}
}

export default withRouter(Head);
// export default Head;
