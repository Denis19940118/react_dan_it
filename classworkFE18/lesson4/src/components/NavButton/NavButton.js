import React, { Component } from 'react';

class NavButton extends Component {
	render() {
		const { handleClick } = this.props;
		return (
			<button onClick={handleClick} className="header__login-btn">
				Login
			</button>
		);
	}
}

export default NavButton;
