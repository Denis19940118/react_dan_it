import React, { Component } from 'react';
import './Email.scss';

class Email extends Component {
	render() {
		const { self } = this.props;
		const { subject, to, from, text } = self;
		const address = from || to;

		return (
			<div className="emails-list__item">
				{
					<span className={`emails-list__link-${from ? 'from' : 'to'}`}>
						{address}
					</span>
				}
				<h4 className="emails-list__subject">{subject}</h4>
				<p className="emails-list__text">{text}</p>
			</div>
		);
	}
}

export default Email;
