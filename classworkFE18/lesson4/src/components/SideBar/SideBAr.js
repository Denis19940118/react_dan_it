import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import './SideBar.scss';

class SideBar extends Component {
	render() {
		const { items } = this.props;
		const sideBarItems = items.map((singleItem, index) => (
			<li key={index} className="emails-sidebar__item">
				<NavLink
					activeClassName="emails-sidebar__item--active"
					to={`/${singleItem.toLowerCase()}`}
				>
					{singleItem.toUpperCase()}
				</NavLink>
				{/*<a href={"/" + singleItem.toLowerCase()}>{singleItem.toUpperCase()}</a>*/}
			</li>
		));

		return (
			<aside className="emails-sidebar">
				<ul className="emails-sidebar__list">{sideBarItems}</ul>
			</aside>
		);
	}
}

export default SideBar;
