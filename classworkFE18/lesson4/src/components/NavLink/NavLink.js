import React, { Component } from 'react';

class NavLink extends Component {
	render() {
		const { text } = this.props;
		return (
			<a href="#" className="header__nav-link">
				{text}
			</a>
		);
	}
}

export default NavLink;
