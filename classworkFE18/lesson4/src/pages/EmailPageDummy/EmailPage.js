import React, { Component } from 'react';
import Email from '../../components/Email/Email';

class EmailPageDummy extends Component {
	render() {
		console.log('EmailPageDummy props - ', this.props);
		const { allEmails } = this.props;
		const email = allEmails.find(
			email => email.id === this.props.match.params.emailID
		);

		return allEmails.length > 0 && email && <Email self={email} />;
	}
}

export default EmailPageDummy;
