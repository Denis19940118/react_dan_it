import './components/Email/Emails.scss';
import Head from './components/Head/Head';
import React, { Component } from 'react';
import LoginForm from './components/LoginForm/LoginForm';
import SideBar from './components/SideBar/SideBar';
import EmailsList from './components/EmailsList/EmailsList';
import { Route, Switch } from 'react-router-dom';
import EmailPage from './pages/EmailPage/EmailPage';
import Page404 from './pages/Page404/Page404';
import EmailPageDummy from './pages/EmailPageDummy/EmailPage';
import './App.scss';

class App extends Component {
	state = {
		currentPage: 'home',
		inbox: [],
		sent: [],
		draft: [],
		spam: [],
	};

	componentDidMount() {
		fetch('emails.json')
			.then(r => r.json())
			.then(data => {
				this.setState({ ...data });
			});
	}

	render() {
		const { currentPage, inbox, sent, draft, spam } = this.state;
		const currentPageComponent = this.getCurrentPageComponent();
		const allEmails = [...inbox, ...sent, ...draft, ...spam];

		return (
			<>
				<Head openLogin={nP => this.handlePageChange(nP)} />

				{currentPageComponent}

				<div className="container emails">
					<SideBar
						items={['Inbox', 'Sent', 'Draft', 'Spam', 'Random/4848415']}
					/>
					{/*<EmailsList emails={sent}/>*/}'
					<Switch>
						<Route
							exact
							path="/inbox"
							render={() => <EmailsList emails={inbox} />}
						/>
						<Route
							exact
							path="/sent"
							render={() => <EmailsList emails={sent} />}
						/>
						<Route
							exact
							path="/draft"
							render={() => <EmailsList emails={draft} />}
						/>
						<Route
							exact
							path="/spam"
							render={() => <EmailsList emails={spam} />}
						/>
						{/*<Route exact path="/emails/:emailID" component={EmailPage}/>*/}
						<Route exact path="/emails/:emailID">
							<EmailPage />
						</Route>
						{/*<Route exact path="/emails/:emailID" render={(props) => <EmailPageDummy allEmails={allEmails} {...props}/>}/>*/}
						<Route path="*" component={Page404} />
					</Switch>
					{/*<Route path="/random/:counterOfRandom" component={Random}/>*/}
				</div>
			</>
		);
	}

	handlePageChange(nextPage) {
		this.setState({ currentPage: nextPage });
	}

	getCurrentPageComponent() {
		const { currentPage } = this.state;
		switch (currentPage) {
			case 'home':
				return <h1>HOME PAGE</h1>;
			case 'login':
				return <LoginForm pageChanger={nP => this.handlePageChange(nP)} />;
			case 'welcome':
				return <h1>Welcome, my little programmer))</h1>;
			case 'error':
				return <h1>Error occurred...probably bad credentials</h1>;
		}
	}
}

export default App;

const Random = () => <h1>Random component for example</h1>;
