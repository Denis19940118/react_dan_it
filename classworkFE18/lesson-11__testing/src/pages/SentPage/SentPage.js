import React, {Component, useEffect} from 'react';
import EmailsList from "../../components/EmailsList/EmailsList";
import PropTypes from "prop-types";
import {connect, useDispatch, useSelector} from "react-redux";
import {emailsActions, emailsSelectors} from "../../redux/emails";
import {fetchEmails} from "../../utils/API";

const SentPage = () => {
  const dispatch = useDispatch();
  const sentEmails = useSelector(emailsSelectors.sent);
  useEffect(() => {
    dispatch(emailsActions.saveCollectionAsync('sent'))
  }, []);

  return (<EmailsList emails={sentEmails}/>);
};

export default SentPage;