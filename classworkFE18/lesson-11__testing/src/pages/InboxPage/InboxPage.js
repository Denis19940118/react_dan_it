import React, {useEffect} from 'react';
import EmailsList from "../../components/EmailsList/EmailsList";
import {useDispatch, useSelector} from "react-redux";
import {emailsActions, emailsSelectors} from "../../redux/emails";
import {fetchEmails} from "../../utils/API";

const InboxPage = () => {
  const dispatch = useDispatch();
  const inboxEmails = useSelector(emailsSelectors.inbox);

  useEffect(() => {
    dispatch(emailsActions.saveCollectionAsync('inbox'));
  }, []);

  return (<>
    <EmailsList emails={inboxEmails}/>
  </>);
};

export default InboxPage;