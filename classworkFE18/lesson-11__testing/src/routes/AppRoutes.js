import React, {Component} from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import EmailPage from "../pages/EmailPage/EmailPage";
import Page404 from "../pages/Page404/Page404";
import InboxPage from "../pages/InboxPage/InboxPage";
import SentPage from "../pages/SentPage/SentPage";
import DraftPage from "../pages/DraftPage/DraftPage";
import SpamPage from "../pages/SpamPage/SpamPage";
import LoginForm from "../components/LoginForm/LoginForm";
import ProtectedRoute from "./ProtectedRoute";
import SideBar from "../components/SideBar/SideBar";
import {connect} from "react-redux";
import {userSelectors} from "../redux/user";
import RegisterForm from "../components/RegisterForm/RegisterForm";

class AppRoutes extends Component {
  render() {
    const {isAuth} = this.props;
    
    const submitLogin = (values, dispatch, formProps) => {
      console.log(...values);
      // if (formProps.isRegister) {
      //   dispatch(userActions.register(newUser));
      // } else {
      //   dispatch(userActions.login(newUser));
      // }
    };

    return (
      <>
        <ProtectedRoute path="/" isAuth={isAuth}
                        protectedRender={() => <SideBar items={["Inbox", "Sent", 'Draft', 'Spam', 'Random/4848415']}/>}/>
        <Switch>
          <Route exact path="/register" isAuth={isAuth} render={(routerProps) => {
            if (isAuth) {
              return <Redirect to='/inbox'/>
            }
            return <RegisterForm/>
          }}/>
          <Route exact path="/login" isAuth={isAuth} render={(routerProps) => {
            if (isAuth) {
              return <Redirect to='/inbox'/>
            }
            return <LoginForm onSubmit={submitLogin} {...routerProps}/>
          }}/>
          <ProtectedRoute exact path="/inbox" isAuth={isAuth} protectedRender={() => <InboxPage/>}/>
          <ProtectedRoute exact path="/sent" isAuth={isAuth} protectedRender={() => <SentPage/>}/>
          <ProtectedRoute exact path="/draft" isAuth={isAuth} protectedRender={() => <DraftPage/>}/>
          <ProtectedRoute exact path="/spam" isAuth={isAuth} protectedRender={() => <SpamPage/>}/>
          <ProtectedRoute exact isAuth={isAuth} path="/emails/:emailID" component={EmailPage}/>
          <Route path="*" component={Page404}/>
        </Switch>
      </>
    );
  }
}

const mapStateToProps = currentStore => ({isAuth: userSelectors.isAuth(currentStore)});

export default connect(mapStateToProps)(AppRoutes);