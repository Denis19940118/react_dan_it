import React, {Component} from 'react';
import Email from "../Email/Email";
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";

class EmailsList extends Component {
  render() {
    const {emails = []} = this.props;
    const emailsComponents = emails.map((email, index) => <Link key={index} to={`/emails/${email.id}`}><Email  self={email}/></Link>);

    return (
      <section className="emails-list">
        <h2>Emails List</h2>
        {emailsComponents}
      </section>
    );
  }
}

export default EmailsList;

const EmailType = PropTypes.shape({
  subject: PropTypes.string,
  from: PropTypes.string,
  to: PropTypes.string,
  text: PropTypes.string
});

EmailsList.propTypes = {
  emails: PropTypes.arrayOf(EmailType)
};