import express from 'express';
import apiRoutes from '../api';

const allowedOrigins = [
	'http://localhost:3000',
	'https://my-lovely-site.com'
];

export default ({config}) => {
	const app = express();

	app.use(cors({
		origin: function (origin, callback) {
			// allow requests with no origin
			// (like mobile apps or curl requests)
			if (!origin) {
				return callback(null, true);
			}

			if (allowedOrigins.indexOf(origin) === -1) {
				const msg = 'The CORS policy for this site does not allow access from the specified Origin.';

				return callback(new Error(msg), false);
			}

			return callback(null, true);
		},
		credentials: true
	}));

	// Load API routes
	app.use('/api', apiRoutes());


	return app;
};