/**
 * @desc Повертає список товарів
 **/
export const ProductListController = (request, response) => {



	response
		.status(200)
		.json({
			success: true,
			data: []
		})
}