import {ProductListController} from './products.controller'
import {Router} from 'express';

const route = Router();

export default function(root) {
	root.use('/products', route);

	/**
	 * @desc GET-метод для отримання списку продуктів
	 **/
	route.get('/', ProductListController);
}