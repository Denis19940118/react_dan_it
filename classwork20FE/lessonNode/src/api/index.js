import {Router} from 'express';
import HomeRoute from './home';
import ProductRoute from './products';

export default () => {
	const router = Router();

	HomeRoute(router);
	ProductRoute(router);

	return router;
}