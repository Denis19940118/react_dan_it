import React, { useEffect, useState } from 'react';
import './App.css';
import Header from "./components/Header/Header";
import Inbox from "./components/Inbox/Inbox";
import Footer from "./components/Footer/Footer";
import Loader from './components/Loader/Loader';
import axios from 'axios';

const App = () => {
  const [user] = useState({ name: 'Andrew', age: 32, email: 'aaa' });
  const [emails, setEmails] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const getEmails = () => {
    axios.get('/api/emails')
      .then(res => {
        setEmails(res.data);
        setIsLoading(false);
      })
  }

  useEffect(() => {
    // componentDidMount, componentDidUpdate
    getEmails();

    return () => {
      // componentWillUnmount
    }
  }, []);

  if (isLoading) {
    return <Loader />;
  }

  return (
    <div className="App">
      <Header user={user} />
      <Inbox emails={emails} />
      <Footer />
    </div>
  );
}

export default App;