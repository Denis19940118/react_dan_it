import React from 'react';
import { Link, NavLink } from 'react-router-dom';

const Sidebar = () => {
	return (
		<div>
			<NavLink className="link" activeClassName="link--active" to="/inbox">
				Inbox
			</NavLink>
			<Link to="/favorites">Favorites</Link>
			<Link to="/sent">Sent</Link>
		</div>
	);
};
export default Sidebar;
