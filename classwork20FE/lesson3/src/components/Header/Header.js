import React, { PureComponent } from 'react';
// import './Header.scss';
import PropTypes from 'prop-types';

class Header extends PureComponent {
	render() {
		const { title, user } = this.props;

		console.log('Header.js', this);

		const userHasEmail = !!user.email;
		// const userHasEmail = Boolean(user.email);

		return (
			<div className={'header'}>
				<h2 className={'header__title'}>Header - class component</h2>
				<div>{title}</div>
				<div>{user.name}</div>
				<div>{user.age}</div>
				{userHasEmail && <div>{user.email}</div>}
			</div>
		);
	}
}

Header.propTypes = {
	title: PropTypes.string,
	user: PropTypes.object.isRequired,
};

Header.defaultProps = {
	title: 'Default title',
};

export default Header;
