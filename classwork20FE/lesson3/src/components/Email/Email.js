import React from 'react';
import PropTypes from 'prop-types';
import './Email.scss';
import Icon from '../Icon/Icon';

const Email = props => {
	const { email } = props;

	return (
		<div className="email">
			<div className="email__topic">
				{email.topic}
				<Icon type="star" color="gold" filled />
			</div>
		</div>
	);
};

// string, object, number, func, bool, array, symbol
// shape(), oneOfType([]), arrayOf(), // exact(), oneOf([]), instanceOf()
Email.propTypes = {
	email: PropTypes.shape({
		id: PropTypes.number.isRequired,
		topic: PropTypes.string.isRequired,
	}).isRequired,
};

export default Email;
