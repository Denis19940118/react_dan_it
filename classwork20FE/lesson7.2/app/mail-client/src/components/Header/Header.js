import React from 'react';
import './Header.scss';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getUserSelector } from '../../store/user/selectors';

const Header = props => {
	const { title, user } = props;

	return (
		<div className="header">
			<h2 className="header__title">Header - class component</h2>
			<div>{title}</div>
			{!!user && <div>{user.login}</div>}
		</div>
	);
};

const mapStateToProps = state => {
	return {
		user: getUserSelector(state),
	};
};

Header.propTypes = {
	title: PropTypes.string,
	user: PropTypes.object,
};

Header.defaultProps = {
	title: 'Default title',
};

export default connect(mapStateToProps)(Header);
