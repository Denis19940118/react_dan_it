import React from 'react';
import './App.css';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';

import Sidebar from './components/Sidebar/Sidebar';
import AppRoutes from './routes/AppRoutes';

const App = () => {
	// const [user, setUser] = useState(null);
	// const [emails, setEmails] = useState([]);
	// const [isLoading, setIsLoading] = useState(true);

	// const getEmails = () => {
	//   axios.get('/api/emails')
	//     .then(res => {
	//       setEmails(res.data);
	//       setIsLoading(false);
	//     })
	// }

	// useEffect(() => {
	//   getEmails();
	// }, []);

	// if (isLoading) {
	//   return <Loader />;
	// }

	return (
		<div className="App">
			<Header />
			<Sidebar />
			<AppRoutes />
			<Footer />
		</div>
	);
};

export default App;
