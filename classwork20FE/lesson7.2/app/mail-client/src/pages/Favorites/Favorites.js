import React from 'react';
import { useSelector } from 'react-redux';
import {
	emailsLoadingSelector,
	getEmailsSelector,
} from '../../store/emails/selectors';
import Email from '../../components/Email/Email';
import Loader from '../../components/Loader/Loader';

function Favorites() {
	const emails = useSelector(getEmailsSelector);
	const isLoading = useSelector(emailsLoadingSelector);

	if (isLoading) {
		return <Loader />;
	}

	const emailCard = emails
		.filter(el => el.favorite)
		.map(e => <Email key={e.id} email={e} />);
	console.log(' emails', emails);

	return (
		<div>
			<div>{emailCard}</div>
		</div>
	);
}

export default Favorites;
