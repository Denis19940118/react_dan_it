import React, { useEffect } from 'react';
import Email from '../../components/Email/Email';
import Loader from '../../components/Loader/Loader';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import {
	emailsLoadingSelector,
	getEmailsSelector,
} from '../../store/emails/selectors';
// import { getEmailsOperation as getEmails } from '../../store/emails/operations';

const Inbox = () => {
	const emails = useSelector(getEmailsSelector);
	console.log('emails', emails);
	const isLoading = useSelector(emailsLoadingSelector);
	// const dispatch = useDispatch();
	console.log('getEmailsSelector', getEmailsSelector);
	// console.log(' getEmails', getEmails);

	// useEffect(() => {
	// 	dispatch(getEmails());
	// }, [dispatch]);

	if (isLoading) {
		return <Loader />;
	}

	const emailCards = emails.map(e => <Email key={e.id} email={e} />);

	return (
		<div>
			<div>{emailCards}</div>
		</div>
	);
};

Inbox.propTypes = {
	title: PropTypes.string,
	incrementAge: PropTypes.func,
	updateTitle: PropTypes.func,
};

export default Inbox;
