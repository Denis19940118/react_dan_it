import React from 'react';
import PropTypes from 'prop-types';
import './Email.scss';
import Icon from '../Icon/Icon';
import Button from '../Button/Button';
import { Link, useHistory } from 'react-router-dom';

const Email = (props) => {
  const { email, showFull } = props;
  const history = useHistory();
  
  const goToPrevious = () => {
    if (email.id > 1) {
      history.push(`/emails/${email.id - 1}`)
    }
  }

  const goToNext = () => {
    history.push(`/emails/${email.id + 1}`)
  }

  return (
    <div className='email'>
      <div className='email__topic'>
        <Link to={`/emails/${email.id}`}>{email.topic}</Link>
        <Icon type='star' color='gold' filled />
      </div>
      {showFull && <div className='email__body'>{email.body}</div>}
      {
        showFull && (
          <div className='email__controls'>
            <Button title='Previous' onClick={goToPrevious} />
            <Button title='Next' onClick={goToNext} />
          </div>
        )
      }
    </div>
  )
}

// string, object, number, func, bool, array, symbol
// shape(), oneOfType([]), arrayOf(), // exact(), oneOf([]), instanceOf()
Email.propTypes = {
  email: PropTypes.shape({
    id: PropTypes.number.isRequired,
    topic: PropTypes.string.isRequired
  }).isRequired,
}

export default Email