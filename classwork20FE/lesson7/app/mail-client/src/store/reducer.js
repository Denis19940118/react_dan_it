import { SET_EMAILS, SET_EMAILS_LOADING } from "./emails/types"
import { SET_CURRENT_USER } from "./user/types"

const initialState = {
  user: null,
  emails: {
    isLoading: true,
    data: []
  }
}

const reducer = (state = initialState, action) => {
  switch(action.type) {
    // case "INCREMENT_AGE": {
    //   return {...state, age: state.age + 1}
    // }
    case SET_CURRENT_USER: {
      const newUser = action.payload
      return {...state, user: newUser}
    }
    case SET_EMAILS: {
      return {...state, emails: {...state.emails, data: action.payload}}
    }
    case SET_EMAILS_LOADING: {
      return {...state, emails: {...state.emails, isLoading: action.payload}}
    }
    default: {
      return state
    }
  }
}

export default reducer;