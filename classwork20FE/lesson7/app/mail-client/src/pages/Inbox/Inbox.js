import React, { useEffect } from 'react';
import Email from '../../components/Email/Email';
import PropTypes from 'prop-types';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import {
	emailsLoadingSelector,
	getEmailsSelector,
} from '../../store/emails/selectors';
import {
	emailsLoadingAction,
	saveEmailsAction,
} from '../../store/emails/actions';
import Loader from '../../components/Loader/Loader';
import { getEmails } from '../../store/emails/operations';

const Inbox = () => {
	const dispatch = useDispatch();
	const emails = useSelector(getEmailsSelector);
	const isLoading = useSelector(emailsLoadingSelector);

	useEffect(() => {
		dispatch(getEmails());
	}, [dispatch]);

	if (isLoading) {
		return <Loader />;
	}

	const emailCards = emails.map(e => <Email key={e.id} email={e} />);

	return (
		<div>
			<div>{emailCards}</div>
		</div>
	);
};

Inbox.propTypes = {
	// emails: PropTypes.arrayOf(PropTypes.shape({
	//   id: PropTypes.number.isRequired,
	//   topic: PropTypes.string.isRequired
	// })).isRequired,
	title: PropTypes.string,
	incrementAge: PropTypes.func,
	updateTitle: PropTypes.func,
};

export default Inbox;
