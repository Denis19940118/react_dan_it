import React from 'react';
import PropTypes from 'prop-types';

export const themes = {
	light: {
		color: '#000',
		background: '#fff',
	},
	dark: {
		color: '#fff',
		background: '#222',
	},
};

const ThemeContext = React.createContext(themes.dark);

ThemeContext.displayName = 'themeContext';
export default ThemeContext;
