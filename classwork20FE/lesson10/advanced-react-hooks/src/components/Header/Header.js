import React, { memo, useContext } from 'react';
import PropTypes from 'prop-types';
import Button from '../Button/Button';
import ThemeContext from '../../ThemeContext/ThemeContext';

const Header = props => {
	const context = useContext(ThemeContext);

	return (
		<div>
			<h2>Header</h2>
			<Button title="header" onClick={context.toggleTheme} />
		</div>
	);
};

Header.propTypes = {};

export default memo(Header);
