import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';

const Body = props => {
	const { title, numbers, fetchEmails } = props;
	useEffect(() => {});
	return (
		<div>
			<h2>Body</h2>
			<div>{title}</div>
			<div>{numbers}</div>
		</div>
	);
};

Body.propTypes = {};

export default memo(Body);
