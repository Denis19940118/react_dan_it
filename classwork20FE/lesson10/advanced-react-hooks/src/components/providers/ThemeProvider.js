import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import ThemeContext, { themes } from '../../ThemeContext/ThemeContext';

function ThemeProvider({ children }) {
	const [theme, setTheme] = useState(themes.light);

	const toggleTheme = useCallback(() => {
		setTheme(theme => (theme === themes.dark ? themes.light : themes.dark));
	}, []);
	return (
		<ThemeContext.Provider value={(theme, setTheme, toggleTheme)}>
			{children}
		</ThemeContext.Provider>
	);
}

ThemeProvider.propTypes = {};

export default ThemeProvider;
