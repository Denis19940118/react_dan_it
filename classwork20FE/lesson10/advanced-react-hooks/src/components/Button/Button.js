import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import ThemeContext from '../../ThemeContext/ThemeContext';
const Button = ({ title }) => {
	const context = useContext(ThemeContext);

	return (
		<button style={context.theme} onClick={context.toggleTheme}>
			{title}
		</button>
		// <ThemeContext.Consumer>
		// 	{theme => {
		// 		return (
		// 			<button style={theme} onClick={onClick}>
		// 				{title}
		// 			</button>
		// 		);
		// 	}}
		// </ThemeContext.Consumer>
	);
};

Button.propTypes = {};

export default Button;
