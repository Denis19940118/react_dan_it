import logo from './logo.svg';
import './App.css';
import Body from './components/Body/Body';
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import { useCallback, useEffect, useMemo, useReducer, useState } from 'react';
import ThemeContext, { themes } from './ThemeContext/ThemeContext';
import Button from './components/Button/Button';
import ThemeProvider from './components/providers/ThemeProvider';

const testEmails = [
	{
		id: 1,
		topic: 'Email 1',
		favorite: true,
		body:
			'Email 1 - Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.',
	},
	{
		id: 2,
		topic: 'Email 2',
		body:
			'Email 2 - Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.',
	},
	{
		id: 3,
		topic: 'Email 3',
		body:
			'Email 3 - Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.',
	},
];

function App() {
	// const [emails, dispatch] = useReducer(reducer, []);
	// const emailsCards = emails.map(e => <div key={e.id}>e.topic</div>);

	// const fetchEmails = useCallback(() => {
	//   dispatch({'SET_EMAILS', payload: testEmails })
	// },[]);

	// useEffect(() => {
	// 	fetchEmails();
	// }, []);

	const numbers = useMemo(() => [1, 2, 3], []);
	// const [theme, setTheme] = useState(themes.light);

	// const toggleThemee = useCallback(() => {
	// 	setTheme(theme => (theme === themes.dark ? themes.light : themes.dark));
	// }, []);

	return (
		<div className="App">
			{/* {emailsCards} */}
			<Button title="click me" />
			<ThemeProvider>
				<Header />
				<Body
					// fetchEmails={fetchEmails}
					title="Hello world"
					numbers={numbers}
				/>
				<Footer />
			</ThemeProvider>
		</div>
	);
}

export default App;
