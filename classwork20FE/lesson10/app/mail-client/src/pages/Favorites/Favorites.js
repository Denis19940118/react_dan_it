import React from 'react'
import Email from '../../components/Email/Email';
import { emailsLoadingSelector, getEmailsSelector } from '../../store/emails/selectors';
import Loader from '../../components/Loader/Loader';
import { useSelector } from 'react-redux';

function Favorites() {
  const emails = useSelector(getEmailsSelector);
  const isLoading = useSelector(emailsLoadingSelector);

  if (isLoading) {
    return <Loader />;
  }

  const emailCards = emails
    .filter(e => e.favorite)
    .map(e => <Email key={e.id} email={e} />);

  return (
    <div>
      <div>
        {emailCards}
      </div>
    </div>
  );
}

export default Favorites
