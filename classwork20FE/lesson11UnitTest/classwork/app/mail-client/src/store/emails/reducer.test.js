import reducer from './reducer';
import { SET_EMAILS, SET_EMAILS_LOADING, TOGGLE_FAVORITES } from './types';

const initialState = {
	isLoading: true,
	data: [],
};

const testEmails = [
	{ id: 111, topic: 'test topic 111', body: 'test body 111' },
	{ id: 112, topic: 'test topic 112', body: 'test body 112' },
	{ id: 113, topic: 'test topic 113', body: 'test body 113' },
];

describe('Testing email reducer', () => {
	test('Testing SET_EMAILS', () => {
		const action = { type: SET_EMAILS, payload: testEmails };

		const newState = reducer(initialState, action);
		expect(newState.data).toBe(testEmails);
		expect(newState.data.length).toBe(3);
		// expect(newState.data[0]).toBe({
		// 	id: 111,
		// 	topic: 'test topic 111',
		// 	body: 'test body 111',
		// });
		expect(newState.data[0]).toEqual({
			id: 111,
			topic: 'test topic 111',
			body: 'test body 111',
		});
	});

	test('Testing SET_EMAILS_LOADING', () => {
		const action = { type: SET_EMAILS_LOADING, payload: false };

		const newState = reducer(initialState, action);
		expect(newState.isLoading).toBeFalsy();
	});

	// test('Testing TOGGLE_FAVORITES adds isFavorite prop', () => {
	// 	const action = { type: TOGGLE_FAVORITES, payload: testEmails[1] };

	// 	// const newState = reducer({ data: testEmails, isLoading: false }, action);
	// 	let newState = reducer({ data: testEmails, isLoading: false }, action);
	// 	expect(newState.data[1].favorite).toBeTruthy();
	// 	expect(newState.data[0].favorite).toBe(undefined);

	// 	const newAction = { type: TOGGLE_FAVORITES, payload: newState.data[1] };

	// 	newState = reducer(newState, newAction);

	// 	expect(newState.data[1].favorite).toBe(false);
	// });

	test('TOGGLE_FAVORITES adds isFavorite prop', () => {
		const action = { type: TOGGLE_FAVORITES, payload: testEmails[1] };

		let newState = reducer({ data: testEmails, isLoading: false }, action);

		expect(newState.data[1].favorite).toBeTruthy();
		expect(newState.data[0].favorite).toBe(undefined);

		const newAction = { type: TOGGLE_FAVORITES, payload: newState.data[1] };
		newState = reducer(newState, newAction);

		expect(newState.data[1].favorite).toBeFalsy();
	});

	// test('Testing SET_EMAILS_LOADING', () => {
	// 	const action = { type: SET_EMAILS_LOADING, payload: false };

	// 	const newState = reducer(initialState, action);
	// 	expect(newState.isLoading).toBe(false);
	// });
});
