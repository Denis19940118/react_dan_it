import { render, fireEvent } from '@testing-library/react';
import { Login } from './Login';

describe('Testing Login.js', () => {
	test('Smoke test of Login', () => {
		render(<Login />);
	});

	test('Clicking Log In button submits form', () => {
		const setUserMock = jest.fn();
		const { getByTestId } = render(<Login setUser={setUserMock} />);
		const loginButton = getByTestId('loginButton');
		expect(loginButton).toBeInTheDocument();

		expect(setUserMock).not.toHaveBeenCalled();
		loginButton.dispatchEvent(new Event('click'));
		fireEvent.click(loginButton);
		expect(setUserMock).toHaveBeenCalledTimes(1);
	});

	test('Login and password are submitted during the login process', () => {});
});
