import logo from './logo.svg';
import './App.css';
import UncontrolledForm from './components/UncontrolledForm/UncontrolledForm';
import ControlledForm from './components/UncontrolledForm/UncontrolledForm';

function App() {
	return (
		<div className="App">
			<UncontrolledForm />
			<ControlledForm />
		</div>
	);
}

export default App;
