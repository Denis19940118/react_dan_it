import { SET_EMAILS, SET_EMAILS_LOADING } from './types';

export const saveEmailsAction = emails => ({
	type: SET_EMAILS,
	payload: emails,
});

export const emailsLoadingAction = isLoading => ({
	type: SET_EMAILS_LOADING,
	payload: isLoading,
});
npm i redux react-redux redux-devtools-extension