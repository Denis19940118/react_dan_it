import React from 'react';
import { connect } from 'react-redux';
import { incrementAgeAction } from '../../store/actions';
import { ageSelector, nameSelector } from '../../store/selectors';

function Footer(props) {
	const { name, age, incrementAge } = props;

	return (
		<div>
			<h2>Footer</h2>
			<div>{name}</div>
			<div>{age}</div>
			<div>
				<button onClick={incrementAge}>Increment age</button>
			</div>
		</div>
	);
}

const mapStateToProps = state => {
	return {
		name: nameSelector(state),
		age: ageSelector(state),
	};
};

const mapDispatchToProps = dispatch => {
	return {
		incrementAge: () => dispatch(incrementAgeAction()),
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
