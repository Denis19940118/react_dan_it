import { useEffect, useState } from "react";

const useTimeout = (arr, timeout = 1000) => {
  const [timeoutArr, setTimeoutArr] = useState([]);

  useEffect(() => {
    arr.forEach((item, index) => {
      setTimeout(() => {
        setTimeoutArr(state => [...state, item])
      }, timeout * index)
    })
  }, [arr, timeout]);

  return timeoutArr;
}

export default useTimeout;