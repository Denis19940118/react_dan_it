import React from 'react';
import PropTypes from 'prop-types';
import { useField } from 'formik';

const MyInput2 = ({ name, ...rest }) => {
	const [field, meta /*helper*/] = useField(name);
	return (
		<div>
			<input {...field} {...rest} />
			{meta.error && meta.touched && (
				<span className="error">{meta.error}</span>
			)}
		</div>
	);
};

MyInput2.propTypes = {};

export default MyInput2;

// const MyInput2 = ({ name, ...rest }) => {
// const [field, meta /* helpers */] = useField(name);

// 	return (
// 		<div>
// 			<input {...field} {...rest} />
// 			{meta.error && meta.touched && (
// 				<span className="error">{meta.error}</span>
// 			)}
// 		</div>
// 	);
// };

// export default MyInput2;
