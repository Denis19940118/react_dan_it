import React from 'react';
import { Formik, Form, Field } from 'formik';
import MyInput from './MyInput';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
const validateForm = values => {
	const { login, password, repeatPassword } = values;
	const errors = {};

	if (!EMAIL_REGEX.test(login)) {
		errors.login = 'This is not a valid email';
	}

	if (!login) {
		errors.login = 'This field is required';
	}

	if (password !== repeatPassword) {
		errors.repeatPassword = 'Passwords do not match';
	}

	if (password.length < 8) {
		errors.password = 'Password should contain at least 8 characters';
	}

	if (!password) {
		errors.password = 'This field is required';
	}

	if (!repeatPassword) {
		errors.repeatPassword = 'This field is required';
	}
	return errors;
};
function FormikForm() {
	// const [values, setValues] = useState({
	// 	login: 'admin@test.com',
	// 	password: '',
	// 	repeatPassword: '',
	// });
	// const [errors, setErrors] = useState({});
	// const [touched, setTouched] = useState({});

	// const register = (values, helper) => {
	const register = (values, { setSubmiting }) => {
		console.log(values);
		// console.log(helper);
	};

	return (
		<Formik
			initialValues={{
				login: 'admin@test.com',
				password: '',
				repeatPassword: '',
			}}
			onSubmit={register}
			validate={validateForm}
		>
			{formikProps => {
				console.log(formikProps);
				return (
					<Form noValidate>
						<h3>ControlledForm</h3>
						<div>
							<Field
								component={MyInput}
								name="login"
								type="email"
								placeholder="Email"
							/>
						</div>
						<div>
							<Field
								component={MyInput}
								name="password"
								type="password"
								placeholder="Password"
							/>
						</div>
						<div>
							<Field
								component={MyInput}
								name="repeatPassword"
								type="password"
								placeholder="Repeat Password"
							/>
						</div>
						<div>
							<button type="submit" disabled={formikProps.isSubmitting}>
								Register
							</button>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
}

export default FormikForm;
