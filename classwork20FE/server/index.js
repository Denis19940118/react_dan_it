const express = require('express');

const app = express();

const emails = [
	{ id: 1, topic: 'Email 1', favorite: true },
	{ id: 2, topic: 'Email 2' },
	{ id: 3, topic: 'Email 3' },
];

app.get('/api/emails', (req, res) => {
	res.send(emails);
});

app.get('/api/emails/:emailId', (req, res) => {
	const { emailId } = req.params;
	res.send(emails.find(e => e.id === +emailId));
});

const port = 8085;

app.listen(port, () => {
	console.log(`Server listening on port ${port}`);
});
