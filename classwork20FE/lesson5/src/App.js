import React, { useEffect, useState } from 'react';
import './App.css';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Loader from './components/Loader/Loader';
import axios from 'axios';
import Sidebar from './components/Sidebar/Sidebar';
import AppRoutes from './routes/AppRoutes';

const App = () => {
	const [user, setUser] = useState(null);
	const [emails, setEmails] = useState([]);
	const [isLoading, setIsLoading] = useState(true);

	const getEmails = () => {
		axios.get('/api/emails').then(res => {
			setEmails(res.data);
			setIsLoading(false);
		});
	};

	useEffect(() => {
		// componentDidMount, componentDidUpdate
		getEmails();

		return () => {
			// componentWillUnmount
		};
	}, []);

	if (isLoading) {
		return <Loader />;
	}

	return (
		<div className="App">
			<Header user={user} />
			<Sidebar />
			<AppRoutes emails={emails} setUser={setUser} />
			<Footer />
		</div>
	);
};

export default App;
