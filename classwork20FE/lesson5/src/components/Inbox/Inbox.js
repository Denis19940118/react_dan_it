import React from 'react';
import Email from '../Email/Email';
import PropTypes from 'prop-types';

const Inbox = (props) => {
  const { emails } = props;

  const emailCards = emails.map(e => <Email key={e.id} email={e} />);

  return (
    <div>
      <div>
        {emailCards}
      </div>
    </div>
  );
}

Inbox.propTypes = {
  emails: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    topic: PropTypes.string.isRequired
  })).isRequired,
  title: PropTypes.string,
  incrementAge: PropTypes.func,
  updateTitle: PropTypes.func
}

export default Inbox;