import React, { Component } from 'react';
import './App.css';
import Header from './components/Header/Header';
import Inbox from './components/Inbox/Inbox';
import Footer from './components/Footer/Footer';

const emails = [
	{ id: 1, topic: 'Email 1' },
	{ id: 2, topic: 'Email 2' },
	{ id: 3, topic: 'Email 3' },
];

class App extends Component {
	state = {
		title: 'Hello world',
		user: {
			name: 'Andrew',
			age: 32,
			email: 'aaa',
		},
	};

	incrementAge = () => {
		const { user } = this.state;
		this.setState({ user: { ...user, age: user.age + 1 } });
	};

	updateTitle = () => {
		this.setState({ title: 'New title' });
	};

	render() {
		const { user, title } = this.state;

		return (
			<div className="App">
				<Header user={user} title={title} />
				<Inbox
					title="Body"
					emails={emails}
					incrementAge={this.incrementAge}
					updateTitle={this.updateTitle}
					actions={
						<div>
							<button>Click me</button>
						</div>
					}
				/>
				<Footer />
			</div>
		);
	}
}

export default App;
