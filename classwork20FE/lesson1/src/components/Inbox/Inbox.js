import React, { PureComponent } from 'react';
import Email from '../Email/Email';
import PropTypes from 'prop-types';
import Button from '../Button/Button';

class Inbox extends PureComponent {
	render() {
		const { title, incrementAge, updateTitle, emails } = this.props;
		console.log('🚀 ~ file: Body.js ~ line 6 ~ Body ~ render ~ emails', emails);

		// console.log('Body.js', this);
		const emailsCards = emails.map(el => <Email key={el.id} email={el} />);
		return (
			<div>
				<h2>{title}</h2>
				{/* <div>{message}</div> */}
				{/* {actions} */}
				<div>
					<Button onClick={updateTitle} title={'Update title'} />
					<Button onClick={incrementAge} title={'Increment age'} color="blue" />
				</div>
				<div>{emailsCards}</div>
			</div>
		);
	}
}

Inbox.propTypes = {
	emails: PropTypes.array.isRequired,
	title: PropTypes.string,
	updateTitle: PropTypes.func.isRequired,
	incrementAge: PropTypes.func.isRequired,
};

export default Inbox;
