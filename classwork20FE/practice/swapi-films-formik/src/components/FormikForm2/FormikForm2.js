import React from 'react';
import { withFormik } from 'formik';
import MyInput2 from './MyInput2';
import * as yup from 'yup';
import schema from './schema';
// const FIELD_REQUIRED = 'This field is required';

// const schema = yup.object().shape({
// 	login: yup
// 		.string()
// 		.required(FIELD_REQUIRED)
// 		.email('This is not a valid email'),
// 	password: yup
// 		.string()
// 		.required(FIELD_REQUIRED)
// 		.min(8, 'Password should contain at least 8 characters'),
// 	repeatPassword: yup
// 		.string()
// 		.required(FIELD_REQUIRED)
// 		// .min(8, 'Password should contain at least 8 characters'),
// 		.oneOf([yup.ref('password')], 'Passwords do not match'),
// });

// const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
// const validateForm = values => {
// 	const { login, password, repeatPassword } = values;
// 	const errors = {};

// 	if (!EMAIL_REGEX.test(login)) {
// 		errors.login = 'This is not a valid email';
// 	}

// 	if (!login) {
// 		errors.login = 'This field is required';
// 	}

// 	if (password !== repeatPassword) {
// 		errors.repeatPassword = 'Passwords do not match';
// 	}

// 	if (password.length < 8) {
// 		errors.password = 'Password should contain at least 8 characters';
// 	}

// 	if (!password) {
// 		errors.password = 'This field is required';
// 	}

// 	if (!repeatPassword) {
// 		errors.repeatPassword = 'This field is required';
// 	}
// 	return errors;
// };

const register = (values, { setSubmiting }) => {
	console.log(values);
	// console.log(helper);
	setSubmiting(false);
};
function FormikForm2(props) {
	console.log(' FormikForm2 ~ props', props);
	// const [values, setValues] = useState({
	// 	login: 'admin@test.com',
	// 	password: '',
	// 	repeatPassword: '',
	// });
	// const [errors, setErrors] = useState({});
	// const [touched, setTouched] = useState({});

	// const register = (values, helper) => {

	return (
		<form noValidate>
			<h3>ControlledForm</h3>
			<div>
				<MyInput2 name="login" type="email" placeholder="Email" />
			</div>
			<div>
				<MyInput2 name="password" type="password" placeholder="Password" />
			</div>
			<div>
				<MyInput2
					name="repeatPassword"
					type="password"
					placeholder="Repeat Password"
				/>
			</div>
			<div>
				<button type="submit" disabled={props.isSubmitting}>
					Register
				</button>
			</div>
		</form>
	);
}

export default withFormik({
	mapPropsToValues: props => ({
		login: 'admin@test.com',
		password: '',
		repeatPassword: '',
	}),
	handleSubmit: register,
	// validate: validateForm,
	validationSchema: schema,
})(FormikForm2);
