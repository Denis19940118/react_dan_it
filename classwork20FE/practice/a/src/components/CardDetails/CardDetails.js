import React from 'react';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { selectCards } from '../../redux/profile/selectors';
import './CardDetails.scss';
import Card from '../Card/Card';
import ContainerCard from '../ContainerCard/ContainerCard';

const CardDetails = props => {
	const { cards, url } = props;
	const history = useHistory();

	const basketCard = JSON.parse(localStorage.getItem('Card'));
	const toBasket = [];

	cards.filter(el => (el.id === basketCard ? toBasket.push(el) : ''));

	const goToCard = () => {
		history.push(`${url}`);
		localStorage.removeItem('Card');
	};

	const details = () => (
		<div className="card__details">
			<p>Details information</p>
			<Card
				key={toBasket[0].id}
				btnDel={false}
				url={url}
				{...toBasket[0]}
				card={toBasket[0]}
			/>
			<div>
				<button onClick={goToCard}>Go back</button>
			</div>
		</div>
	);

	return <ContainerCard showDetails details={details} title="Choice product" />;
};

const mapStateToProps = currentStore => ({
	cards: selectCards(currentStore),
});

export default connect(mapStateToProps)(CardDetails);
