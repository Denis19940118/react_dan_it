import React, { Component, PureComponent } from 'react';

export default class Loader extends PureComponent {
	render() {
		return <div>"Loading..."</div>;
	}
}
