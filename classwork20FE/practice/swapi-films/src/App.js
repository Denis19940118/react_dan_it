import logo from './logo.svg';
import './App.css';
import { Component } from 'react';
import axios from 'axios';
import Loader from './components/Loader/Loader';
import Film from './components/Film/Film';

class App extends Component {
	state = {
		films: [],
		isLoading: true,
	};

	componentDidMount() {
		axios('https://ajax.test-danit.com/api/swapi/films').then(res =>
			this.setState({ films: res.data, isLoading: false })
		);
	}

	// filmsItems = () => {
	// 	const { films } = this.state;
	// 	const film = films.map(e => <div>e</div>);
	// };

	render() {
		const { films, isLoading } = this.state;
		console.log('🚀 ~ file: App.js ~ line 25 ~ App ~ render ~ films', films);

		if (isLoading) {
			return <Loader />;
		}

		const filmItems = films.map(e => <Film key={e.id} film={e} />);

		return <ol>{filmItems}</ol>;
	}
}

export default App;
