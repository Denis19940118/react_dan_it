// import { useEffect, useState } from 'react';
// import axios from 'axios';
// import Loader from './components/Loader/Loader';
// import Film from './components/Film/Film';
// import Films from './components/Films/Films';
import AppRoutes from './router/AppRouter';

const App = () => {
	return (
		<ol>
			<AppRoutes />
		</ol>
	);
};

export default App;
