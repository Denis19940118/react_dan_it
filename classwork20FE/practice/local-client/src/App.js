import logo from './logo.svg';
import './App.css';
import { Component } from 'react';
import Button from './components/Button/Button';
import Numbers from './components/Numbers/Numbers';

const LIMIT = 36;

class App extends Component {
	state = {
		numbers: [],
	};

	random = () => {};

	generateNumber = () => {
		const { numbers } = this.state;

		let numRandom;

		if (numbers.length >= LIMIT) {
			return;
		}

		do {
			numRandom = Math.ceil(Math.random() * LIMIT);
		} while (numbers.includes(numRandom));

		this.setState({ numbers: [...numbers, numRandom] });
	};

	deleteNumber = number => {
		const { numbers } = this.state;

		this.setState({ numbers: numbers.filter(i => i !== number) });
	};

	render() {
		const { numbers } = this.state;

		return (
			<div className="App">
				<Button
					title="Generate"
					onClick={this.generateNumber}
					disabled={numbers.length >= LIMIT}
				/>
				<Numbers numbers={numbers} deleteNumber={this.deleteNumber} />
			</div>
		);
	}
}

export default App;
