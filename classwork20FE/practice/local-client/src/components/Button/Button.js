import React, { PureComponent } from 'react';

export default class Button extends PureComponent {
	render() {
		const { title, onClick, disabled } = this.props;
		return (
			<>
				<button onClick={onClick} disabled={disabled}>
					{title}
				</button>
			</>
		);
	}
}
