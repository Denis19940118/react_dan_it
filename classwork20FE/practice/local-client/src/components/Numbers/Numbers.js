import React, { PureComponent } from 'react';
import Button from '../Button/Button';

export default class Numbers extends PureComponent {
	render() {
		const { numbers, deleteNumber } = this.props;
		const numberItems = numbers.map((el, index) => (
			<div key={index}>
				{el}
				<Button title="X" onClick={() => deleteNumber(el)} />
			</div>
		));
		return (
			<>
				<div>{numberItems}</div>
			</>
		);
	}
}
