import React from 'react';
import PropTypes from 'prop-types';

const Button = (title, onClick) => {
	return <button onClick={onClick}>{title}</button>;
};

Button.propTypes = {};

export default Button;
