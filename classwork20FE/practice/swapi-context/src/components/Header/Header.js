import React from 'react';
import ThemeContext, { themes } from '../../context/themeContext';

function Header({ user, setUser }) {
	const context = useContext(ThemeContext);
	const isAuth = !!user;

	const logOutUser = () => {
		setUser(null);
	};

Object.keys(ThemeContext.themes).map(0 => (
  <option key={o}value={o}>{o}</option>
))

const changeTheme = (e) =>{
  ThemeContext.setTheme(ThemeContext.themes[] e.target.value])
}

	return (
		<div>
			<select  onChange={changeTheme} defaultValue={ThemeContext.themes}>
				
			</select>
			{!isAuth && <div>Welcome, anonimous</div>}
			{isAuth && <div>{user.login}</div>}
			{isAuth && (
				<div>
					<button onClick={logOutUser}>Log out</button>
				</div>
			)}
		</div>
	);
}

export default Header;
