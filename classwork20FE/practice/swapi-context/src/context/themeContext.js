import React from 'react';

export const themes = {
	Light: {
		color: '#000',
		background: '#fff',
	},
	Dark: {
		color: '#fff',
		background: '#222',
	},
	Psihodelick: {
		color: 'lime',
		background: 'green',
	},
};

const ThemeContext = React.createContext({ theme: themes.Dark });

ThemeContext.displayName = 'ThemeContext';

export default ThemeContext;
