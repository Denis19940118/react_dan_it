import React, { useCallback, useState } from 'react';
import ThemeContext, { themes } from '../context/themeContext';

function ThemeProvider({ children }) {
	const [theme, setTheme] = useState(themes.Light);

	// const toggleTheme = useCallback(() => {
	//   setTheme(theme => theme === themes.dark ? themes.light : themes.dark)
	// }, [])

	const themeOptions = Object(themes);
	return (
		<ThemeContext.Provider value={{ theme, setTheme, themeOptions }}>
			{children}
		</ThemeContext.Provider>
	);
}

export default ThemeProvider;
