import React, { useRef } from 'react';
import { Redirect } from 'react-router-dom';

const Login = ({ setUser, isAuth }) => {
	const loginRef = useRef(null);
	const passwordRef = useRef(null);

	const loginUser = e => {
		e.preventDefault();

		setUser({
			login: loginRef.current.value,
			password: passwordRef.current.value,
		});
	};
	if (isAuth) {
		return <Redirect to="/" />;
	}
	return (
		<form onSubmit={loginUser}>
			<div>
				<input type="text" placeholder="Login" ref={loginRef} />
			</div>
			<div>
				<input type="password" placeholder="Password" ref={passwordRef} />
			</div>
			<button type="submit">Log in</button>
		</form>
	);
};

export default Login;
