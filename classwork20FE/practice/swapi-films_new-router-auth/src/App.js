import { useState } from 'react';
import Header from './components/Header/Header';
import AppRoutes from './router/AppRouter';

const App = () => {
	const [user, setUser] = useState(null);
	return (
		<ol>
			<Header user={user} setUser={setUser} />
			<AppRoutes user={user} setUser={setUser} />
		</ol>
	);
};

export default App;
