import logo from './logo.svg';
import './App.css';
import { Component, useEffect, useState } from 'react';
import axios from 'axios';
import Loader from './components/Loader/Loader';
import Film from './components/Film/Film';

const App = () => {
	const [films, setFilms] = useState([]);
	const [isLoading, setIsLoading] = useState(true);

	useEffect(() => {
		axios('https://ajax.test-danit.com/api/swapi/films').then(
			res => setFilms(res.data),
			setIsLoading(!isLoading)
		);
	}, []);

	// filmsItems = () => {
	// 	const { films } = this.state;
	// 	const film = films.map(e => <div>e</div>);
	// };

	if (isLoading) {
		return <Loader />;
	}

	const filmItems = films.map(e => <Film key={e.id} film={e} />);

	return <ol>{filmItems}</ol>;
};

export default App;
