import { render } from '@testing-library/react';
import { Films } from './Films';

jest.mock('../Loader/Loader', () => () => (
	<div data-testid="test-loader">Loading...</div>
));
// jest.mock('../Film/Film', props => {
// 	<div data-testid="film">props.film.name</div>;
// });
jest.mock('../Film/Film', () => props => (
	<div data-testid={`film-${props.film.id}`}>
		{props.film.name}-{props.film.id}
	</div>
));
describe('Testing films', () => {
	test('Smoke test for Films', () => {
		const { getByTestId } = render(<Films films={[]} getFilms={jest.fn()} />);
	});

	test('Smoke test for Films', () => {
		const { getByTestId } = render(
			<Films isLoading="true" getFilms={jest.fn()} />
		);
		getByTestId('test-loader');
	});

	// test('Smoke test for Films', () => {
	// 	const testFilms = [
	// 		{ id: 1, name: 'Film1' },
	// 		{ id: 2, name: 'Film2' },
	// 		{ id: 3, name: 'Film3' },
	// 	];

	// 	const { getAllByTestId } = render(
	// 		<Films films={testFilms} getFilms={jest.fn()} />
	// 	);

	// 	const filmItems = getAllByTestId('film');
	// 	expect(filmItems.length).toBe(3);
	// 	expect(filmItems[1].textContent).toBe(testFilms[1].name);
	// });

	test('Smoke test for Films', () => {
		// const loadFilmsMock = jest.fn();
		// const { getByTestId, getByText } =
		// render(<Films films={[]} loadFilms={loadFilmsMock} />);

		// expect(loadFilmsMock).toHaveBeenCalledTimes(1);

		const getFilmsMock = jest.fn();

		render(<Films isLoading="true" getFilms={getFilmsMock} />);
		expect(getFilmsMock).toHaveBeenCalledTimes(1);
	});
});
