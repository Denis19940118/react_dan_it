import logo from './logo.svg';
import React, { Component } from 'react';
import './App.css';

class App extends Component {
	state = {
		counter: 50,
		step: 5,
	};

	incrementCounter = () => {
		const { counter, step } = this.state;

		this.setState({ counter: Math.max(counter + step, 0) });
	};

	decrementCounter = () => {
		const { counter, step } = this.state;

		this.setState({ counter: Math.min(counter - step, 100) });
	};

	decrementStep = () => {
		const { step } = this.state;

		this.setState({ step: Math.max(step - 1, 1) });
	};

	incrementStep = () => {
		const { step } = this.state;

		this.setState({ step: Math.min(step + 1, 10) });
	};

	render() {
		const { counter, step } = this.state;

		return (
			<div className="App">
				<div>
					<button onClick={this.decrementCounter}>-</button>
					<h2>{counter} %</h2>
					<button onClick={this.incrementCounter}>+</button>
				</div>

				<div>
					<button onClick={this.decrementStep}>-</button>
					<h2>{step}</h2>
					<button onClick={this.incrementStep}>+</button>
				</div>
			</div>
		);
	}
}

export default App;
