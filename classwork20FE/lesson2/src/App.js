import React, { useEffect, useState } from 'react';
import './App.css';
import Header from './components/Header/Header';
import Inbox from './components/Inbox/Inbox';
import Footer from './components/Footer/Footer';
import Loader from './components/Loader/Loader';

const rawEmails = [
	{ id: 1, topic: 'Email 1' },
	{ id: 2, topic: 'Email 2' },
	{ id: 3, topic: 'Email 3' },
];

// componentDidUpdate() {
// 	// console.log('componentDidUpdate')
// }
// UNSAFE_componentWillMount() {
//   console.log('componentWillMount')
// }

const App = () => {
	const [user] = useState({ name: 'Andrew', age: 32, email: 'aaa' });
	const [emails, setEmails] = useState([]);
	const [isLoading, setIsLoading] = useState(true);
	// const [token, setToken] = useState(null);

	const getEmails = () => {
		setEmails(rawEmails);
	};

	useEffect(() => {
		setTimeout(() => {
			getEmails();
			setIsLoading(false);
			// const token = Math.random();
			// console.log('Generated new token', token);
			// setToken(token);
		}, 2000);

		return () => {
			// componentWillUnmount
		};
	}, [isLoading]);

	if (isLoading) {
		return <Loader />;
	}

	return (
		<div className="App">
			<Header user={user} />
			<Inbox emails={emails} />
			<Footer />
		</div>
	);
};

export default App;
