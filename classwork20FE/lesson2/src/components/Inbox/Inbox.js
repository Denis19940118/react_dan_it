import React from 'react';
import Email from '../Email/Email';
import PropTypes from 'prop-types';
import Button from '../Button/Button';

const Inbox = props => {
	const { emails } = props;

	const emailsCards = emails.map(el => <Email key={el.id} email={el} />);
	return (
		<div>
			<div>{emailsCards}</div>
		</div>
	);
};

Inbox.propTypes = {
	emails: PropTypes.array.isRequired,
	title: PropTypes.string,
	updateTitle: PropTypes.func.isRequired,
	incrementAge: PropTypes.func.isRequired,
};

export default Inbox;
