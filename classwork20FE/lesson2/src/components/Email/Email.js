import React from 'react';
import PropTypes from 'prop-types';

const Email = props => {
	const { email } = props;

	return (
		<>
			<div key={email.id}>{email.topic}</div>
			<div>{email.id}</div>
		</>
	);
};

//string, numgers, func, bool, array symbol ...
// share(), exact(),oneOf, oneOfType, arrayOf(), instanceOf()
Email.propTypes = {
	email: PropTypes.shape({
		id: PropTypes.number.isRequired,
		topic: PropTypes.string.isRequired,
	}).isRequired,
	// count: PropTypes.oneOf([1,2,3])
};
export default Email;
