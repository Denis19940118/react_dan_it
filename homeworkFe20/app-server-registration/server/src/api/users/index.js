import {CreateUserController} from './user.controller'
import {Router} from 'express';

const route = Router();

export default function(root) {
	root.use('/users', route);

	/**
	 * @desc Create user
	 **/
	route.post('/register', CreateUserController);
}