import {createUser} from '../../services/user.service'
import UserMapper from './user.mapper';
/**
 * @desc
 **/
export const CreateUserController = async (request, response) => {

	const {
		first_name,
		last_name,
		password,
		email,
		salt,
		city,
		gender
	} = request.body;

	const {user} = await createUser({
		first_name,
		last_name,
		password,
		email,
		salt,
		city,
		gender
	});

	response
		.status(201)
		.json({
			status: true,
			user: UserMapper(user)
		});
}