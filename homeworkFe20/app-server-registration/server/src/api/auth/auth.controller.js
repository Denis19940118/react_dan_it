import { createUser, getUser, userSignIn } from '../../services/user.service';
import { UserMapper, UserListItemMapper } from './auth.mapper';
import { BadRequestErrorHandler } from '../../errors';

/**
 * @desc
 **/
export const CreateUserController = async (request, response, next) => {
	const {
		first_name,
		last_name,
		password,
		email,
		salt,
		city,
		gender,
	} = request.body;

	try {
		const { user, token } = await createUser({
			first_name,
			last_name,
			password,
			email,
			salt,
			city,
			gender,
		});

		// await sendEmail({
		// 	to: user.email,
		// 	subject: 'Registration',
		// 	text: 'You have just been successfully registered on our Cool portal.',
		// });

		response.status(201).json({
			status: true,
			token,
			user: UserMapper(user),
		});
	} catch (e) {
		next(e);
	}
};

export const SignInController = async (request, response, next) => {
	const { email, password } = request.body;

	try {
		if (!email) {
			throw new BadRequestErrorHandler('Email not set.');
		}

		const { user, token } = await userSignIn({
			email,
			password,
		});

		response.status(200).json({
			status: true,
			token,
			user: UserMapper(user),
		});
	} catch (e) {
		next(e);
	}
};
/**
 * @desc Повертає список користувачів
 **/

export const UserListController = async (request, response, next) => {
	try {
		const { offset = 0, limit = 100 } = request.query;

		const users = await getUser({
			offset,
			limit,
		});
		console.log('users', users);
		await response.status(200).json({
			success: true,
			users: Array.isArray(users)
				? users.map(user => UserListItemMapper(user))
				: [],
		});
	} catch (e) {
		next(e);
	}

	return response.status(200).json({
		success: true,
		users,
	});
};

// /**
//  * @desc
//  **/
// export const CreateUserController = async (request, response, next) => {
// 	const {
// 		first_name,
// 		last_name,
// 		password,
// 		email,
// 		salt,
// 		city,
// 		gender,
// 	} = request.body;

// 	try {
// 		const { user, token } = await createUser({
// 			first_name,
// 			last_name,
// 			password,
// 			email,
// 			salt,
// 			city,
// 			gender,
// 		});

// 		response.status(201).json({
// 			status: true,
// 			token,
// 			user: UserMapper(user),
// 		});
// 	} catch (e) {
// 		next(e);
// 	}
// };

// export const SignInControllerMy = async (request, response, next) => {
// 	const { email, password } = request.body;

// 	try {
// 		if (!email) {
// 			throw new Error('Email not set.');
// 		}

// 		const { user, token } = await userSignIn({
// 			email,
// 			password,
// 		});

// 		response.status(200).json({
// 			status: true,
// 			token,
// 			user: UserMapper(user),
// 		});
// 	} catch (e) {
// 		next(e);
// 	}
// };
