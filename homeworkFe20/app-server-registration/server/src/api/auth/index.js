import {
	CreateUserController,
	SigninControllerMy,
	UserListController,
} from './auth.controller';
import { signInValidator, signUpValidator } from './auth.validation';
import { Router } from 'express';

const route = Router();

export default function (root) {
	root.use('/auth', route);

	route.get('/', UserListController);
	/**
	 * @desc Create user
	 **/
	route.post('/signup', signUpValidator, CreateUserController);

	/**
	 * @desc Authorize user
	 **/

	root.post('/signin', signInValidator, SigninControllerMy);
}
