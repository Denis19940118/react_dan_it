import { addNewCardAction, saveAllCardsAction } from './actions';

const LINK_PRODUCT = 'http://localhost:3005/api/v1/products';
const LINK_ADD = 'http://localhost:3005/api/v1/products/';

export const getProductsOperation = () => async (dispatch, getState) => {
	const response = await fetch(`${LINK_PRODUCT}`, {
		method: 'GET',
	});
	if (response.status === 200) {
		const json = await response.json();
		console.log(json);
		if (!!JSON.parse(localStorage.getItem('products'))) {
			localStorage.removeItem('products');
		}

		localStorage.setItem('products', JSON.stringify(json.products));
		await dispatch(saveAllCardsAction(json));
		return;
	}
	// fetch(`${LINK_PRODUCT}`, {
	// 	method: 'GET',
	// })
	// 	.then(res => res.json())
	// 	.then(res => {
	// 		console.log(res);
	// 		dispatch(saveAllCardsAction(res));
	// 	});
};

export const postAddProductsOperation = card => async (dispatch, getState) => {
	console.log('postAddProductsOperation ~ card', card);
	// const card = JSON.parse(localStorage.getItem('card'));
	// console.log(' postAddProductsOperation ~ card', card);

	const token = JSON.parse(localStorage.getItem('token'));
	console.log('getState', getState());
	// const card = getState();
	// console.log('card', card.products.cards);

	const response = await fetch(`${LINK_ADD}`, {
		method: 'POST',
		body: JSON.stringify(card),
		mode: 'cors', // no-cors, *cors, same-origin
		cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
		credentials: 'same-origin', // include, *same-origin, omit
		headers: {
			'Content-Type': 'application/json',
			Authorization: 'Token ' + token,
		},
	});

	if (response.status === 201) {
		let json = await response.json();
		dispatch(addNewCardAction(json));
		console.log('json', json);
	}
};
