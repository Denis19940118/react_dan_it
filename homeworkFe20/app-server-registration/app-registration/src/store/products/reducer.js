import { ADD_NEW_CARD, SAVE_ALL_CARDS } from './types';

const initionStore = {
	user: null,
	cards: [],
};

const reducer = (currentStore = initionStore, action) => {
	switch (action.type) {
		case SAVE_ALL_CARDS: {
			return {
				...currentStore,
				...action.payload,
			};
		}
		case ADD_NEW_CARD: {
			const card = action.payload;

			return { ...currentStore, cards: card };
		}
		default:
			return currentStore;
	}
};

export default reducer;
