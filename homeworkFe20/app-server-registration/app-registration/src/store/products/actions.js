import { ADD_NEW_CARD, SAVE_ALL_CARDS } from './types';

export const saveAllCardsAction = cards => ({
	type: SAVE_ALL_CARDS,
	payload: { cards: cards },
});

export const addNewCardAction = card => ({
	type: ADD_NEW_CARD,
	payload: card,
});
