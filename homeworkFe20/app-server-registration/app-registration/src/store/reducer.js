import { combineReducers } from 'redux';
import productReducer from '../store/products/reducer';
import userReducer from '../store/users/reducer';

const reducer = combineReducers({
	products: productReducer,
	users: userReducer,
});

export default reducer;
