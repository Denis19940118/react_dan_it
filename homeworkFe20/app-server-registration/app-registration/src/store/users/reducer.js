import { SET_CURRENT_USER, USER_REGISTRATION } from './types';

const initionStore = {
	user: null,
	userRegister: null,
};

const reducer = (state = initionStore, action) => {
	switch (action.type) {
		case SET_CURRENT_USER: {
			return { ...state, user: action.payload };
		}
		case USER_REGISTRATION: {
			return { ...state, userRegister: action.payload };
		}
		default: {
			return state;
		}
	}
};
export default reducer;
