import { SET_CURRENT_USER, USER_REGISTRATION } from './types';

export const saveUserAction = newUser => ({
	type: SET_CURRENT_USER,
	payload: newUser,
});
export const userRegisterAction = newUser => ({
	type: USER_REGISTRATION,
	payload: newUser,
});
