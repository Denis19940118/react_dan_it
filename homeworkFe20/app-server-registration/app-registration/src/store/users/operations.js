import { saveUserAction,userRegisterAction } from './actions';

const LINK_SIGNIN = 'http://localhost:3005/api/v1/auth/signin';
const LINK_REGEX = 'http://localhost:3005/api/v1/auth/signup';

export const postUserLoginOperation = user => async (dispatch, getState) => {
	console.log('postAddProductsOperation ~ card', user);

	// const token = JSON.parse(localStorage.getItem('token'));
	console.log('getState', getState());

	const response = await fetch(`${LINK_SIGNIN}`, {
		method: 'POST',
		body: JSON.stringify(user),
		mode: 'cors', // no-cors, *cors, same-origin
		cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
		credentials: 'same-origin', // include, *same-origin, omit
		headers: {
			'Content-Type': 'application/json',
			// Authorization: 'Token ' + token,
		},
	});

	if (response.status === 200) {
		let json = await response.json();
		console.log('json', json);
		localStorage.setItem('token', JSON.stringify(json.token));
		dispatch(saveUserAction(json));
	}
};


export const postUserRegisterOperation = user => async (dispatch, getState) => {
	const response = await fetch(`${LINK_REGEX}`, {
			method: 'POST',
			body: JSON.stringify(user),
			mode: 'cors', // no-cors, *cors, same-origin
			cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
			credentials: 'same-origin', // include, *same-origin, omit
			headers: {
				'Content-Type': 'application/json',
			},
		});

		if (response.status === 201) {
		let json = await response.json();
		console.log('json', json);
		dispatch(userRegisterAction(json));
	}
		
}

