import React from 'react';
import './MyInput.scss';

const MyInput = ({ field, form, ...rest }) => {
	const { className, title } = rest;
	return (
		<div className={className}>
			<label>{title}</label>
			<p className="about-input">
				<input {...field} {...rest} />
				{form.errors[field.name] && form.touched[field.name] && (
					<span className="error">{form.errors[field.name]}</span>
				)}
				{/* name={name}
					 type={type}
					 placeholder={placeholder}
					 onChange={onChange}
					 value={value} */}
			</p>
		</div>
	);
};

MyInput.propTypes = {};

export default MyInput;
