import React from 'react';
import { useDispatch } from 'react-redux';
import { Redirect, useHistory } from 'react-router';
import MyInput from '../MyInput/MyInput';
import { Formik, Form, Field } from 'formik';
import { postUserRegisterOperation } from '../../store/users/operations';

import './RegisterForms.scss';

const LINK_REGEX = 'http://localhost:3005/api/v1/auth/signup';

const RegisterForms = ({ isAuth }) => {
	const history = useHistory();
	const dispatch = useDispatch();
	// const [values, setValues] = useState({
	// 	first_name: '',
	// 	last_name: '',
	// 	password: '',
	// 	email: '',
	// 	city: '',
	// 	gender: '',
	// 	role: '',
	// });

	// console.log('values', values);

	// const handleChange = e => {
	// 	setValues({ ...values, [e.target.name]: e.target.value });
	// };

	// console.log('values', JSON.stringify({ values }));

	const clickSubmiting = async (values, { setSubmitting }) => {
		// e.preventDefault();
		setSubmitting(false);
		console.log('values1', JSON.stringify(values));

		dispatch(postUserRegisterOperation(values));

		history.push('/login');
		// const response = await fetch(`${LINK_REGEX}`, {
		// 	method: 'POST',
		// 	body: JSON.stringify(values),
		// 	mode: 'cors', // no-cors, *cors, same-origin
		// 	cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
		// 	credentials: 'same-origin', // include, *same-origin, omit
		// 	headers: {
		// 		'Content-Type': 'application/json',
		// 	},
		// });
		// history.push('/login');
		// // <Redirect exact to="/login" />;
		// return await response.json();
	};

	// useEffect(() => {
	// 	setValues(values);
	// }, [values]);
	// if (isAuth === true) {
	// 	return <Redirect to="/login" />;
	// }
	return (
		<Formik
			initialValues={{
				first_name: '',
				last_name: '',
				password: '',
				email: '',
				city: '',
				gender: '',
				role: '',
			}}
			// validate={validateForm}
			onSubmit={clickSubmiting}
		>
			{forminProps => {
				return (
					<Form>
						<h2>Form registration</h2>
						{/* <div>
							<Field
								component={MyInput}
								Name="form-input"
								name="first_name"
								title="Your first name"
								type="text"
								placeholder="Enter first name"
								// value={values.first_name}
								// onChange={handleChange}
							/>
						</div> */}
						<div>
							<Field
								component={MyInput}
								className="form-input"
								name="first_name"
								title="Your first name"
								type="text"
								placeholder="Enter first name"
								// value={values.last_name}
								// onChange={handleChange}
							/>
						</div>
						<div>
							<Field
								component={MyInput}
								className="form-input"
								name="last_name"
								title="Your second name"
								type="text"
								placeholder="Enter last name"
								// value={values.last_name}
								// onChange={handleChange}
							/>
						</div>
						<div>
							<Field
								component={MyInput}
								className="form-input"
								name="password"
								title="Your password"
								type="password"
								placeholder="Enter password"
								// value={values.password}
								// onChange={handleChange}
							/>
						</div>
						<div>
							<Field
								component={MyInput}
								className="form-input"
								name="email"
								title="Your email"
								type="email"
								placeholder="Enter email"
								// value={values.email}
								// onChange={handleChange}
							/>
						</div>
						{/* <MyInput
				className="form-input"
				name="gender"
				title="Your gender"
				type="text"
				placeholder="Enter your gender, male or female"
				value={values.gender}
				onChange={handleChange}
			/> */}
						<div className="form__gender">
							<div>
								<Field
									component={MyInput}
									className="form__gender-point"
									name="gender"
									title="Male"
									type="radio"
									value="male"
									// onChange={handleChange}
								/>
							</div>
							<div>
								<Field
									component={MyInput}
									className="form__gender-point"
									name="gender"
									title="Female"
									type="radio"
									value="female"
									// onChange={handleChange}
								/>
							</div>
						</div>
						<div>
							<Field
								component={MyInput}
								className="form-input"
								name="city"
								title="Your city"
								type="text"
								placeholder="Enter city"
								// value={values.city}
								// onChange={handleChange}
							/>
						</div>

						<div className="form__gender">
							<div>
								<Field
									component={MyInput}
									className="form__gender-point"
									name="role"
									title="Costomer"
									type="radio"
									value="customer"
									// onChange={handleChange}
								/>
							</div>
							<div>
								<Field
									component={MyInput}
									className="form__gender-point"
									name="role"
									title="Seller"
									type="radio"
									value="seller"
									// onChange={handleChange}
								/>
							</div>
							<div>
								<Field
									component={MyInput}
									className="form__gender-point"
									name="role"
									title="Admin"
									type="radio"
									value="admin"
									// onChange={handleChange}
								/>
							</div>
						</div>

						{/* <MyInput
				className="form-input"
				name="role"
				title="You role"
				type="text"
				placeholder="Enter, who are you, costumer or seller"
				value={values.role}
				onChange={handleChange}
			/> */}
						<button type="submit" disabled={forminProps.isSubmitting}>
							Submit
						</button>
					</Form>
				);
			}}
		</Formik>
	);
};

export default RegisterForms;
