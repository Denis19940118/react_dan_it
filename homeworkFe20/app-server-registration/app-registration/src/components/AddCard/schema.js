import * as yup from 'yup';

const FIELD_REQUIRED = 'This field is required';

const schema = yup.object().shape({
	name_product: yup.string().required(FIELD_REQUIRED),
	price: yup
		.string()
		.required(FIELD_REQUIRED)
		.min(1, 'price should contain at least 1 characters'),
	picture: yup.string().required(FIELD_REQUIRED),
	articule: yup.string().required(FIELD_REQUIRED),
});

export default schema;
