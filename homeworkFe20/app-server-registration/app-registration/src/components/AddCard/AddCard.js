import React from 'react';

import MyInput from '../MyInput/MyInput';
import { Formik, Form, Field } from 'formik';
import { postAddProductsOperation as postAddProducts } from '../../store/products/operations';
import { connect, useDispatch } from 'react-redux';
import { addNewCardAction } from '../../store/products/actions';
import { useHistory } from 'react-router';
import { getUserSelectorApp } from '../../store/users/selectors';

const LINK_ADD = 'http://localhost:3005/api/v1/products/';

const AddCard = ({ userLogin }) => {
	const history = useHistory();
	const dispatch = useDispatch();
	// const person = JSON.parse(localStorage.getItem('user'));
	// const token = JSON.parse(localStorage.getItem('token'));
	const { first_name, id, last_name, role } = userLogin.user;

	const registerProduct = (values, { setSubmitting }) => {
		setSubmitting(false);
		localStorage.setItem('card', JSON.stringify(values));
		// dispatch(addNewCardAction(values));
		dispatch(postAddProducts(values));
		// 	const response = await fetch(`${LINK_ADD}`, {
		// 		method: 'POST',
		// 		body: JSON.stringify(values),
		// 		mode: 'cors', // no-cors, *cors, same-origin
		// 		cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
		// 		credentials: 'same-origin', // include, *same-origin, omit
		// 		headers: {
		// 			'Content-Type': 'application/json',
		// 			Authorization: 'Token ' + token,
		// 		},
		// 	});

		// 	if (response.status === 201) {
		// 		let json = await response.json();
		// 		console.log(json);
		// 	}
		history.push('/titlePage');
	};

	return (
		<Formik
			initialValues={{
				title: '',
				price: '',
				description: '',
				articule: '',
				first_name,
				id_user: id,
				last_name,
				role,
			}}
			// validate={validateForm}
			onSubmit={registerProduct}
		>
			{forminProps => {
				console.log('forminProps', forminProps);

				return (
					<Form>
						<p>{`${first_name} ${last_name}, ${role}`}</p>
						<div>
							<Field
								component={MyInput}
								name="title"
								title="Your name product"
								type="text"
								placeholder="Enter name product"
							/>
						</div>
						<div>
							<Field
								component={MyInput}
								name="price"
								title="Your price"
								type="text"
								placeholder="Enter price"
							/>
						</div>
						<div>
							<Field
								component={MyInput}
								name="description"
								title="added description"
								type="text"
								placeholder="Add description"
							/>
						</div>
						<div>
							<Field
								component={MyInput}
								name="articule"
								title="Enter articule product"
								type="text"
								placeholder="Enter articule"
							/>
						</div>
						<div>
							<button type="submit" disabled={forminProps.isSubmitting}>
								Add product
							</button>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
};
const mapStateToProps = user => ({
	userLogin: getUserSelectorApp(user),
});

export default connect(mapStateToProps)(AddCard);
