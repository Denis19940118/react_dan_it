import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { getUserSelectorApp } from '../../store/users/selectors';
import './Head.scss';

const Head = ({ userLogin }) => {
	console.log('Head ~ loginUser', userLogin);
	// const user = JSON.parse(localStorage.getItem('user'));
	const user = userLogin;
	const [nameUser, setNameUser] = useState(null);

	const [resolutionLogin, setResolutionLogin] = useState(false);
	useEffect(() => {
		setNameUser(
			user !== null
				? `${user.user.first_name}  ${user.user.last_name}`
				: 'Guest'
		);
		if (user === null) {
			setResolutionLogin(false);
		} else if (user.user.role === 'admin') {
			setResolutionLogin(true);
		} else if (user.user.role === 'seller') {
			setResolutionLogin(true);
		} else {
			setResolutionLogin(false);
		}
	}, [user]);

	return (
		<section className="header">
			<div className="header__container">
				<div className="header__container--burger-box">
					<span className="header__container--burger"></span>
				</div>

				<div className="header__container--not-burger">
					<div>
						<NavLink exact to="/user">
							<h3>{nameUser}</h3>
						</NavLink>
					</div>
					{!userLogin && (
						<div>
							<NavLink exact to="/register">
								Registration
							</NavLink>
						</div>
					)}
					{!userLogin && (
						<div>
							<NavLink exact to="/login">
								Sign in
							</NavLink>
						</div>
					)}
					{resolutionLogin && (
						<div>
							<NavLink exact to="/createCard">
								Create Card
							</NavLink>
						</div>
					)}
					<div>
						<NavLink exact to="/titlePage">
							Home title
						</NavLink>
					</div>
				</div>
			</div>
		</section>
	);
};

const mapStateToProps = state => {
	return {
		userLogin: getUserSelectorApp(state),
	};
};

export default connect(mapStateToProps)(Head);
