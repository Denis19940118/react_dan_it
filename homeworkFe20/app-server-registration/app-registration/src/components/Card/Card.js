import React from 'react';
import PropTypes from 'prop-types';
import './Card.scss';

const Card = props => {
	const { card } = props;
	const {
		title,
		price,
		description,
		picture,
		articule,
		role,
		first_name,
		last_name,
	} = card;
	return (
		<div className="card">
			<div className="card__picture">
				<img alt={title} src={picture} />
			</div>
			<div className="card__title">
				<p>Name: {title}</p>
				<p>Price:{price} Uan</p>
				<p>Articule:{articule}</p>
				<p>Description:{description}</p>
			</div>
			<div>
				<p>
					Seller: {role}, {first_name} {last_name}
				</p>
			</div>
		</div>
	);
};

Card.propTypes = {};

export default Card;
