import React from 'react';
import PropTypes from 'prop-types';
import Card from '../Card/Card';

const ContainerCard = ({ cards }) => {
	if (cards === undefined) {
		return <div>Nothing</div>;
	}
	const card = cards.map(p => <Card key={p.articule} card={p} />);
	return <section>{card}</section>;
};

ContainerCard.propTypes = {
	cards: PropTypes.array,
};

export default ContainerCard;
