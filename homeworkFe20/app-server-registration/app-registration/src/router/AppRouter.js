import React from 'react';
import { Redirect, Route, Switch } from 'react-router';
import Register from '../pages/Register/Register';
import Login from '../pages/Login/Login';
import UserPage from '../pages/UserPage/UserPage';
import TitlePage from '../pages/TitlePage/TitlePage';
import ContainerCard from '../components/ContainerCard/ContainerCard';
import CreateCard from '../pages/CreateCad/CreateCard';
import { connect } from 'react-redux';
import { getUserSelectorApp } from '../store/users/selectors';

const AppRouter = ({ user }) => {
	console.log('user', user);

	const isAuth = !!user;
	console.log('AppRouter ~ isAuth', isAuth);

	const products = <ContainerCard />;

	return (
		<div>
			<Switch>
				<Redirect exact from="/" to="/titlePage" />
				<Route exact path="/sidebar">
					{products}
				</Route>
				<Route exact path="/register">
					<Register />
				</Route>
				<Route exact path="/login">
					<Login isAuth={isAuth} />
				</Route>
				<Route exact path="/titlePage">
					<TitlePage />
				</Route>
				<ProtectedRoute exact path="/createCard" isAuth={isAuth}>
					<CreateCard />
				</ProtectedRoute>
				<ProtectedRoute exact path="/user" isAuth={isAuth}>
					<UserPage />
				</ProtectedRoute>
			</Switch>
		</div>
	);
};

export const ProtectedRoute = ({ children, isAuth, ...rest }) => {
	// const user = JSON.parse(localStorage.getItem('user'));

	return (
		<Route
			{...rest}
			render={() => {
				if (isAuth) {
					return children;
				} else {
					return <Redirect to="/login" />;
				}
			}}
		/>
	);
};

export const ProtectedRouteSeller = ({ children, isAuthSel, ...rest }) => {
	// const user = JSON.parse(localStorage.getItem('user'));
	switch (isAuthSel) {
		case 'admin':
			return (
				<Route
					{...rest}
					render={() => {
						if (isAuthSel) {
							return children;
						} else {
							return <Redirect to="/titlePage" />;
						}
					}}
				/>
			);
		case 'seller':
			return (
				<Route
					{...rest}
					render={() => {
						if (isAuthSel) {
							return children;
						} else {
							return <Redirect to="/titlePage" />;
						}
					}}
				/>
			);
		default:
			return (
				<Route
					{...rest}
					render={() => {
						if (isAuthSel) {
							return children;
						} else {
							return <Redirect to="/titlePage" />;
						}
					}}
				/>
			);
	}
};

const mapStateToProps = state => {
	console.log('AppRouter ~ state', state);
	return { user: getUserSelectorApp(state) };
};

export default connect(mapStateToProps)(AppRouter);
