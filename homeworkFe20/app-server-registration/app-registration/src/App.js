import './App.scss';
import Head from './components/Head/Head';
import AppRouter from './router/AppRouter';
import { useState } from 'react';

function App() {
	// const [user, setUser] = useState(JSON.parse(localStorage.getItem('user')));
	// const loginUser = !!user;
	return (
		<div className="App">
			<Head />
			<AppRouter />
		</div>
	);
}

export default App;
