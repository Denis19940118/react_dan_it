import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import ContainerCard from '../../components/ContainerCard/ContainerCard';
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { selectCards } from '../../store/products/selectors';
import { getProductsOperation as getProducts } from '../../store/products/operations';
// const LINK_PRODUCT = 'http://localhost:3005/api/v1/products';

const TitlePage = props => {
	const history = useHistory();
	const dispatch = useDispatch();

	// const clickSubmiting = async () => {

	// 	const response = await fetch(`${LINK_PRODUCT}`, {
	// 		method: 'GET',
	// 	});

	// 	if (response.status === 200) {
	// 		const json = await response.json();
	// 		console.log(json);
	// 		if (!!JSON.parse(localStorage.getItem('products'))) {
	// 			localStorage.removeItem('products');
	// 		}

	// 		localStorage.setItem('products', JSON.stringify(json.products));

	// 		// setUser(JSON.parse(localStorage.getItem('token')));
	// 		// history.push(`/titlePage`);
	// 		return;
	// 	}
	// };
	const cards = useSelector(selectCards);

	useEffect(() => {
		dispatch(getProducts());
	}, [dispatch]);

	return (
		<div>
			<ContainerCard cards={cards} />
		</div>
	);
};

TitlePage.propTypes = {};

export default TitlePage;
