import React from 'react';
import PropTypes from 'prop-types';
import RegisterForms from '../../components/RegisterForms/RegisterForms';
import './Register.scss';

const Register = props => {
	return (
		<div className="register">
			<RegisterForms />
		</div>
	);
};

Register.propTypes = {};

export default Register;
