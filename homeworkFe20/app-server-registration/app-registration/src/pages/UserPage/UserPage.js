import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router';
import { connect } from 'react-redux';
import { getUserSelectorApp } from '../../store/users/selectors';
import { saveUserAction } from '../../store/users/actions';

const UserPage = ({ userLogin, dataUser }) => {
	const history = useHistory();
	// const user = JSON.parse(localStorage.getItem('user'));
	console.log('user', userLogin);
	const user = userLogin.user;

	const clickOut = () => {
		localStorage.removeItem('user');
		localStorage.removeItem('token');
		dataUser(null);
		// setUser(false);

		history.push('/login');
	};

	return (
		<section>
			<h3>
				You are authrized{' '}
				{`${user.first_name}  ${user.last_name}, like ${user.role}`}
			</h3>
			<p>You are gender {user.gender}</p>
			<p>Tour mail is {user.email}</p>
			<p>Your native city {user.city}</p>
			<button type="submit" onClick={() => clickOut()}>
				Sign out
			</button>
		</section>
	);
};

UserPage.propTypes = {};

const mapStateToProps = user => ({
	userLogin: getUserSelectorApp(user),
});

const mapDispatchToProps = dispatch => ({
	dataUser: dataUser => dispatch(saveUserAction(dataUser)),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserPage);
