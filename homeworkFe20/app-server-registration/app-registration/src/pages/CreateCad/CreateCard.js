import React from 'react';
import PropTypes from 'prop-types';
import './CreateCard.scss';
import AddCard from '../../components/AddCard/AddCard';

const CreateCard = props => {
	return (
		<div>
			<h4>Create card new product</h4>
			<AddCard />
		</div>
	);
};

CreateCard.propTypes = {};

export default CreateCard;
