import React from 'react';
import MyInput from '../../components/MyInput/MyInput';
import { Redirect, useHistory } from 'react-router';
import { Formik, Form, Field, connect } from 'formik';
import { getUserSelectorApp } from '../../store/users/selectors';
import { saveUserAction } from '../../store/users/actions';
import { postUserLoginOperation as postUserLogin } from '../../store/users/operations';
import { useDispatch } from 'react-redux';
// const LINK_AUTH = 'http://localhost:3005/api/v1/auth/signin';

const Login = ({ isAuth }) => {
	// console.log('Login ~ newUser', newUser);
	const dispatch = useDispatch();
	// const [values, setValues] = useState({
	// 	email: '',
	// 	password: '',
	// });
	// const history = useHistory();

	// const handleChange = e => {
	// 	setValues({ ...values, [e.target.name]: e.target.value });
	// };

	// useEffect(() => {
	// 	setValues(values);
	// }, [values]);

	const clickSubmiting = async (values, { setSubmitting }) => {
		setSubmitting(false);
		localStorage.setItem('userLogin', JSON.stringify(values));
		dispatch(postUserLogin(values));

		// newUser(values);

		// const response = await fetch(`${LINK_AUTH}`, {
		// 	method: 'POST',
		// 	body: JSON.stringify(values),
		// 	mode: 'cors', // no-cors, *cors, same-origin
		// 	cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
		// 	credentials: 'same-origin', // include, *same-origin, omit
		// 	headers: {
		// 		'Content-Type': 'application/json',
		// 	},
		// });

		// if (response.status === 200) {
		// 	const json = await response.json();
		// 	console.log(json);
		// 	localStorage.removeItem('token');
		// 	localStorage.removeItem('user');
		// 	localStorage.setItem('token', JSON.stringify(json.token));
		// 	localStorage.setItem('user', JSON.stringify(json.user));
		// 	setUser(JSON.parse(localStorage.getItem('token')));
		// 	switch (json.user.role) {
		// 		case 'admin':
		// 			history.push(`/user`);
		// 			break;
		// 		case 'seller':
		// 			history.push(`/createCard`);
		// 			break;
		// 		default:
		// 			history.push(`/titlePage`);
		// 			break;
		// 	}
		// 	return;
		// }
	};

	if (isAuth) {
		return <Redirect to="/" />;
	}

	return (
		<Formik
			initialValues={{
				email: '',
				password: '',
			}}
			onSubmit={clickSubmiting}
		>
			{forminProps => {
				return (
					<Form>
						<div>
							<Field
								component={MyInput}
								className="form-input"
								name="email"
								title="Your email"
								type="email"
								placeholder="Enter email"
								// value={values.email}
								// onChange={handleChange}
							/>
						</div>
						<div>
							<Field
								component={MyInput}
								className="form-input"
								name="password"
								title="Your password"
								type="password"
								placeholder="Enter password"
								// value={values.password}
								// onChange={handleChange}
							/>
						</div>
						<button type="submit" disabled={forminProps.isSubmitting}>
							Sign in
						</button>
					</Form>
				);
			}}
		</Formik>
	);
};

// const mapDispatchToProps = dispatch => ({
// 	newUser: newUser => dispatch(saveUserAction(newUser)),
// });

export default Login;
