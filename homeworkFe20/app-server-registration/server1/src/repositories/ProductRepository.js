import BaseRepository from './BaseRepository';

export class ProductRepository extends BaseRepository {
	async find(options) {
		return this.models.ProductModel.find(options);
	}

	async create(options) {
		return this.models.ProductModel.create(options);
	}
}
