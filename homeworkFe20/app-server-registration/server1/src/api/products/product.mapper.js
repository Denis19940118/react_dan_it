import { addCreationAndUpdatingDate } from '../../utils/entities';

export const ProductListItemMapper = product => ({
	id: product._id,
	title: product.title,
	price: parseFloat(product.price),
	articule: parseFloat(product.articule),
	first_name: product.first_name,
	id_user: product.id_user,
	last_name: product.last_name,
	role: product.role,
});

/**
 *
 **/
export const ProductMapper = product => ({
	id: product._id,
	title: product.title,
	description: product.description,
	price: parseFloat(product.price),
	articule: parseFloat(product.articule),
	first_name: product.first_name,
	id_user: product.id_user,
	last_name: product.last_name,
	role: product.role,
	...addCreationAndUpdatingDate(product),
});
