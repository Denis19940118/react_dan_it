import React, { useState } from 'react';
import Head from '../components/Head/Head';
import SideBar from '../components/SideBar/SideBar';
import { Redirect, Route, Switch } from 'react-router-dom';
import Start from '../pages/Start/Start';
import Favorites from '../pages/Favorite/Favorites';
import Basket from '../pages/Basket/Basket';
import CardDetails from '../components/CardDetails/CardDetails';
import Confirmation from '../pages/Confirmation/Confirmation';

const AppRouter = () => {
	const [isFavorite] = useState(
		JSON.parse(localStorage.getItem('favorite')) ||
			localStorage.setItem('favorite', JSON.stringify([]))
	);
	const [isBasket] = useState(
		JSON.parse(localStorage.getItem('basket')) ||
			localStorage.setItem('basket', JSON.stringify([]))
	);
	const [start] = useState('/start');
	const [favorite] = useState('/favorite');
	const [basket] = useState('/basket');
	const [cardD] = useState('/:card');
	const [confirm] = useState('/confirm');

	return (
		<section>
			<Head className="header" title={<SideBar />} />
			<Redirect exact from="/" to={start} />
			<Switch>
				<Route exact path={start}>
					<Start url={start} cardD={cardD} />
				</Route>
				<Route exact path={`${start}${cardD}`}>
					<CardDetails url={start} cardD={cardD} />
				</Route>
				<Route exact path={favorite}>
					<Favorites url={favorite} cardD={cardD} />
				</Route>
				<Route exact path={`${favorite}${cardD}`}>
					<CardDetails url={favorite} cardD={cardD} />
				</Route>
				<Route exact path={basket}>
					<Basket url={basket} cardD={cardD} confirm={confirm} />
				</Route>
				<Route exact path={`${basket}${cardD}`}>
					<CardDetails url={basket} cardD={cardD} />
				</Route>
				<Route exact path={`${confirm}`}>
					<Confirmation url={basket} start={start} />
				</Route>
			</Switch>
		</section>
	);
};

export default AppRouter;
