import React from 'react';
import './Head.scss';

const Head = props => {
	const { className, title } = props;

	return <div className={className}>{title}</div>;
};
export default Head;
