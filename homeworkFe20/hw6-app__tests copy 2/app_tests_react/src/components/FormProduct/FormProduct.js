import React, { useEffect, useState } from 'react';
import Button from '../Button/Button';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
	saveDataProductsUser,
	saveDataUser,
} from '../../redux/userOrder/actions';
import { useHistory } from 'react-router';
import { selectCards } from '../../redux/profile/selectors';
import '../FormProduct/FormProduct.scss';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const FormProduct = ({ cards, dataUser, confirm, products }) => {
	const [values, setValues] = useState({
		firstName: '',
		secondName: '',
		age: '',
		age: '',
		town: '',
		street: '',
		house: '',
		entrance: '',
		apartment: '',
		phone: '',
		mail: 'admin@test.com',
	});
	const [errors, setErrors] = useState({});
	const [touched, setTouched] = useState({});

	const [basket, setBasket] = useState([]);

	const history = useHistory();

	useEffect(() => {
		setBasket(JSON.parse(localStorage.getItem('basket')));
	}, []);

	const toBasket = cards.filter(el => basket.some(b => el.id === b));
	console.log('toBasket', toBasket);

	const validateForm = () => {
		const {
			firstName,
			secondName,
			age,
			town,
			street,
			house,
			entrance,
			apartment,
			phone,
			mail,
		} = values;
		const errors = {};

		if (!EMAIL_REGEX.test(mail)) {
			errors.mail = 'This is not a valid email';
		}

		if (!mail) {
			errors.mail = 'This field is required';
		}

		if (!firstName) {
			errors.firstName = 'This field is required';
		}

		if (!secondName) {
			errors.secondName = 'This field is required';
		}

		if (age === 0 || age < 16 || age > 150) {
			errors.age = 'This field is not true age';
		}

		if (!age) {
			errors.age = 'This field is required';
		}

		if (!town) {
			errors.town = 'This field is required';
		}

		if (!street) {
			errors.street = 'This field is required';
		}

		if (!house) {
			errors.house = 'This field is required';
		}

		if (!entrance) {
			errors.entrance = 'This field is required';
		}

		if (!apartment) {
			errors.apartment = 'This field is required';
		}

		if (!phone) {
			errors.phone = 'This field is required';
		}

		if (phone.length < 10) {
			errors.phone = 'This field is false';
		}
		setErrors(errors);
		return Object.keys(errors).length === 0;
	};

	const register = e => {
		e.preventDefault();

		setTouched({
			mail: true,
			firstName: true,
			secondName: true,
			age: true,
			town: true,
			street: true,
			house: true,
			entrance: true,
			apartment: true,
			phone: true,
		});
		const isValid = validateForm();

		if (!isValid) {
			return;
		}
		localStorage.removeItem('basket');
		localStorage.setItem('basket', JSON.stringify([]));
		dataUser(values);
		products(toBasket);
		console.log('toBasket', toBasket);
		console.log('products', products);

		console.log(values);

		history.push(`${confirm}`);
	};

	const handleChange = e => {
		setValues({ ...values, [e.target.name]: e.target.value });
	};

	const handleTouched = e => {
		setTouched({ ...touched, [e.target.name]: true });
	};

	useEffect(() => {
		validateForm();
	}, [values]);

	const buyProduct = e => {
		localStorage.removeItem('basket');
		localStorage.setItem('basket', JSON.stringify([]));
		dataUser(values);

		products(toBasket);
		console.log(
			'🚀 ~ file: FormProduct.js ~ line 125 ~ FormProduct ~ toBasket',
			toBasket
		);

		history.push(`${confirm}`);
	};

	return (
		<form className="form__client" onSubmit={register} noValidate>
			<h3>Form buy in shop</h3>
			<div>
				<input
					name="firstName"
					type="text"
					placeholder="Your first name"
					value={values.firstName}
					onChange={handleChange}
					onBlur={handleTouched}
				/>
				{errors.firstName && touched.firstName && (
					<span className="error">{errors.firstName}</span>
				)}
			</div>
			<div>
				<input
					name="secondName"
					type="text"
					placeholder="Your second name"
					value={values.secondName}
					onChange={handleChange}
					onBlur={handleTouched}
				/>
				{errors.secondName && touched.secondName && (
					<span className="error">{errors.secondName}</span>
				)}
			</div>
			<div>
				<input
					name="age"
					type="number"
					placeholder="Your age"
					value={values.age}
					onChange={handleChange}
					onBlur={handleTouched}
				/>
				{errors.age && touched.age && (
					<span className="error">{errors.age}</span>
				)}
			</div>
			<div>
				<input
					name="town"
					type="text"
					placeholder="Your town"
					value={values.town}
					onChange={handleChange}
					onBlur={handleTouched}
				/>
				{errors.town && touched.town && (
					<span className="error">{errors.town}</span>
				)}
			</div>
			<div>
				<input
					name="street"
					type="text"
					placeholder="Your street"
					value={values.street}
					onChange={handleChange}
					onBlur={handleTouched}
				/>
				{errors.street && touched.street && (
					<span className="error">{errors.street}</span>
				)}
			</div>
			<div>
				<input
					name="house"
					type="text"
					placeholder="Your house number"
					value={values.house}
					onChange={handleChange}
					onBlur={handleTouched}
				/>
				{errors.house && touched.house && (
					<span className="error">{errors.house}</span>
				)}
			</div>
			<div>
				<input
					name="entrance"
					type="text"
					placeholder="Your entrance number"
					value={values.entrance}
					onChange={handleChange}
					onBlur={handleTouched}
				/>
				{errors.entrance && touched.entrance && (
					<span className="error">{errors.entrance}</span>
				)}
			</div>
			<div>
				<input
					name="apartment"
					type="text"
					placeholder="Your apartment number"
					value={values.apartment}
					onChange={handleChange}
					onBlur={handleTouched}
				/>
				{errors.apartment && touched.apartment && (
					<span className="error">{errors.apartment}</span>
				)}
			</div>
			<div>
				<input
					name="phone"
					type="phone"
					placeholder="Your phone"
					value={values.phone}
					onChange={handleChange}
					onBlur={handleTouched}
				/>
				{errors.phone && touched.phone && (
					<span className="error">{errors.phone}</span>
				)}
			</div>
			<div>
				<input
					name="mail"
					type="mail"
					placeholder="Your mail"
					value={values.mail}
					onChange={handleChange}
					onBlur={handleTouched}
				/>
				{errors.mail && touched.mail && (
					<span className="error">{errors.mail}</span>
				)}
			</div>
			<Button type="submit" title="Buy" onSubmit={buyProduct} />
		</form>
	);
};

const mapDispatchToProps = dispatch => ({
	dataUser: dataUser => dispatch(saveDataUser(dataUser)),
	products: products => dispatch(saveDataProductsUser(products)),
	// saveAllCards: cards => dispatch(saveAllCardsAction(cards)),
});

const mapStateToProps = currentStore => ({
	cards: selectCards(currentStore),
});

FormProduct.propTypes = {
	cards: PropTypes.array.isRequired,
	// dataUser: PropTypes.object.isRequired,
	confirm: PropTypes.string,
};

export default connect(
	// () => ({}),
	mapStateToProps,
	mapDispatchToProps
)(FormProduct);
