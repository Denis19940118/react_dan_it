import './App.scss';
import React, { useEffect } from 'react';
import AppRouter from './router/AppRouter';
import { saveAllCardsAction } from './redux/profile/action';
import { connect } from 'react-redux';

function App({ saveAllCards }) {
	// const [cards, setCards] = useState(null);

	useEffect(() => {
		fetch('/api/cards')
			.then(res => res.json())
			.then(res => {
				saveAllCards(res);
			});
	}, []);

	return (
		<section className="app__container app__container:after">
			<AppRouter />
		</section>
	);
}
const mapDispatchToProps = dispatch => ({
	saveAllCards: cards => dispatch(saveAllCardsAction(cards)),
});
export default connect(() => ({}), mapDispatchToProps)(App);
// export default App;
