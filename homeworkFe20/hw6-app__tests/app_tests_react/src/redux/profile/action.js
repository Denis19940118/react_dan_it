import {
	SAVE_ALL_CARDS,
	// SAVE_DATA_PRODUCT_USER,
	// SAVE_DATA_USER,
} from './actionType';

export const saveAllCardsAction = cards => ({
	type: SAVE_ALL_CARDS,
	payload: { cards: cards },
});

// export const saveDataProductsUser = products => ({
// 	type: SAVE_DATA_PRODUCT_USER,
// 	payload: { products: products },
// });

// export const saveDataUser = dataUser => ({
// 	type: SAVE_DATA_USER,
// 	payload: { dataUser: dataUser },
// });
