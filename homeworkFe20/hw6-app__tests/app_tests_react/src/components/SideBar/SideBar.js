import React from 'react';
import { NavLink } from 'react-router-dom';
import './SideBar.scss';

function SideBar(props) {
	return (
		<ul className="sidebar">
			<li className="sidebar__item">
				<NavLink
					exact
					to="/start"
					className="link"
					activeClassName="link__active"
				>
					Start
				</NavLink>
			</li>
			<li className="sidebar__item">
				<NavLink
					exact
					to="/favorite"
					className="link"
					activeClassName="link__active"
				>
					Favorite
				</NavLink>
			</li>
			<li className="sidebar__item">
				<NavLink
					exact
					to="/basket"
					className="link"
					activeClassName="link__active"
				>
					Basket
				</NavLink>
			</li>
		</ul>
	);
}

export default SideBar;
