import React, { useState } from 'react';
import Button from '../Button/Button';
import Modal from '../Modal/Modal';
import './ModalContainer.scss';

const ModalContainer = props => {
	const [opened, setOpened] = useState(false);
	const { name, text, btn1, btn2, color } = props;

	return (
		<>
			<Button
				className="btn"
				title={name}
				backgroundColor={color}
				onClick={() => setOpened(!opened)}
			/>
			{opened && (
				<Modal
					title={text}
					action={
						<>
							<Button
								className="btn-modal"
								title={'Ok'}
								onClick={() => setOpened(!opened)}
								backgroundColor={btn1}
							/>
							<Button
								className="btn-modal"
								title={'X'}
								onClick={() => setOpened(!opened)}
								backgroundColor={btn2}
							/>
						</>
					}
					opened={opened}
					setOpened={setOpened}
				/>
			)}
		</>
	);
};

export default ModalContainer;
