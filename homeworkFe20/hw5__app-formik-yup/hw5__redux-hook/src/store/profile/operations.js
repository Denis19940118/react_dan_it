import { saveAllCardsAction } from './action';

export const getProductsOperation = () => (dispatch, getState) => {
	fetch('/api/cards')
		.then(res => res.json())
		.then(res => {
			dispatch(saveAllCardsAction(res.data));
		});
};
