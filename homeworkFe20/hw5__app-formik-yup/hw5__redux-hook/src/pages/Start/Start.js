import React from 'react';
import PropTypes from 'prop-types';
import ContainerCard from '../../components/ContainerCard/ContainerCard';
import { selectCards } from '../../redux/profile/selectors';
import { useSelector } from 'react-redux';
// import { getProductsOperation as getProducts } from '../../redux/profile/operations';

const Start = props => {
	const { url, cardD } = props;
	// const dispatch = useDispatch();
	const cards = useSelector(selectCards);

	// useEffect(() => {
	// 	dispatch(getProducts());
	// }, [dispatch]);

	return (
		<div>
			<ContainerCard
				title={'Product items in your basket room'}
				cards={cards}
				btnDel={false}
				url={url}
				cardD={cardD}
			/>
		</div>
	);
};

Start.propTypes = {
	cards: PropTypes.array,
};

export default Start;
