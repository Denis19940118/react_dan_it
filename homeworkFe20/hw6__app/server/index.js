const express = require('express');

const app = express();

const cards = [
	{
		id: '1',
		name: 'Brit adult salmon',
		price: '100',
		picture: './picture/brit-adult-salmon.jpg',
		articule: '123425678',
		color: 'yellow',
	},
	{
		id: '2',
		name: 'Brit premium adult cat chicken',
		price: '180',
		picture: './picture/brit-premium-adult-cat-chicken.jpg',
		articule: '098765345',
		color: 'yellow',
	},
	{
		id: '3',
		name: 'Brit premium cat sterilized',
		price: '140',
		picture: './picture/brit-premium-cat-sterilized.jpg',
		articule: '098766789',
		color: 'yellow',
	},
	{
		id: '4',
		name: 'Friskies darling salmon',
		price: '140',
		picture: './picture/friskies_darling-salmon.jpg',
		articule: '123451234',
		color: 'yellow',
	},
	{
		id: '5',
		name: 'Friskies junior',
		price: '170',
		picture: './picture/purina-friskies-junior.jpg',
		articule: '234567809',
		color: 'yellow',
	},
	{
		id: '6',
		name: 'Royal canin sterilised',
		price: '160',
		picture: './picture/koshek-royal-canin-sterilised.jpg',
		articule: '987650234',
		color: 'yellow',
	},
	{
		id: '7',
		name: 'Royal canin savoir exigent',
		price: '155',
		picture: './picture/Royal-Canin-Savoir-Exigent.jpg',
		articule: '678345098',
		color: 'yellow',
	},
	{
		id: '8',
		name: 'Royal canin sterilised appetite control',
		price: '150',
		picture: './picture/Royal-Canin-Sterilised-Appetite-Control.jpg',
		articule: '123456734',
		color: 'yellow',
	},
	{
		id: '9',
		name: 'Cat sensitive',
		price: '160',
		picture: './picture/suhoj-korm-cat-Sensitive.jpg',
		articule: '678901232',
		color: 'yellow',
	},
	{
		id: '10',
		name: 'Special 3in1 cat-chow',
		price: '190',
		picture: './picture/special-3-in-1-cat-chow.jpg',
		articule: '987623454',
		color: 'yellow',
	},
	{
		id: '11',
		name: 'Special care sterrilized cat-chow',
		price: '145',
		picture: './picture/special-care-sterrilized-cat-chow.jpg',
		articule: '876545412',
		color: 'yellow',
	},
	{
		id: '12',
		name: 'Cat elegant',
		price: '145',
		picture: './picture/suhoj-korm-dlya-elegant.jpg',
		articule: '876545412',
		color: 'yellow',
	},
	{
		id: '13',
		name: 'Purina pro plan sterilised',
		price: '145',
		picture: './picture/purina_pro_plan_sterilised.jpg',
		articule: '876545412',
		color: 'yellow',
	},
];

app.use(express.static(__dirname + '/public'));

app.get('/api/cards', (req, res) => {
	res.send(cards);
});

app.get('/api/cards/:cardId', (req, res) => {
	const cardId = req.params;
	res.send(cards.find(e => e.id === +cardId));
});

app.get('*', (req, res) => {
	res.sendFile(__dirname + '/public/index.html');
});

app.get('', (req, res) => {});

const port = process.env.PORT || 8085;

app.listen(port, () => {
	console.log(`Server listening on port ${port}`);
});
