import { render, unmountComponentAtNode } from 'react-dom';
import { act } from 'react-dom/test-utils';
import Favorites from './Favorites';

const testCards = [
	{
		id: '1',
		name: 'Test1',
		price: '100',
		picture: './picture/brit-adult-salmon.jpg',
		articule: '123425678',
		color: 'yellow',
		favorite: true,
	},
	{
		id: '2',
		name: 'Test2',
		price: '180',
		picture: './picture/brit-premium-adult-cat-chicken.jpg',
		articule: '098765345',
		color: 'yellow',
	},
	{
		id: '3',
		name: 'Test3',
		price: '140',
		picture: './picture/brit-premium-cat-sterilized.jpg',
		articule: '098766789',
		color: 'yellow',
	},
	{
		id: '4',
		name: 'Test4',
		price: '140',
		picture: './picture/friskies_darling-salmon.jpg',
		articule: '123451234',
		color: 'yellow',
	},
];

jest.mock('../../components/ContainerCard/ContainerCard', () => () => (
	<div data-testid="test-card"></div>
));

let container = null;

beforeEach(() => {
	container = document.createElement('div');
	document.body.appendChild(container);
});

// afterEach(() => {
// 	unmountComponentAtNode(container);
// 	container.remove();
// 	container = null;
// });

describe('Testing Favorites', () => {
	test('Smoke test Favorites', () => {
		act(() => {
			render(<Favorites />, container);
		});
	});
});
