// import { render } from '@testing-library/react';
import Start from './Start';
import * as reactRedux from 'react-redux';
import { render } from '@testing-library/react';

const testCards = [
	{
		id: '1',
		name: 'Test1',
		price: '100',
		picture: './picture/brit-adult-salmon.jpg',
		articule: '123425678',
		color: 'yellow',
		favorite: true,
	},
	{
		id: '2',
		name: 'Test2',
		price: '180',
		picture: './picture/brit-premium-adult-cat-chicken.jpg',
		articule: '098765345',
		color: 'yellow',
	},
	{
		id: '3',
		name: 'Test3',
		price: '140',
		picture: './picture/brit-premium-cat-sterilized.jpg',
		articule: '098766789',
		color: 'yellow',
	},
	{
		id: '4',
		name: 'Test4',
		price: '140',
		picture: './picture/friskies_darling-salmon.jpg',
		articule: '123451234',
		color: 'yellow',
	},
];

jest.mock('../../components/ContainerCard/ContainerCard', () => () => (
	<div data-testid="test-card"></div>
));

describe('Testing Start', () => {
	const useSelectorMock = jest.spyOn(reactRedux, 'useSelector');

	beforeEach(() => {
		useSelectorMock.mockClear();
	});

	test('Smoke test Start', () => {
		useSelectorMock.mockReturnValue(testCards);

		render(<Start url={'url'} cardID={'cardID'} />);
	});

	// it('Length cards test Start', () => {
	// 	useSelectorMock.mockReturnValue(testCards);

	// 	const { getByTestId } = render(<Start url={'url'} cardID={'cardID'} />);

	// 	const cardsTest = getByTestId('test-card');
	// 	expect(cardsTest).toBe(4);
	// });
});
