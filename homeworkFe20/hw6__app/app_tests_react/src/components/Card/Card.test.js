import {
	cleanup,
	fireEvent,
	getByTestId,
	render,
} from '@testing-library/react';
import { Provider } from 'react-redux';
import store from '../../redux/store';
import Card from './Card';

afterEach(cleanup);

jest.mock('react-router-dom', () => ({
	...jest.requireActual('react-router-dom'),
	useHistory: () => ({
		push: jest.fn(),
	}),
}));

const testCards = {
	id: '1',
	name: 'Test1',
	price: '100',
	picture: './picture/brit-adult-salmon.jpg',
	articule: '123425678',
	color: 'yellow',
	favorite: true,
};

describe('Testing Card', () => {
	test('Smoke test Card', () => {
		render(
			<Provider store={store}>
				<Card
					id={testCards.id}
					card={testCards}
					name={testCards.name}
					price={testCards.price}
					picture={testCards.picture}
					articule={testCards.articule}
					color={testCards.color}
					openedFav={true}
					btnDel={true}
					btnDetail={true}
				/>
			</Provider>
		);
	});

	test('test button Detail', () => {
		const setCardMock = jest.fn();

		// const { getByTestId } =
		render(
			<Provider store={store}>
				<Card
					id={testCards.id}
					card={testCards}
					name={testCards.name}
					price={testCards.price}
					picture={testCards.picture}
					articule={testCards.articule}
					color={testCards.color}
					openedFav={true}
					btnDel={true}
					btnDetail={true}
				/>
			</Provider>
		);

		const cardButton = getByTestId('cardButton');
		expect(cardButton).toBeInTheDocument();
		expect(setCardMock).not.toHaveBeenCalled();
		fireEvent.click(cardButton);
		expect(setCardMock).toHaveBeenCalledTimes(1);
	});
});
