import React, { useEffect, useState } from 'react';
import Button from '../Button/Button';
import PropTypes from 'prop-types';
import Icon from '../Icon/Icon';
import Modal from '../Modal/Modal';
import { useHistory } from 'react-router-dom';
import './Card.scss';
import { useDispatch } from 'react-redux';
import {
	buyProductAction,
	delBuyProductAction,
	toggleFavoritesAction,
} from '../../redux/products/actions';

const Card = props => {
	const [opened, setOpened] = useState(false);
	const [openedBas, setOpenedBas] = useState(false);
	const [delBas, setDelBas] = useState(false);
	const dispatch = useDispatch();

	const {
		id,
		card,
		name,
		price,
		picture,
		articule,
		color,
		openedFav,
		btnDel,
		btnDetail,
		url,
		favorite,
	} = props;

	const history = useHistory();
	const favoriteLocal = JSON.parse(localStorage.getItem('favorite'));

	// useEffect(() => {
	// 	favoriteLocal.filter(el => (el === id ? setOpened(!opened) : ''));
	// }, [id]);

	const handleClickToFavorite = op => {
		if (op === true) {
			const favorite = JSON.parse(localStorage.getItem('favorite'));
			localStorage.removeItem('favorite');

			favorite.push(id);
			localStorage.setItem('favorite', JSON.stringify(favorite));
		} else {
			const i = JSON.parse(localStorage.getItem('favorite'));
			localStorage.removeItem('favorite');

			const fav = [];
			i.map(el => (el !== id ? fav.push(el) : ''));
			localStorage.setItem('favorite', JSON.stringify(fav));
		}
	};

	const clickToBasket = () => {
		dispatch(buyProductAction(id));
		const basket = JSON.parse(localStorage.getItem('basket'));
		localStorage.removeItem('basket');
		basket.push(id);
		localStorage.setItem('basket', JSON.stringify(basket));
	};

	const clickDelCardBasket = () => {
		dispatch(delBuyProductAction(id));
		const arr = JSON.parse(localStorage.getItem('basket'));
		localStorage.removeItem('basket');

		const bas = [];
		arr.map(el => (el !== id ? bas.push(el) : ''));
		localStorage.setItem('basket', JSON.stringify(bas));
	};

	const addedCard = () => {
		clickToBasket();
		setOpenedBas(!openedBas);
	};

	const deletedCard = () => {
		clickDelCardBasket();
		setDelBas(!delBas);
	};

	const changeColor = () => {
		dispatch(toggleFavoritesAction(card));
		setOpened(openedFav || !opened);
		handleClickToFavorite(!opened);
	};

	const showDetails = () => {
		history.push(`${url}/${id}`);
		localStorage.setItem('Card', JSON.stringify(id));
	};

	return (
		<article className="card ">
			<div className=" card__container">
				<Icon
					className="card__star"
					type={'star'}
					color={color}
					filled={favorite}
					onClick={() => changeColor()}
				/>
				<div className="card__img-container">
					<img
						className="card__img"
						src={`.${picture}`}
						alt={name}
						onClick={showDetails}
					/>
				</div>
				<div className="card__star-container">
					<p className="card__text">{name}</p>
				</div>

				<p className="card__text">Price: {price}</p>
				<p className="card__text">articule: {articule}</p>

				{btnDetail && (
					<Button
						data-testid="cardButton"
						type="button"
						title="Details"
						onClick={showDetails}
					/>
				)}

				{!btnDel && (
					<Button
						data-testid="cardButton"
						title="Add to basket"
						onClick={() => setOpenedBas(!openedBas)}
					/>
				)}

				{btnDel && (
					<>
						<Button
							data-testid="cardButton"
							title="Delete card"
							onClick={() => setDelBas(!delBas)}
						/>
					</>
				)}
				{openedBas && (
					<Modal
						title={'Add to Basket?'}
						action={
							<>
								<Button title={'Ok'} onClick={() => addedCard()} />
								<Button title={'X'} onClick={() => setOpenedBas(!openedBas)} />
							</>
						}
						bas={openedBas}
						setBas={setOpenedBas}
					/>
				)}
				{delBas && (
					<Modal
						title={'Delete from Basket?'}
						action={
							<>
								<Button title={'Ok'} onClick={() => deletedCard()} />
								<Button title={'X'} onClick={() => setDelBas(!delBas)} />
							</>
						}
						bas={delBas}
						setBas={setDelBas}
					/>
				)}
			</div>
		</article>
	);
};

Card.propTypes = {
	card: PropTypes.shape({
		id: PropTypes.string,
		name: PropTypes.string.isRequired,
	}).isRequired,
	id: PropTypes.string,
	name: PropTypes.string.isRequired,
	price: PropTypes.string.isRequired,
	picture: PropTypes.string,
	articule: PropTypes.string.isRequired,
	color: PropTypes.string,
	btnDel: PropTypes.bool.isRequired,
	url: PropTypes.string,
};

export default Card;
