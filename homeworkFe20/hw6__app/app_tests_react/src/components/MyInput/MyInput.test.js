import { render } from '@testing-library/react';
import MyInput from './MyInput';

const testField = {
	name: 'test',
};

const testForm = {
	errors: 'error',
	touched: 'touch',
};

describe('Testing MyInput', () => {
	test('Smoke test MyInput', () => {
		render(<MyInput field={testField} form={testForm} />);
	});
});
