import React from 'react';
import PropTypes from 'prop-types';

const MyInput = ({ field, form, ...rest }) => {
	return (
		<div>
			<input {...field} {...rest} />
			{form.errors[field.name] && form.touched[field.name] && (
				<span className="error">{form.errors[field.name]}</span>
			)}
		</div>
	);
};

export default MyInput;
