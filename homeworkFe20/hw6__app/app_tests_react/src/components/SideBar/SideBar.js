import React from 'react';
import { NavLink } from 'react-router-dom';
import './SideBar.scss';

function SideBar(props) {
	return (
		<aside>
			<ul data-testid="navbar" className="sidebar">
				<li className="sidebar__item">
					<NavLink
						data-testid="home-link"
						exact
						to="/start"
						className="link"
						activeClassName="link__active"
					>
						Start
					</NavLink>
				</li>
				<li className="sidebar__item">
					<NavLink
						data-testid="favorite-link"
						exact
						to="/favorite"
						className="link"
						activeClassName="link__active"
					>
						Favorite
					</NavLink>
				</li>
				<li className="sidebar__item">
					<NavLink
						data-testid="basket-link"
						exact
						to="/basket"
						className="link"
						activeClassName="link__active"
					>
						Basket
					</NavLink>
				</li>
			</ul>
		</aside>
	);
}

export default SideBar;
