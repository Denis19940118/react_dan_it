// import { render } from '@testing-library/react';
// import { Router } from 'react-router';
// import SideBar from './SideBar';
import React from 'react';
import { Router } from 'react-router-dom';
import { render, fireEvent } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import SideBar from './SideBar';

const renderWithRouter = component => {
	const history = createMemoryHistory();
	return {
		...render(<Router history={history}>{component}</Router>),
	};
};

describe('Testing SideBar', () => {
	test('should render the home page', () => {
		const { container, getByTestId } = renderWithRouter(<SideBar />);
		const navbar = getByTestId('navbar');
		const link = getByTestId('home-link');

		expect(container.innerHTML).toMatch('Start');
		expect(navbar).toContainElement(link);
	});
	test('should navigate to the favorite page', () => {
		const { container, getByTestId } = renderWithRouter(<SideBar />);

		fireEvent.click(getByTestId('favorite-link'));

		expect(container.innerHTML).toMatch('Favorite');
	});

	test('should navigate to the basket page with the params', () => {
		const { container, getByTestId } = renderWithRouter(<SideBar />);

		fireEvent.click(getByTestId('basket-link'));

		expect(container.innerHTML).toMatch('Basket');
	});
});
