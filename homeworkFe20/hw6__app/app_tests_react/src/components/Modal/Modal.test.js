import { fireEvent, render } from '@testing-library/react';
import Modal from './Modal';

describe('Testing Modal', () => {
	test('Smoke test Modal', () => {
		render(
			<Modal title="test Modal" action={'test'} bas={true} setBas={() => {}} />
		);
	});

	test('Clicking test close X', () => {
		const setModalMock = jest.fn();
		const { getByTestId } = render(<Modal setBas={setModalMock} />);
		const modalButton = getByTestId('modalButton');

		expect(modalButton).toBeInTheDocument();
		expect(setModalMock).not.toHaveBeenCalled();
		fireEvent.click(modalButton);
		expect(setModalMock).toHaveBeenCalledTimes(1);
	});
});
