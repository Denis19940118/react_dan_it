import React from 'react';
import './Head.scss';

const Head = props => {
	const { className, title } = props;

	return <nav className={className}>{title}</nav>;
};
export default Head;
