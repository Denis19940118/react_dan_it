import { cleanup, render } from '@testing-library/react';
import ContainerCard from './ContainerCard';

jest.mock('../Card/Card', () => props => (
	<div key={props.c.id} data-testid="test-card">
		{props.c}
	</div>
));

afterEach(cleanup);

const testCards = [
	{
		id: '1',
		name: 'Test1',
		price: '100',
		picture: './picture/brit-adult-salmon.jpg',
		articule: '123425678',
		color: 'yellow',
		favorite: true,
	},
	{
		id: '2',
		name: 'Test2',
		price: '180',
		picture: './picture/brit-premium-adult-cat-chicken.jpg',
		articule: '098765345',
		color: 'yellow',
	},
	{
		id: '3',
		name: 'Test3',
		price: '140',
		picture: './picture/brit-premium-cat-sterilized.jpg',
		articule: '098766789',
		color: 'yellow',
	},
	{
		id: '4',
		name: 'Test4',
		price: '140',
		picture: './picture/friskies_darling-salmon.jpg',
		articule: '123451234',
		color: 'yellow',
	},
];

describe(' Testing ContainerCard', () => {
	test('Smoke test ContainerCard', () => {
		render(
			<ContainerCard
				cards={testCards}
				openedFav={false}
				btnDel={true}
				url={'test'}
				cardID={1}
				title={'Test'}
				btnDetail={true}
				showDetails={true}
				details={() => {}}
			/>
		);
	});

	test('Snapshot test ContainerCard', () => {
		const containerMock = jest.fn();
		const { container } = render(
			<ContainerCard
				cards={testCards}
				openedFav={false}
				btnDel={true}
				url={'test'}
				cardID={1}
				title={'Test'}
				btnDetail={true}
				showDetails={true}
				details={containerMock}
			/>
		);
		expect(container.innerHTML).toMatchSnapshot();
	});
	test('ContainerCard page ', () => {
		const containerMock = jest.fn();
		const { getByTestId } = render(
			<ContainerCard
				cards={testCards}
				openedFav={false}
				btnDel={true}
				url={'test'}
				cardID={1}
				title={'Test'}
				btnDetail={true}
				showDetails={true}
				details={containerMock}
			/>
		);

		const cardsTest = getByTestId('test-card');
		expect(cardsTest.length).toBe(3);
	});
});
