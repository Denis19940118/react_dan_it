import React from 'react';
import PropTypes from 'prop-types';
import '../Button/Button.scss';

const Button = props => {
	const {
		dataTestid,
		className,
		title,
		backgroundColor,
		onClick,
		width,
		fontSize,
		type,
	} = props;

	const style = {
		backgroundColor: backgroundColor,
		width: width,
		fontSize: fontSize,
	};

	return (
		<>
			<button
				data-testid={dataTestid}
				type={type}
				className={className}
				onClick={onClick}
				style={style}
			>
				{title}
			</button>
		</>
	);
};

Button.propTypes = {
	title: PropTypes.string,
	backgroundColor: PropTypes.string,
	onClick: PropTypes.func,
};

Button.defaultProps = {
	title: 'Click me',
	fontSize: 14,
	className: 'button__default button__default:hover',
};

export default Button;
