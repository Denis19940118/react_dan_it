import React, { useEffect, useState } from 'react';
import Button from '../Button/Button';
import PropTypes from 'prop-types';
import { connect, useDispatch, useSelector } from 'react-redux';
import {
	saveDataProductsUser,
	saveDataUser,
} from '../../redux/userOrder/actions';
import { useHistory } from 'react-router';
import { selectCards } from '../../redux/profile/selectors';
import '../FormProduct/FormProduct.scss';
import { Formik, Form, withFormik } from 'formik';
import MyInput from './MyInput/MyInput';
// import {
// 	selectDataUser,
// 	selectProductsUser,
// } from '../../redux/userOrder/selectors';
import schema from './schema/schema';

// const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const FormProduct = props => {
	const { confirm } = props;
	console.log(' props', props);
	const cards = useSelector(selectCards);

	console.log('cards', cards);

	const dispatch = useDispatch();
	const [basket, setBasket] = useState([]);

	const history = useHistory();

	useEffect(() => {
		setBasket(JSON.parse(localStorage.getItem('basket')));
	}, []);

	const toBasket = cards.filter(el => basket.some(b => el.id === b));

	const buy = () => {
		localStorage.removeItem('basket');
		localStorage.setItem('basket', JSON.stringify([]));
		// console.log('values', values);
		dispatch(saveDataUser(props.values));
		dispatch(saveDataProductsUser(toBasket));
		// console.log('toBasket', toBasket);
		// console.log('products', products);

		history.push(`${confirm}`);
	};

	return (
		<form className="form__client" onSubmit={props.handleSubmit} noValidate>
			<h3>Form buy in shop</h3>
			<div>
				<MyInput name="firstName" type="text" placeholder="Your first name" />
			</div>
			<div>
				<MyInput name="secondName" type="text" placeholder="Your second name" />
			</div>
			<div>
				<MyInput name="age" type="number" placeholder="Your age" />
			</div>
			<div>
				<MyInput name="town" type="text" placeholder="Your town" />
			</div>
			<div>
				<MyInput name="street" type="text" placeholder="Your street" />
			</div>
			<div>
				<MyInput name="house" type="text" placeholder="Your house number" />
			</div>
			<div>
				<MyInput
					name="entrance"
					type="text"
					placeholder="Your entrance number"
				/>
			</div>
			<div>
				<MyInput
					name="apartment"
					type="text"
					placeholder="Your apartment number"
				/>
			</div>
			<div>
				<MyInput name="phone" type="phone" placeholder="Your phone" />
			</div>
			<div>
				<MyInput name="mail" type="mail" placeholder="Your mail" />
			</div>
			<Button
				type="submit"
				title="Buy"
				disabled={props.isSubmitting}
				onClick={buy}
			/>
		</form>
	);
};

// const mapDispatchToProps = dispatch => ({
// 	dataUser: dataUser => dispatch(saveDataUser(dataUser)),
// 	products: products => dispatch(saveDataProductsUser(products)),
// });

// const mapStateToProps = currentStore => ({
// 	cards: selectCards(currentStore),
// });

FormProduct.propTypes = {
	cards: PropTypes.array.isRequired,
	confirm: PropTypes.string,
};

const buyProduct = (values, { setSubmitting }) => {
	console.log(values);
	setSubmitting(false);
};
export default //  connect(
// 	// () => ({}),
// 	mapStateToProps,
// 	mapDispatchToProps
// );
withFormik({
	mapPropsToValues: props => ({
		firstName: '',
		secondName: '',
		age: '',
		age: '',
		town: '',
		street: '',
		house: '',
		entrance: '',
		apartment: '',
		phone: '',
		mail: 'admin@test.com',
	}),
	handleSubmit: buyProduct,
	validationSchema: schema,
})(FormProduct);
