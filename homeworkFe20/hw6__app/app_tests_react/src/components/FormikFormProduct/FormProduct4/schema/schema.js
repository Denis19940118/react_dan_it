import * as yup from 'yup';

const FIELD_REQUIRED = 'This field is required';

const schema = yup.object().shape({
	mail: yup
		.string()
		.required(FIELD_REQUIRED)
		.email('This is not a valid email'),
	firstName: yup.string().required(FIELD_REQUIRED).min(2, 'Too Short!'),
	secondName: yup.string().required(FIELD_REQUIRED).min(2, 'Too Short!'),
	age: yup.number().required(FIELD_REQUIRED).min(18).max(150),
	town: yup.string().required(FIELD_REQUIRED),
	street: yup.string().required(FIELD_REQUIRED),
	house: yup.string().required(FIELD_REQUIRED),
	entrance: yup.string().required(FIELD_REQUIRED),
	apartment: yup.string().required(FIELD_REQUIRED),
	phone: yup
		.string()
		.required(FIELD_REQUIRED)
		.min(10, 'Too Short!')
		.max(15, 'Too Long!'),
});

export default schema;
