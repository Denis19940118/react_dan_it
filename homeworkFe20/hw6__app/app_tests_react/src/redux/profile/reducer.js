// import { createStore } from 'redux';
import {
	TOGGLE_FAVORITES,
	BUY_PRODUCT,
	DEL_BUY_PRODUCT,
} from '../products/types';
import { SAVE_ALL_CARDS } from './actionType';

const initialStore = {
	user: null,
	cards: [],
	buyProduct: [],
	data: [],
	textProp: 'here we are in redux, finally!',
};

const reducer = (currentStore = initialStore, action) => {
	switch (action.type) {
		case SAVE_ALL_CARDS:
			return {
				...currentStore,
				...action.payload,
			};
		case TOGGLE_FAVORITES: {
			const product = action.payload;
			const index = currentStore.cards.indexOf(product);
			const updateProduct = { ...product, favorite: !product.favorite };
			const newProduct = [...currentStore.cards];
			newProduct.splice(index, 1, updateProduct);

			return {
				...currentStore,
				cards: newProduct,
			};
		}
		case BUY_PRODUCT: {
			// const index = currentStore.cards.indexOf(product);
			const updateId = action.payload;
			const newProduct = [...currentStore.buyProduct];
			newProduct.push(updateId);

			return {
				...currentStore,
				buyProduct: newProduct,
			};
		}
		case DEL_BUY_PRODUCT: {
			const updateState = action.payload;
			const newProduct = [...currentStore.buyProduct];
			const curStateProd = newProduct.filter(item => item !== updateState);
			console.log(
				'🚀 ~ file: reducer.js ~ line 51 ~ reducer ~ curStateProd',
				curStateProd
			);
			return {
				...currentStore,
				buyProduct: curStateProd,
			};
		}
		// case SAVE_DATA_PRODUCT_USER:
		// 	return { ...currentStore, ...action.payload };
		// case SAVE_DATA_USER:
		// 	return {
		// 		...currentStore,
		// 		...action.payload,
		// 		// products: {...currentStore.products, data:action.payload }
		// 	};

		default:
			return currentStore;
	}
};

export default reducer;
