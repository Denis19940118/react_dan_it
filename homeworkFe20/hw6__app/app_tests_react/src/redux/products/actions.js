import { TOGGLE_FAVORITES, BUY_PRODUCT, DEL_BUY_PRODUCT } from './types';

export const toggleFavoritesAction = product => ({
	type: TOGGLE_FAVORITES,
	payload: product,
});

export const buyProductAction = product => ({
	type: BUY_PRODUCT,
	payload: product,
});

export const delBuyProductAction = product => ({
	type: DEL_BUY_PRODUCT,
	payload: product,
});
