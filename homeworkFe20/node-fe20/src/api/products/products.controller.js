import { createProduct } from '../../services/product.service';
import productMapper from './product.mapper';

/**
 * @desc Повертає список товарів
 **/
export const ProductListController = (request, response) => {
	response.status(200).json({
		success: true,
		product,
	});
	console.log('REQUEST', request.body);
};

export const CreateProductController = async (request, response) => {
	const { title, price, description } = request.body;

	const product = await createProduct({
		title,
		price,
		description,
	});

	return response.status(201).json({
		status: true,
		product: productMapper(product),
	});
};
