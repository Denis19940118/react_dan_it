/**
 *
 **/
export default product => ({
	id: product._id,
	title: product.title,
	price: parseFloat(product.price),
	description: product.description,
	status: product.status,
	created: product.created_at,
	updated: product.updated_at,
});
