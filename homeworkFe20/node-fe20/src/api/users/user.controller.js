import { createUser } from '../../services/user.service';
import UserMapper from './user.mapper';
/**
 * @desc
 **/

const user = [
	{
		id: 1,
		title: 'The very first article I created',
	},

	{
		id: 2,
		title: 'The very second article I created',
	},
];
export const CreateUserController = async (request, response) => {
	console.log('REQUEST', request.body);
	const {
		first_name,
		last_name,
		password,
		email,
		salt,
		city,
		gender,
	} = request.body;

	const { user } = await createUser({
		first_name,
		last_name,
		password,
		email,
		salt,
		city,
		gender,
	});

	response.status(201).json({
		status: true,
		// user,
		user: UserMapper(user),
	});
};
