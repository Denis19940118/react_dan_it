import { CreateUserController } from './user.controller';
import { Router } from 'express';

const route = Router();

export default function (root) {
	root.use('/users', route);

	route.get('/', CreateUserController);
	/**
	 * @desc Create user
	 **/

	route.post('/register', CreateUserController);
}

// "last_name": "Stryzhalov",
// "city": "Kiev",
// "email": "denis.stryzhalov@mail.com",
// "gender": "male"
