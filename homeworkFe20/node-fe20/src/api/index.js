import {Router} from 'express';
import HomeRoute from './home';
import ProductRoute from './products';
import UserRoute from './users';

export default () => {
	const router = Router();

	HomeRoute(router);
	ProductRoute(router);
	UserRoute(router);

	return router;
}