import { randomBytes } from 'crypto';
import argon2 from 'argon2';
import UserModel from '../models/user.model';

/**
 * @desc Creates user
 **/
export const createUser = async ({
	first_name,
	last_name,
	password: pswdIn,
	email,
	city,
	gender,
}) => {
	const salt = randomBytes(32);
	const password = await argon2.hash(pswdIn, { salt });

	const user = await UserModel.create({
		first_name,
		last_name,
		password,
		email,
		salt,
		city,
		gender,
	});

	return {
		user,
		// token: this.generateToken(user)
	};
};
