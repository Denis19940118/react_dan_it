import productModel from '../models/product.model';
/**
 * @desc Creates product
 **/
export const createProduct = async ({ title, price, description }) => {
	return await productModel.create({
		title,
		price,
		description,
	});
};
