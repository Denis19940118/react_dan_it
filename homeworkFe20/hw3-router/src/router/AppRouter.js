import React, { useEffect, useState } from 'react';
import Head from '../components/Head/Head';
import SideBar from '../components/SideBar/SideBar';
import { Redirect, Route, Switch } from 'react-router-dom';
import Start from '../pages/Start/Start';
import Favorites from '../pages/Favorite/Favorites';
import Basket from '../pages/Basket/Basket';

const AppRouter = props => {
	const [cards, setCards] = useState([]);
	const [isFavorite] = useState(
		JSON.parse(localStorage.getItem('favorite')) ||
			localStorage.setItem('favorite', JSON.stringify([]))
	);
	const [isBasket] = useState(
		JSON.parse(localStorage.getItem('basket')) ||
			localStorage.setItem('basket', JSON.stringify([]))
	);

	useEffect(() => {
		fetch('./products.json')
			.then(res => res.json())
			.then(res => setCards(res));
	}, []);

	return (
		<section>
			<Head className="header" title={<SideBar />} />
			<Redirect exact from="/" to="/start" />
			<Switch>
				<Route exact path="/start">
					<Start cards={cards} />
				</Route>
				<Route exact path="/favorite">
					<Favorites cards={cards} />
				</Route>
				<Route exact path="/basket">
					<Basket cards={cards} />
				</Route>
			</Switch>
		</section>
	);
};

export default AppRouter;
