import React, { useEffect, useState } from 'react';
import Button from '../Button/Button';
import './Card.scss';
import PropTypes from 'prop-types';
import Icon from '../Icon/Icon';
import Modal from '../Modal/Modal';

const Card = props => {
	const [opened, setOpened] = useState(false);
	const [openedBas, setOpenedBas] = useState(false);
	const [delBas, setDelBas] = useState(false);

	const {
		id,
		name,
		price,
		picture,
		articule,
		color,
		openedFav,
		btnDel,
	} = props;

	const favorite = JSON.parse(localStorage.getItem('favorite'));

	useEffect(() => {
		favorite.filter(el => (el === id ? setOpened(!opened) : ''));
	}, [id]);

	// useEffect(() => {
	// 	setOpened(openedFav || false);
	// }, [openedFav]);

	const handleClickToFavorite = op => {
		if (op === true) {
			const favorite = JSON.parse(localStorage.getItem('favorite'));
			localStorage.removeItem('favorite');

			favorite.push(id);
			localStorage.setItem('favorite', JSON.stringify(favorite));
		} else {
			const i = JSON.parse(localStorage.getItem('favorite'));
			localStorage.removeItem('favorite');

			const fav = [];
			i.map(el => (el !== id ? fav.push(el) : ''));
			localStorage.setItem('favorite', JSON.stringify(fav));
		}
	};

	const clickToBasket = () => {
		const basket = JSON.parse(localStorage.getItem('basket'));
		localStorage.removeItem('basket');
		basket.push(id);
		localStorage.setItem('basket', JSON.stringify(basket));
	};

	const clickDelCardBasket = () => {
		const i = JSON.parse(localStorage.getItem('basket'));
		localStorage.removeItem('basket');

		const bas = [];
		i.map(el => (el !== id ? bas.push(el) : ''));
		localStorage.setItem('basket', JSON.stringify(bas));
	};

	const addedCard = () => {
		clickToBasket();
		setOpenedBas(!openedBas);
	};

	const deletedCard = () => {
		clickDelCardBasket();
		setDelBas(!delBas);
	};

	const changeColor = () => {
		setOpened(openedFav || !opened);
		handleClickToFavorite(!opened);
	};

	return (
		<div className="card card__container">
			<img className="card__img" src={picture} alt={name} />
			<p className="card__text">
				{name}
				<Icon
					type={'star'}
					color={color}
					filled={opened}
					onClick={() => changeColor()}
				/>
			</p>

			<p className="card__text">Price: {price}</p>
			<p className="card__text">articule: {articule}</p>
			{!btnDel && (
				<Button
					title="Add to basket"
					onClick={() => setOpenedBas(!openedBas)}
				/>
			)}
			{openedBas && (
				<Modal
					title={'Add to Basket?'}
					action={
						<>
							<Button
								className="btn-modal"
								title={'Ok'}
								onClick={() => addedCard()}
							/>
							<Button
								className="btn-modal"
								title={'X'}
								onClick={() => setOpenedBas(!openedBas)}
							/>
						</>
					}
					bas={openedBas}
					setBas={setOpenedBas}
				/>
			)}

			{btnDel && (
				<Button title="Delete card" onClick={() => setDelBas(!delBas)} />
			)}
			{delBas && (
				<Modal
					title={'Delete from Basket?'}
					action={
						<>
							<Button
								className="btn-modal"
								title={'Ok'}
								onClick={() => deletedCard()}
							/>
							<Button
								className="btn-modal"
								title={'X'}
								onClick={() => setDelBas(!delBas)}
							/>
						</>
					}
					bas={delBas}
					setBas={setDelBas}
				/>
			)}
		</div>
	);
};

Card.propTypes = {
	id: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	price: PropTypes.string.isRequired,
	picture: PropTypes.string,
	articule: PropTypes.string.isRequired,
	color: PropTypes.string,
	btnDel: PropTypes.bool.isRequired,
};

export default Card;
