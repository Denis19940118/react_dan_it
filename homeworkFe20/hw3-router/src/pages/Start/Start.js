import React from 'react';
import PropTypes from 'prop-types';
import ContainerCard from '../../components/ContainerCard/ContainerCard';

const Start = props => {
	const { cards } = props;

	return (
		<div>
			<ContainerCard cards={cards} btnDel={false} />
		</div>
	);
};

Start.propTypes = {
	cards: PropTypes.array.isRequired,
};

export default Start;
