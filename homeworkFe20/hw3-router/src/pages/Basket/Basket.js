import React from 'react';
import PropTypes from 'prop-types';
import ContainerCard from '../../components/ContainerCard/ContainerCard';

const Basket = props => {
	const { cards } = props;

	const basket = JSON.parse(localStorage.getItem('basket'));

	const toBasket = cards.filter(el => basket.some(b => el.id === b));

	return (
		<div>
			<ContainerCard cards={toBasket} btnDel={true} />
		</div>
	);
};

Basket.propTypes = {
	cards: PropTypes.array.isRequired,
};

export default Basket;
