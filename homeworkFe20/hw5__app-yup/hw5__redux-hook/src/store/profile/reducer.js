// import { createStore } from 'redux';
// import { TOGGLE_FAVORITES } from '../products/types';
import {
	SAVE_ALL_CARDS,
	SAVE_DATA_PRODUCT_USER,
	SAVE_DATA_USER,
} from './type';

const initialStore = {
	user: null,
	cards: [],
	products: null,
	dataUser: null,
	data: [],
	textProp: 'here we are in redux, finally!',
};

const reducer = (currentStore = initialStore, action) => {
	switch (action.type) {
		case SAVE_ALL_CARDS:
			return {
				...currentStore,
				...action.payload,
			};
		case SAVE_DATA_PRODUCT_USER:
			return { ...currentStore, ...action.payload };
		case SAVE_DATA_USER:
			return {
				...currentStore,
				...action.payload,
				// products: {...currentStore.products, data:action.payload }
			};
		// case TOGGLE_FAVORITES: {
		// 	const product = action.payload;
		// 	const index = currentStore.data.indexOf(product);
		// 	const updateProduct = { ...product, favorite: !product.favorite };
		// 	const newProduct = currentStore.data;
		// 	newProduct.splice(index, 1, updateProduct);

			return {
				...currentStore,
				data: newProduct,
			};
		}

		default:
			return currentStore;
	}
};

export default reducer;
