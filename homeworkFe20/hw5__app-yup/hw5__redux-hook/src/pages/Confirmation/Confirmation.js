import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router';
import Button from '../../components/Button/Button';
import { connect, useSelector } from 'react-redux';
import {
	selectDataUser,
	selectProductsUser,
} from '../../redux/userOrder/selectors';
import '../Confirmation/Confirmation.scss';
import Loader from '../../components/Loader/Loader';

const Confirmation = props => {
	const { start, dataUser, products } = props;
	// const dataUser = useSelector(selectDataUser);
	console.log('dataUser', dataUser);
	// const products = useSelector(selectProductsUser);
	console.log(' products', products);

	const arrList = [];
	const history = useHistory();
	const [itemData] = useState(dataUser);
	console.log('itemData', itemData);
	const [loading, setLoading] = useState(true);
	const [detailsUser, setDetailsUser] = useState(false);

	const goToBack = () => {
		history.push(`${start}`);
	};

	for (let prop in itemData) {
		let val = itemData[prop];
		arrList.push(val);
	}
	console.log('arrList', arrList);
	const confirmDataUser = arrList.map(el =>
		el === arrList[0] || el === arrList[1] ? <p>{el}</p> : ''
	);

	const confirmDetailsDataUser = arrList.map(el =>
		el === arrList[0] || el === arrList[1] ? '' : <p>{el}</p>
	);

	const confirmDataProducts = products.map(el => (
		<p>
			<img className="confirm__img" src={`.${el.picture}`} />

			{el.name}
		</p>
	));

	const detailsUserData = () => {
		setDetailsUser(!detailsUser);
	};

	const isLoad = () => {
		setTimeout(() => {
			setLoading(false);
		}, 2000);
	};

	if (loading) {
		isLoad();
		return (
			<>
				<Loader />
			</>
		);
	} else {
		return (
			<>
				<div className="confirm">
					<h3>Operation successful</h3>
					<p className="confirm__package">
						<p>
							Your full name:
							{confirmDataUser}
						</p>
						{!detailsUser && (
							<Button
								title="Details data user"
								onClick={() => detailsUserData()}
							/>
						)}
						{detailsUser && (
							<Button
								title="Hide details data user"
								onClick={() => detailsUserData()}
							/>
						)}
						{detailsUser && <p> {confirmDetailsDataUser}</p>}
						{confirmDataProducts}
						<Button title="Go to general title" onClick={() => goToBack()} />
					</p>
				</div>
			</>
		);
	}
};

const mapStateToProps = State => ({
	dataUser: selectDataUser(State),
	products: selectProductsUser(State),
});

Confirmation.propTypes = {
	start: PropTypes.string,
	dataUser: PropTypes.object,
};

export default connect(mapStateToProps)(Confirmation);
