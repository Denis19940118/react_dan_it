import React from 'react';
import PropTypes from 'prop-types';
import Card from '../Card/Card';
import './ContainerCard.scss';

const ContainerCard = props => {
	const { cards, openedFav, btnDel, url, cardD, title } = props;

	const card = cards.map(c => (
		<Card
			key={c.id}
			{...c}
			card={c}
			openedFav={openedFav}
			btnDel={btnDel}
			url={url}
			cardD={cardD}
		/>
	));

	return (
		<div className="container__body">
			<h2 className={'container__body-heading'}>{title}</h2>
			<div className="container__body-card">{card}</div>
		</div>
	);
};

ContainerCard.propTypes = {
	cards: PropTypes.array,
	btnDel: PropTypes.bool,
	openedFav: PropTypes.bool,
};

ContainerCard.defaultProps = {
	className: '',
};

export default ContainerCard;
