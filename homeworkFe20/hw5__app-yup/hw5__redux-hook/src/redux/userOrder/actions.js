import { SAVE_DATA_PRODUCT_USER, SAVE_DATA_USER } from './types';

export const saveDataProductsUser = products => ({
	type: SAVE_DATA_PRODUCT_USER,
	payload: { products: products },
});

export const saveDataUser = dataUser => ({
	type: SAVE_DATA_USER,
	payload: { dataUser: dataUser },
});
