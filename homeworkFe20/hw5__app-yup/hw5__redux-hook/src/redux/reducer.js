import { combineReducers } from 'redux';
import profileReducer from './profile/reducer';
import userOrder from './userOrder/reducer';

const reducer = combineReducers({ user: profileReducer, userOrder });
export default reducer;
