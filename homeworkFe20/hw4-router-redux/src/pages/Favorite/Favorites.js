import React from 'react';
import PropTypes from 'prop-types';
import ContainerCard from '../../components/ContainerCard/ContainerCard';
import { connect } from 'react-redux';
import { selectCards } from '../../redux/selectors';

const Favorites = props => {
	const { cards } = props;

	const fav = JSON.parse(localStorage.getItem('favorite'));

	const cardsFav = cards.filter(el => fav.some(f => el.id === f));

	return (
		<div>
			<ContainerCard cards={cardsFav} openedFav={true} btnDel={false} />
		</div>
	);
};

const mapStateToProps = currentStore => ({
	cards: selectCards(currentStore),
});

Favorites.propTypes = {
	cards: PropTypes.array,
};

export default connect(mapStateToProps)(Favorites);
