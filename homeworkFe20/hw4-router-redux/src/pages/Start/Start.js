import React from 'react';
import PropTypes from 'prop-types';
import ContainerCard from '../../components/ContainerCard/ContainerCard';
import { connect } from 'react-redux';
import { selectCards } from '../../redux/selectors';

const Start = props => {
	const { cards } = props;
	console.log('Startcards', cards);

	return (
		<div>
			<ContainerCard cards={cards} btnDel={false} />
		</div>
	);
};

const mapStateToProps = currentStore => ({
	cards: selectCards(currentStore),
});

Start.propTypes = {
	cards: PropTypes.array,
};

export default connect(mapStateToProps)(Start);
