import React from 'react';
import PropTypes from 'prop-types';
import ContainerCard from '../../components/ContainerCard/ContainerCard';
import { connect } from 'react-redux';
import { selectCards } from '../../redux/selectors';

const Basket = props => {
	const { cards } = props;

	const basket = JSON.parse(localStorage.getItem('basket'));

	const toBasket = cards.filter(el => basket.some(b => el.id === b));

	return (
		<div>
			<ContainerCard cards={toBasket} btnDel={true} />
		</div>
	);
};

const mapStateToProps = currentStore => ({
	cards: selectCards(currentStore),
});

Basket.propTypes = {
	cards: PropTypes.array,
};

export default connect(mapStateToProps)(Basket);
