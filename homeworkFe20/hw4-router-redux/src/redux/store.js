// import reducer from './reducer';
// import thunk from 'redux-thunk';
// import { createStore, applyMiddleware } from 'redux';
// import { composeWithDevtools } from 'redux-devtools-extension';

// export default createStore(
// 	reducer,
// 	composeWithDevtools(applyMiddleware(thunk))
// );
import reducer from './reducer';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

export default createStore(
	reducer,
	// window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
	composeWithDevTools(applyMiddleware(thunk))
);
