import { SAVE_ALL_CARDS } from '../actionType';

export const saveAllCardsAction = cards => ({
	type: SAVE_ALL_CARDS,
	payload: { cards: cards },
});
