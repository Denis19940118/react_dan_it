import { createStore } from 'redux';
import { SAVE_ALL_CARDS } from './actionType';

const initialStore = {
	user: null,
	cards: [],
	textProp: 'here we are in redux, finally!',
};

const reducer = (currentStore = initialStore, action) => {
	switch (action.type) {
		case SAVE_ALL_CARDS:
			return {
				...createStore,
				...action.payload,
			};
		default:
			return currentStore;
	}
};

export default reducer;
