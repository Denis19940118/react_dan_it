import React, { useEffect, useState } from 'react';
import Head from '../components/Head/Head';
import SideBar from '../components/SideBar/SideBar';
import { Redirect, Route, Switch } from 'react-router-dom';
import Start from '../pages/Start/Start';
import Favorites from '../pages/Favorite/Favorites';
import Basket from '../pages/Basket/Basket';
import { connect } from 'react-redux';
import { saveAllCardsAction } from '../redux/action';
import Card from '../components/Card/Card';
import CardDetails from '../components/CardDetails/CardDetails';

const AppRouter = ({ saveAllCards, ...props }) => {
	const [cards, setCards] = useState([]);
	const [isFavorite] = useState(
		JSON.parse(localStorage.getItem('favorite')) ||
			localStorage.setItem('favorite', JSON.stringify([]))
	);
	const [isBasket] = useState(
		JSON.parse(localStorage.getItem('basket')) ||
			localStorage.setItem('basket', JSON.stringify([]))
	);

	useEffect(() => {
		fetch('./products.json')
			.then(res => res.json())
			.then(res => {
				saveAllCards(res);
				console.log('saveAllCards(res.data)', res);
			});
	}, []);

	return (
		<section>
			<Head className="header" title={<SideBar />} />
			<Redirect exact from="/" to="/start" />
			<Switch>
				<Route exact path="/start">
					<Start />
				</Route>
				<Route exact path="/start/:card">
					<CardDetails />
				</Route>
				<Route exact path="/favorite">
					<Favorites />
				</Route>
				<Route exact path="/favorite/:card">
					<CardDetails />
				</Route>
				<Route exact path="/basket">
					<Basket />
				</Route>
				<Route exact path="/basket/:card">
					<CardDetails />
				</Route>
			</Switch>
		</section>
	);
};

const mapDispatchToProps = dispatch => ({
	saveAllCards: cards => dispatch(saveAllCardsAction(cards)),
});

export default connect(() => ({}), mapDispatchToProps)(AppRouter);
