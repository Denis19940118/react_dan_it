import React from 'react';
import { useHistory, useParams } from 'react-router-dom';

import { connect } from 'react-redux';
import Icon from '../Icon/Icon';
import { selectCards } from '../../redux/selectors';
import './CardDetails.scss';

const CardDetails = props => {
	const { cards } = props;
	const { cardId } = useParams();
	const history = useHistory();

	const basketCard = JSON.parse(localStorage.getItem('basketCard'));
	const basketPicture = JSON.parse(localStorage.getItem('basketPicture'));

	const toBasket = [];
	cards.filter(el => (el.id === basketCard ? toBasket.push(el) : ''));
	const { name, price, picture, articule, color } = toBasket[0];

	const goToCard = () => {
		history.push('/basket');
		localStorage.removeItem('basketCard');
	};

	return (
		<>
			<div className="card__details card__container">
				<img className="card__img" src={picture} alt={name} />
				<p className="card__text">
					{name}
					<Icon
						type={'star'}
						color={color}
						// filled={opened}
						// onClick={() => changeColor()}
					/>
				</p>

				<p className="card__text">Price: {price}</p>
				<p className="card__text">articule: {articule}</p>
				<div>
					<button onClick={goToCard}>Go back</button>
				</div>
			</div>
		</>
	);
};

const mapStateToProps = currentStore => ({
	cards: selectCards(currentStore),
});

export default connect(mapStateToProps)(CardDetails);
