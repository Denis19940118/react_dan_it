import React from 'react';
import PropTypes from 'prop-types';
import Card from '../Card/Card';
import './ContainerCard.scss';

const ContainerCard = props => {
	const { cards, openedFav, btnDel } = props;

	const card = cards.map(c => (
		<Card key={c.id} {...c} openedFav={openedFav} btnDel={btnDel} />
	));
	return <div className="container__body">{card}</div>;
};

ContainerCard.propTypes = {
	cards: PropTypes.array,
	btnDel: PropTypes.bool,
	openedFav: PropTypes.bool,
};

export default ContainerCard;
