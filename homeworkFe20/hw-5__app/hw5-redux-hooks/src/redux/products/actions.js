import { TOGGLE_FAVORITES } from './types';

export const toggleFavoritesAction = product => ({
	type: TOGGLE_FAVORITES,
	payload: product,
});
