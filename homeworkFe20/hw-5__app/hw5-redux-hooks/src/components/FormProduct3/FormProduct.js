import React, { useEffect, useState } from 'react';
import Button from '../Button/Button';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
	saveDataProductsUser,
	saveDataUser,
} from '../../redux/userOrder/actions';
import { useHistory } from 'react-router';
import { selectCards } from '../../redux/profile/selectors';
import '../FormikFormProduct/FormProduct/FormProduct.scss';
import { Formik, Form, Field } from 'formik';
import MyInput from '../MyInput/MyInput';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const validateForm = values => {
	const {
		firstName,
		secondName,
		age,
		town,
		street,
		house,
		entrance,
		apartment,
		phone,
		mail,
	} = values;
	const errors = {};
	if (!EMAIL_REGEX.test(mail)) {
		errors.mail = 'This is not a valid email';
	}
	if (!mail) {
		errors.mail = 'This field is required';
	}
	if (!firstName) {
		errors.firstName = 'This field is required';
	}
	if (!secondName) {
		errors.secondName = 'This field is required';
	}
	if (age === 0 || age < 16 || age > 150) {
		errors.age = 'This field is not true age';
	}
	if (!age) {
		errors.age = 'This field is required';
	}
	if (!town) {
		errors.town = 'This field is required';
	}
	if (!street) {
		errors.street = 'This field is required';
	}
	if (!house) {
		errors.house = 'This field is required';
	}
	if (!entrance) {
		errors.entrance = 'This field is required';
	}
	if (!apartment) {
		errors.apartment = 'This field is required';
	}
	if (!phone) {
		errors.phone = 'This field is required';
	}
	if (phone.length < 10) {
		errors.phone = 'This field is false';
	}

	return errors;
};

const FormProduct = ({ cards, dataUser, confirm, products }) => {
	const [basket, setBasket] = useState([]);

	const history = useHistory();

	useEffect(() => {
		setBasket(JSON.parse(localStorage.getItem('basket')));
	}, []);

	const toBasket = cards.filter(el => basket.some(b => el.id === b));
	console.log('toBasket', toBasket);

	const buyProduct = values => {
		localStorage.removeItem('basket');
		localStorage.setItem('basket', JSON.stringify([]));

		dataUser(values);
		products(toBasket);

		console.log('toBasket', toBasket);
		console.log('products', products);
		console.log(values);

		history.push(`${confirm}`);
	};

	return (
		<Formik
			initialValues={{
				firstName: '',
				secondName: '',
				age: '',
				age: '',
				town: '',
				street: '',
				house: '',
				entrance: '',
				apartment: '',
				phone: '',
				mail: 'admin@test.com',
			}}
			validate={validateForm}
			onSubmit={buyProduct}
		>
			{forminProps => {
				return (
					<Form className="form__client" noValidate>
						<h3>Form buy in shop</h3>
						<div>
							<Field
								component={MyInput}
								name="firstName"
								type="text"
								placeholder="Your first name"
							/>
						</div>
						<div>
							<Field
								component={MyInput}
								name="secondName"
								type="text"
								placeholder="Your second name"
							/>
						</div>
						<div>
							<Field
								component={MyInput}
								name="age"
								type="number"
								placeholder="Your age"
							/>
						</div>
						<div>
							<Field
								component={MyInput}
								name="town"
								type="text"
								placeholder="Your town"
							/>
						</div>
						<div>
							<Field
								component={MyInput}
								name="street"
								type="text"
								placeholder="Your street"
							/>
						</div>
						<div>
							<Field
								component={MyInput}
								name="house"
								type="text"
								placeholder="Your house number"
							/>
						</div>
						<div>
							<Field
								component={MyInput}
								name="entrance"
								type="text"
								placeholder="Your entrance number"
							/>
						</div>
						<div>
							<Field
								component={MyInput}
								name="apartment"
								type="text"
								placeholder="Your apartment number"
							/>
						</div>
						<div>
							<Field
								component={MyInput}
								name="phone"
								type="phone"
								placeholder="Your phone"
							/>
						</div>
						<div>
							<Field
								component={MyInput}
								name="mail"
								type="mail"
								placeholder="Your mail"
							/>
						</div>

						<Button type="submit" title="Buy" />
					</Form>
				);
			}}
		</Formik>
	);
};

const mapDispatchToProps = dispatch => ({
	dataUser: dataUser => dispatch(saveDataUser(dataUser)),
	products: products => dispatch(saveDataProductsUser(products)),
});

const mapStateToProps = currentStore => ({
	cards: selectCards(currentStore),
});

FormProduct.propTypes = {
	cards: PropTypes.array.isRequired,
	confirm: PropTypes.string,
};

export default connect(mapStateToProps, mapDispatchToProps)(FormProduct);
