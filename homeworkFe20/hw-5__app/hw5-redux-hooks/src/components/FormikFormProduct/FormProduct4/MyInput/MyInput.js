import { useField } from 'formik';
import React from 'react';

const MyInput = ({ name, ...rest }) => {
	const [field, meta] = useField(name);
	return (
		<div>
			<input {...field} {...rest} />
			{meta.errors && meta.touched && (
				<span className="error">{meta.errors}</span>
			)}
		</div>
	);
};

export default MyInput;
