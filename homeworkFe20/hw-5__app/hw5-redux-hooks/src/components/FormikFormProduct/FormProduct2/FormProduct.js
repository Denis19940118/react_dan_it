import React, { useEffect, useState } from 'react';
import Button from '../../Button/Button';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
	saveDataProductsUser,
	saveDataUser,
} from '../../../redux/userOrder/actions';
import { useHistory } from 'react-router';
import { selectCards } from '../../../redux/profile/selectors';
import '../FormProduct/FormProduct.scss';
import { Formik, Form, Field } from 'formik';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const validateForm = values => {
	const {
		firstName,
		secondName,
		age,
		town,
		street,
		house,
		entrance,
		apartment,
		phone,
		mail,
	} = values;
	const errors = {};
	if (!EMAIL_REGEX.test(mail)) {
		errors.mail = 'This is not a valid email';
	}
	if (!mail) {
		errors.mail = 'This field is required';
	}
	if (!firstName) {
		errors.firstName = 'This field is required';
	}
	if (!secondName) {
		errors.secondName = 'This field is required';
	}
	if (age === 0 || age < 16 || age > 150) {
		errors.age = 'This field is not true age';
	}
	if (!age) {
		errors.age = 'This field is required';
	}
	if (!town) {
		errors.town = 'This field is required';
	}
	if (!street) {
		errors.street = 'This field is required';
	}
	if (!house) {
		errors.house = 'This field is required';
	}
	if (!entrance) {
		errors.entrance = 'This field is required';
	}
	if (!apartment) {
		errors.apartment = 'This field is required';
	}
	if (!phone) {
		errors.phone = 'This field is required';
	}
	if (phone.length < 10) {
		errors.phone = 'This field is false';
	}

	return errors;
};

const FormProduct = ({ cards, dataUser, confirm, products }) => {
	const [basket, setBasket] = useState([]);

	const history = useHistory();

	useEffect(() => {
		setBasket(JSON.parse(localStorage.getItem('basket')));
	}, []);

	const toBasket = cards.filter(el => basket.some(b => el.id === b));
	console.log('toBasket', toBasket);

	// const register = values => {
	// 	const isValid = validateForm();

	// 	if (isValid !== object) {
	// 		return;
	// 	}

	// 	localStorage.removeItem('basket');
	// 	localStorage.setItem('basket', JSON.stringify([]));
	// 	dataUser(values);
	// 	products(toBasket);

	// 	console.log('toBasket', toBasket);
	// 	console.log('products', products);
	// 	console.log(values);

	// 	history.push(`${confirm}`);
	// };

	const buyProduct = values => {
		localStorage.removeItem('basket');
		localStorage.setItem('basket', JSON.stringify([]));

		dataUser(values);
		products(toBasket);

		console.log('toBasket', toBasket);
		console.log('products', products);
		console.log(values);

		history.push(`${confirm}`);
	};

	return (
		<Formik
			initialValues={{
				firstName: '',
				secondName: '',
				age: '',
				age: '',
				town: '',
				street: '',
				house: '',
				entrance: '',
				apartment: '',
				phone: '',
				mail: 'admin@test.com',
			}}
			validate={validateForm}
			onSubmit={buyProduct}
			// onSubmit={register}
		>
			{forminProps => {
				return (
					<Form className="form__client" noValidate>
						<h3>Form buy in shop</h3>
						<div>
							<Field
								component="input"
								name="firstName"
								type="text"
								placeholder="Your first name"
							/>
							{forminProps.errors.firstName &&
								forminProps.touched.firstName && (
									<span className="error">{forminProps.errors.firstName}</span>
								)}
						</div>
						<div>
							<Field
								component="input"
								name="secondName"
								type="text"
								placeholder="Your second name"
							/>
							{forminProps.errors.secondName &&
								forminProps.touched.secondName && (
									<span className="error">{forminProps.errors.secondName}</span>
								)}
						</div>
						<div>
							<Field
								component="input"
								name="age"
								type="number"
								placeholder="Your age"
							/>
							{forminProps.errors.age && forminProps.touched.age && (
								<span className="error">{forminProps.errors.age}</span>
							)}
						</div>
						<div>
							<Field
								component="input"
								name="town"
								type="text"
								placeholder="Your town"
							/>
							{forminProps.errors.town && forminProps.touched.town && (
								<span className="error">{forminProps.errors.town}</span>
							)}
						</div>
						<div>
							<Field
								component="input"
								name="street"
								type="text"
								placeholder="Your street"
							/>
							{forminProps.errors.street && forminProps.touched.street && (
								<span className="error">{forminProps.errors.street}</span>
							)}
						</div>
						<div>
							<Field
								component="input"
								name="house"
								type="text"
								placeholder="Your house number"
							/>
							{forminProps.errors.house && forminProps.touched.house && (
								<span className="error">{forminProps.errors.house}</span>
							)}
						</div>
						<div>
							<Field
								component="input"
								name="entrance"
								type="text"
								placeholder="Your entrance number"
							/>
							{forminProps.errors.entrance && forminProps.touched.entrance && (
								<span className="error">{forminProps.errors.entrance}</span>
							)}
						</div>
						<div>
							<Field
								component="input"
								name="apartment"
								type="text"
								placeholder="Your apartment number"
							/>
							{forminProps.errors.apartment &&
								forminProps.touched.apartment && (
									<span className="error">{forminProps.errors.apartment}</span>
								)}
						</div>
						<div>
							<Field
								component="input"
								name="phone"
								type="phone"
								placeholder="Your phone"
							/>
							{forminProps.errors.phone && forminProps.touched.phone && (
								<span className="error">{forminProps.errors.phone}</span>
							)}
						</div>
						<div>
							<Field
								component="input"
								name="mail"
								type="mail"
								placeholder="Your mail"
							/>
							{forminProps.errors.mail && forminProps.touched.mail && (
								<span className="error">{forminProps.errors.mail}</span>
							)}
						</div>

						<Button type="submit" title="Buy" />
					</Form>
				);
			}}
		</Formik>
	);
};

const mapDispatchToProps = dispatch => ({
	dataUser: dataUser => dispatch(saveDataUser(dataUser)),
	products: products => dispatch(saveDataProductsUser(products)),
	// saveAllCards: cards => dispatch(saveAllCardsAction(cards)),
});

const mapStateToProps = currentStore => ({
	cards: selectCards(currentStore),
});

FormProduct.propTypes = {
	cards: PropTypes.array.isRequired,
	// dataUser: PropTypes.object.isRequired,
	confirm: PropTypes.string,
};

export default connect(
	// () => ({}),
	mapStateToProps,
	mapDispatchToProps
)(FormProduct);
