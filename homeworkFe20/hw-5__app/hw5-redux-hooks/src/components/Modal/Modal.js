import React from 'react';
import Button from '../Button/Button';
import './Modal.scss';

const Modal = props => {
	const { className, title, bas, setBas, action } = props;

	return (
		<div className="container__modal" onClick={() => setBas(!bas)}>
			<div className="modal" onClick={e => e.stopPropagation()}>
				<div className="container__modal-btn">
					<Button
						className="modal-btn"
						title={'X'}
						onClick={() => setBas(!bas)}
					/>
				</div>
				<p>{title}</p>
				<div className="container-btn">{action}</div>
			</div>
		</div>
	);
};
export default Modal;
