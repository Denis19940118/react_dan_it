import React from 'react';
import PropTypes from 'prop-types';
import ContainerCard from '../../components/ContainerCard/ContainerCard';
import { connect } from 'react-redux';
import { selectCards } from '../../redux/profile/selectors';

const Favorites = props => {
	const { cards, url, cardD } = props;

	const cardsFav = cards.filter(el => el.favorite);

	return (
		<ContainerCard
			title={'Your favorite food'}
			cards={cardsFav}
			openedFav={true}
			btnDetail={true}
			btnDel={false}
			url={url}
			cardD={cardD}
		/>
	);
};

const mapStateToProps = currentStore => ({
	cards: selectCards(currentStore),
});

Favorites.propTypes = {
	cards: PropTypes.array,
};

export default connect(mapStateToProps)(Favorites);
