import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import ContainerCard from '../../components/ContainerCard/ContainerCard';
import { connect } from 'react-redux';
import { selectCards } from '../../redux/profile/selectors';
import FormProduct from '../../components/FormProduct3/FormProduct';

const Basket = props => {
	// const { cards, url, cardD } = props;
	// console.log('cards', cards);
	// // const basket = JSON.parse(localStorage.getItem('basket'));
	// // console.log('🚀 ~ file: Basket.js ~ line 12 ~ basket', basket);

	// const [basket, setBasket] = useState([]);

	// useEffect(() => {
	// 	setBasket(JSON.parse(localStorage.getItem('basket')));
	// 	console.log('basket', basket);
	// });
	// const toBasket = cards.filter(el => basket.some(b => el.id === b));
	// console.log('toBasket', toBasket);

	// return (
	// 	<div>
	// 		<ContainerCard cards={toBasket} url={url} btnDel={true} cardD={cardD} />
	// 		<FormProduct url={url} />
	// 	</div>
	// );

	const { cards, url, cardD, confirm } = props;

	const [basket, setBasket] = useState([]);
	useEffect(() => {
		setBasket(JSON.parse(localStorage.getItem('basket')));
	}, []);

	const toBasket = cards.filter(el => basket.some(b => el.id === b));

	return (
		<div>
			<ContainerCard
				title={'Your favorite food'}
				url={url}
				cards={toBasket}
				btnDel={true}
				btnDetail={true}
				cardD={cardD}
			/>
			<FormProduct url={url} confirm={confirm} />
		</div>
	);
};

const mapStateToProps = currentStore => ({
	cards: selectCards(currentStore),
});

Basket.propTypes = {
	cards: PropTypes.array,
};

export default connect(mapStateToProps)(Basket);
