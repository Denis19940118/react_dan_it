import './App.scss';
import ModalContainer from './components/ModalContainer/ModalContainer';
const buttonsList = [
	{
		id: 1,
		name: `Open first modal`,
		text: 'This is first modal',
		btn1: 'pink',
		btn2: 'yellow',
		color: 'brown',
	},
	{
		id: 2,
		name: `Open second modal`,
		text: 'This is second modal',
		btn1: 'grey',
		btn2: 'red',
		color: 'green',
	},
];

function App() {
	const buttons = buttonsList.map(b => (
		<>
			<ModalContainer key={b.id} {...b} />
		</>
	));
	return buttons;
}

export default App;
