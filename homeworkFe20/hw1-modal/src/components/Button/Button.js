import React from 'react';
import PropTypes from 'prop-types';

const Button = props => {
	const { className, title, backgroundColor, onClick, width, fontSize } = props;

	const style = {
		backgroundColor: backgroundColor,
		width: width,
		fontSize: fontSize,
	};

	return (
		<button className={className} onClick={onClick} style={style}>
			{title}
		</button>
	);
};

Button.propTypes = {
	title: PropTypes.string,
	backgroundColor: PropTypes.string,
	onClick: PropTypes.func.isRequired,
};

Button.defaultProps = {
	title: 'Click me',
	fontSize: 14,
};

export default Button;
