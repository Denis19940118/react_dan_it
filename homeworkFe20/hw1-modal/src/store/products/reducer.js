import { TOGGLE_FAVORITES } from './types';

const initialState = {
	data: [],
};

const reducer = (currentStore = initialState, action) => {
	switch (action.type) {
		case TOGGLE_FAVORITES: {
			const product = action.payload;
			const index = currentStore.data.indexOf(product);
			const updateProduct = { ...product, favorite: !product.favorite };
			const newProduct = currentStore.data;
			newProduct(index, 1, updateProduct);

			return {
				...currentStore,
				data: newProduct,
			};
		}
		default: {
			return currentStore;
		}
	}
};

export default reducer;
