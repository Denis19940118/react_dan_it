import React from 'react';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { selectCards } from '../../redux/profile/selectors';
import './CardDetails.scss';
import Card from '../Card/Card';

const CardDetails = props => {
	const { cards, url } = props;
	const history = useHistory();

	const basketCard = JSON.parse(localStorage.getItem('Card'));
	const toBasket = [];

	cards.filter(el => (el.id === basketCard ? toBasket.push(el) : ''));
	console.log('toBasket[0]', toBasket[0]);

	const goToCard = () => {
		history.push(`${url}`);
		localStorage.removeItem('Card');
	};

	return (
		<>
			<Card
				key={toBasket[0].id}
				btnDel={false}
				url={url}
				{...toBasket[0]}
				card={toBasket[0]}
			/>
			<div>
				<button onClick={goToCard}>Go back</button>
			</div>
		</>
	);
};

const mapStateToProps = currentStore => ({
	cards: selectCards(currentStore),
});

export default connect(mapStateToProps)(CardDetails);
