import { combineReducers } from 'redux';
import profileReducer from './profile/reducer';
import products from './products/reducer';

const reducer = combineReducers({ user: profileReducer, products });
export default reducer;
