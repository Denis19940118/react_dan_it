import { SAVE_DATA_PRODUCT_USER, SAVE_DATA_USER } from './types';

const initialStore = {
	products: null,
	dataUser: null,
};

const reducer = (currentStore = initialStore, action) => {
	switch (action.type) {
		case SAVE_DATA_PRODUCT_USER:
			console.log(' action', action);

			return { ...currentStore, ...action.payload };
		case SAVE_DATA_USER:
			console.log(' action', action);

			return {
				...currentStore,
				...action.payload,
			};

		default:
			return currentStore;
	}
};

export default reducer;
