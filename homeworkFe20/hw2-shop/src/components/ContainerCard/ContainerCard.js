import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Card from '../Card/Card';
import './ContainerCard.scss';

const ContainerCard = props => {
	const [cards, setCards] = useState([]);
	const [isFavorite] = useState(
		JSON.parse(localStorage.getItem('favorite')) ||
			localStorage.setItem('favorite', JSON.stringify([]))
	);

	useEffect(() => {
		fetch('./products.json')
			.then(res => res.json())
			.then(res => setCards(res));
	}, [isFavorite]);

	const card = cards.map(c => (
		<Card isFavorite={isFavorite} key={c.id} {...c} />
	));

	return <div className="container__body">{card}</div>;
};

ContainerCard.propTypes = {
	cards: PropTypes.array,
	isFavorite: PropTypes.object,
};

export default ContainerCard;
