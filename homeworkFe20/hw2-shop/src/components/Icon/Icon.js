import React from 'react';
import * as icons from '../../theme/icons';
import PropTypes from 'prop-types';

function Icon(props) {
	const { type, color, filled, onClick } = props;

	const iconJsx = icons[type];

	if (!iconJsx) {
		return null;
	}

	return <span onClick={onClick}>{iconJsx(color, filled)}</span>;
}

Icon.propTypes = {
	type: PropTypes.string,
	color: PropTypes.string,
	filled: PropTypes.bool.isRequired,
	onClick: PropTypes.func.isRequired,
};

export default Icon;
