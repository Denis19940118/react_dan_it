import React from 'react';

const Head = props => {
	const { className, title } = props;

	return <div className={className}>{title}</div>;
};
export default Head;
