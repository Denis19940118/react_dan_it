import React, { useEffect, useState } from 'react';
import Button from '../Button/Button';
import './Card.scss';
import PropTypes from 'prop-types';
import Icon from '../Icon/Icon';
import Modal from '../Modal/Modal';

const Card = props => {
	const [opened, setOpened] = useState(false);
	const [openedBas, setOpenedBas] = useState(false);

	const { id, name, price, picture, articule, color, isFavorite } = props;

	useEffect(() => {
		isFavorite.map(el => (el === id ? setOpened(!opened) : ''));
	}, [isFavorite, id]);

	const handleClickToFavorite = op => {
		if (op) {
			const favorite = JSON.parse(localStorage.getItem('favorite'));
			localStorage.removeItem('favorite');

			favorite.push(id);
			localStorage.setItem('favorite', JSON.stringify(favorite));
		} else {
			const i = JSON.parse(localStorage.getItem('favorite'));
			localStorage.removeItem('favorite');

			const fav = [];
			i.map(el => (el !== id ? fav.push(el) : ''));
			localStorage.setItem('favorite', JSON.stringify(fav));
		}
	};

	const changeColor = () => {
		setOpened(!opened);
		handleClickToFavorite(!opened);
	};

	return (
		<div className="card card__container">
			<img className="card__img" src={picture} alt={name} />
			<p className="card__text">
				{name}
				<Icon
					type={'star'}
					color={color}
					filled={opened}
					onClick={() => changeColor()}
				/>
			</p>

			<p className="card__text">Price: {price}</p>
			<p className="card__text">articule: {articule}</p>
			<Button title="Add to basket" onClick={() => setOpenedBas(!openedBas)} />
			{openedBas && (
				<Modal
					title={'Add to Basket?'}
					action={
						<>
							<Button
								className="btn-modal"
								title={'Ok'}
								onClick={() => setOpenedBas(!openedBas)}
							/>
							<Button
								className="btn-modal"
								title={'X'}
								onClick={() => setOpenedBas(!openedBas)}
							/>
						</>
					}
					opened={openedBas}
					setOpened={setOpenedBas}
				/>
			)}
		</div>
	);
};

Card.propTypes = {
	id: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	price: PropTypes.string.isRequired,
	picture: PropTypes.string,
	articule: PropTypes.string.isRequired,
	color: PropTypes.string,
	isFavorite: PropTypes.array.isRequired,
};

export default Card;
