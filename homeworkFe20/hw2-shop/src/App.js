import './App.scss';
import { Fragment } from 'react';
import ContainerCard from './components/ContainerCard/ContainerCard';
import Head from './components/Head/Head';

function App() {
	return (
		<Fragment>
			<Head className="header" title={'Hello React!'} />
			<ContainerCard />
		</Fragment>
	);
}

export default App;
