import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Card from '../Card/Card';
import './ContainerCard.scss';

const ContainerCard = props => {
	const {
		cards,
		openedFav,
		btnDel,
		url,
		cardD,
		title,
		showDetails,
		details,
	} = props;

	let card = null;

	if (!showDetails) {
		card = cards.map(c => (
			<Card
				key={c.id}
				{...c}
				card={c}
				openedFav={openedFav}
				btnDel={btnDel}
				url={url}
				cardD={cardD}
			/>
		));
	}

	return (
		<div className="container__body">
			<h2 className={'container__body-heading'}>{title}</h2>
			{!showDetails && <div className="container__body-card">{card}</div>}
			{showDetails && details()}
		</div>
	);
};

ContainerCard.propTypes = {
	cards: PropTypes.array,
	btnDel: PropTypes.bool,
	openedFav: PropTypes.bool,
};

ContainerCard.defaultProps = {
	className: '',
};

export default memo(ContainerCard);
