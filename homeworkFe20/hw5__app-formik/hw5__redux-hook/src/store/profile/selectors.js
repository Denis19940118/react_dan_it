export const selectCards = currentStore => currentStore.cards;
export const selectProductsUser = currentStore => currentStore.products;
export const selectDataUser = currentStore => currentStore.dataUser;
