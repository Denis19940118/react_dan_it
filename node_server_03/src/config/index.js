import dotenv from 'dotenv';

const environment = dotenv.config();

if (!environment) {
	throw new Error('Config file was not found.');
}

export default {
	/**
	 * @desc Port of the server
	 **/
	PORT: process.env.PORT || 3005,

	/**
	 * @desc The service prefix
	 **/
	SERVICE_PREFIX: process.env.SERVICE_PREFIX || '/api',

	/**
	 * @desc The service version
	 **/
	VERSION: process.env.VERSION || 1

}