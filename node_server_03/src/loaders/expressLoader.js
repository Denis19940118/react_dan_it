import routes from '../api';

/**
 * @desc Express loader
 **/
export default function({config, app}) {
	// Load API routes
	app.use(`${config.SERVICE_PREFIX}/v${config.VERSION}`, routes());
}