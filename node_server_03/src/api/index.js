import {Router} from 'express';

import home from './routes/home';

export default () => {
	const router = Router();

	home(router);

	return router;
}