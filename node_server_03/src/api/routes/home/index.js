import {Router} from 'express';
import {
	HomeController,
// 	aboutController,
// 	productsController
} from './controller';

// help information
const route = Router();

export default mainRoute => {
	mainRoute.use('/', route);

	/**
	 * @desc
	 **/
	route.get('/', HomeController);


	route.get('/home1', (request, response) => {
		response.send('<h1>Sends from HOME1</h1>');
	});
}
