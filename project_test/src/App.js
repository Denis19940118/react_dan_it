import './App.css';
import TimerDisplay from './components/TimerDisplay/TimerDisplay';

function App() {
	return (
		<div className="App">
			<TimerDisplay />
		</div>
	);
}

export default App;
