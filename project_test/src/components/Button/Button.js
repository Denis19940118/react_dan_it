import React from 'react';
import PropTypes from 'prop-types';
import './Button.scss';

const Button = props => {
	const { className, type, title, onClick } = props;

	return (
		<button className={className} type={type} onClick={onClick}>
			{title}
		</button>
	);
};

Button.propTypes = {
	className: PropTypes.string,
	type: PropTypes.string.isRequired,
	title: PropTypes.string.isRequired,
	onClick: PropTypes.func.isRequired,
};

Button.defaultProps = {
	className: 'btn__default',
};

export default Button;
