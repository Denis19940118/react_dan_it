import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import Button from '../Button/Button';
import ResetButton from '../ResetButton/ResetButton';
import PauseButton from '../PauseButton/PauseButton';

const TimerButtons = props => {
	const {
		seconds,
		setSeconds,
		minutes,
		setMinutes,
		hours,
		setHours,
		timerOpened,
		setTimerOpened,
		displayTimer,
	} = props;
	console.log('timerOpened', timerOpened.type);

	let handle = null;

	useEffect(() => {
		if (timerOpened === false) {
			handleTime();
		}
	});

	const handleTime = () => {
		handle = setTimeout(() => {
			if (seconds <= 58) {
				setSeconds(seconds + 1);
			} else if (seconds >= 59) {
				setSeconds(0);
				setMinutes(minutes + 1);
			}
			if (minutes >= 59 && seconds >= 59) {
				setMinutes(0);
				setHours(hours + 1);
			}
		}, 1000);
	};

	const startTimer = () => {
		setTimerOpened(false);
	};

	const stop = () => {
		setSeconds(0);
		setMinutes(0);
		setHours(0);
	};

	const stopTimer = () => {
		setTimerOpened(true);
		stop();

		displayTimer();
		clearTimeout(handle);
	};

	return (
		<div>
			{timerOpened && (
				<Button type="button" title="Start" onClick={() => startTimer()} />
			)}

			{!timerOpened && (
				<Button type="button" title="Stop" onClick={() => stopTimer()} />
			)}
			<ResetButton
				stopTimer={stopTimer}
				displayTimer={displayTimer}
				startTimer={startTimer}
			/>
			<PauseButton setTimerOpened={setTimerOpened} handle={handle} />
		</div>
	);
};

TimerButtons.propTypes = {
	seconds: PropTypes.number.isRequired,
	setSeconds: PropTypes.func.isRequired,
	minutes: PropTypes.number.isRequired,
	setMinutes: PropTypes.func.isRequired,
	hours: PropTypes.number.isRequired,
	setHours: PropTypes.func.isRequired,
	setTimerOpened: PropTypes.func.isRequired,
	displayTimer: PropTypes.func.isRequired,
};

export default TimerButtons;
