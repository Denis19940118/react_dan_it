import React, { useState } from 'react';
import PropTypes from 'prop-types';
import TimerButtons from '../TimerButtons/TimerButtons';
import './TimerDisplay.scss';

const TimerDisplay = props => {
	const [seconds, setSeconds] = useState(57);
	const [minutes, setMinutes] = useState(59);
	const [hours, setHours] = useState(1);
	const [timerOpened, setTimerOpened] = useState('true');
	const { className } = props;

	const zeroAdd = props => {
		return <>{props < 10 ? `0${props}` : props}</>;
	};

	const displayTimer = () => {
		return (
			<>
				<span>{zeroAdd(hours)}</span>:<span>{zeroAdd(minutes)}</span>:
				<span>{zeroAdd(seconds)}</span>
			</>
		);
	};

	return (
		<div>
			<h3>Timer</h3>
			<p className={className}>{displayTimer()}</p>
			<TimerButtons
				seconds={seconds}
				setSeconds={setSeconds}
				minutes={minutes}
				setMinutes={setMinutes}
				hours={hours}
				setHours={setHours}
				timerOpened={timerOpened}
				setTimerOpened={setTimerOpened}
				displayTimer={displayTimer}
			/>
		</div>
	);
};

TimerDisplay.propTypes = {
	seconds: PropTypes.number,
	minutes: PropTypes.number,
	hours: PropTypes.number,
	timerOpened: PropTypes.bool,
};

TimerDisplay.defaultProps = {
	className: 'display__default-cifral',
};

export default TimerDisplay;
