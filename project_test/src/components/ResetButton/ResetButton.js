import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button/Button';

const ResetButton = props => {
	const { stopTimer, displayTimer, startTimer } = props;

	const resetTimer = () => {
		stopTimer();
		displayTimer();
		startTimer();
	};

	return <Button type="button" title="Reset" onClick={() => resetTimer()} />;
};

ResetButton.propTypes = {
	stopTimer: PropTypes.func,
	displayTimer: PropTypes.func,
	startTimer: PropTypes.func,
};

export default ResetButton;
