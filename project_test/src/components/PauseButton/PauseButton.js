import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Button from '../Button/Button';

const PauseButton = props => {
	const { setTimerOpened, handle } = props;
	const [clicks] = useState([]);

	const pauseTimer = () => {
		setTimerOpened(true);
		// clearTimeout(handle);
	};

	const clickHandler = event => {
		console.log('event', event);
		event.preventDefault();
		clicks.push(new Date().getTime());
		// clearTimeout(handle);
		// handle = setTimeout(() => {
		if (
			clicks.length > 1 &&
			clicks[clicks.length - 1] - clicks[clicks.length - 2] < 300
		) {
			pauseTimer(event.target);
		}
		// }, 300);
	};

	return <Button type="button" title="Pause" onClick={clickHandler} />;
};

PauseButton.propTypes = {
	setTimerOpened: PropTypes.func.isRequired,
};

export default PauseButton;
