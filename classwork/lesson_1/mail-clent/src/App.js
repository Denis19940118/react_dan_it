// import React, { Children, Component } from 'react';
// import logo from './logo.svg';
// import './App.scss';
// import Body from './components/Body/Body';
// import Header from './components/Header/Header';
// import Footer from './components/Footer/Footer';
// import Loader from './components/Loader/Loader';
// import axios from 'axios';
// import Sidebar from './components/Sidebar/Sidebar';

// //npm i prop-types
// // React.createElement("div", {className="App"}, Children....)

// // const emails = [
// // 	{ id: 1, topic: 'Email 1', body: 'Lorem ipsum Email 1' },
// // 	{ id: 2, topic: 'Email 2', body: 'Lorem ipsum Email 2' },
// // 	{ id: 3, topic: 'Email 3', body: 'Lorem ipsum Email 3' },
// // ];

// class App extends Component {
// 	// constructor (props){
// 	// super(props)
// 	// console.log('componentDidMount in App JS');
// 	// this.state = {}
// 	// }

// 	state = {
// 		currentUser: undefined,
// 		// {
// 		// 	name: 'Andrew',
// 		// 	age: 32,
// 		// 	email: 'aaa@a.com',
// 		// 	hobbies: [],
// 		// },
// 		title: 'Hello world',
// 		emails: [],
// 		isLoading: true,
// 		// errors:
// 	};

// 	componentDidMount() {
// 		setTimeout(
// 			() =>
// 				axios('/emails.json')
// 					//.then(res => res.json()) если fetch
// 					.then(res => this.setState({ emails: res.data, isLoading: false })),
// 			1000
// 		);
// 		// console.log('componentDidMount in App JS');
// 	}
// 	com;
// 	componentDidUpdate() {
// 		console.log('componentDidUpdate in App JS');
// 	}

// 	componentDidCatch() {}

// 	render() {
// 		// console.log('logo', logo, this);

// 		// const currentUser = {
// 		// 	name: 'Andrew',
// 		// 	age: 32,
// 		// 	email: 'aaa@a.com',
// 		// 	hobbies: [],
// 		// };
// 		//npx create-react-app add_simple-counter
// 		const { currentUser, title, emails, isLoading } = this.state;
// 		// const { name, age, email, hobbies } = currentUser;

// 		// const hasEmail = !!email;

// 		// if (isLoading) {
// 		// 	return <Loader />;
// 		// }

// 		return (
// 			<div className="App">
// 				<Header title={title} user={currentUser} />
// 				<Sidebar />
// 				{isLoading && <Loader />};
// 				{!isLoading && (
// 					<Body
// 						incrementAge={this.incrementAge}
// 						updateTitle={this.updateTitle}
// 						emails={emails}
// 					/>
// 				)}
// 				<Footer />
// 				<div>
// 					{/* {!!hobbies.length && hobbies}
// 					{!hobbies.length && 'No hobbies specified'} */}
// 				</div>
// 				{/* <img></img>
//         State & Props
//         */}
// 			</div>
// 		);
// 	}
// 	incrementAge = () => {
// 		const { currentUser } = this.state;
// 		const { age } = currentUser;

// 		this.setState({ currentUser: { ...currentUser, age: age + 1 } });
// 		// spread , rest  повторить
// 	};

// 	updateTitle = () => {
// 		const { title } = this.state;

// 		this.setState({ title: 'New title' });
// 	};
// }

// // function App() {
// // 	// console.log('logo', logo);
// // 	// return (
// // 	// 	<div className="App">
// // 	// 		<header className="App-header">
// // 	// 			<img src="/logo192.png" className="App-logo" alt="logo" />
// // 	// 			<img src={logo} className="App-logo" alt="logo" />
// // 	// 			<p>
// // 	// 				Edit <code>src/App.js</code> and save to reload. Hello React
// // 	// 			</p>
// // 	// 			<a
// // 	// 				className="App-link"
// // 	// 				href="https://reactjs.org"
// // 	// 				target="_blank"
// // 	// 				rel="noopener noreferrer"
// // 	// 			>
// // 	// 				Learn React
// // 	// 			</a>
// // 	// 		</header>
// // 	// 	</div>
// // 	// );
// // }

// export default App;

import React, { Component } from 'react';
import './App.scss';
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import Loader from './components/Loader/Loader';
import axios from 'axios';
import Sidebar from './components/Sidebar/Sidebar';
import AppRoutes from './routes/AppRoutes';

class App extends Component {
	// constructor (props) {
	//   super(props)

	//   console.log('constructor in App.js')
	// }

	state = {
		currentUser: undefined,
		title: 'Hello world',
		emails: [],
		isLoading: true,
	};

	componentDidMount() {
		setTimeout(() => {
			axios('/emails.json').then(res =>
				this.setState({ emails: res.data, isLoading: false })
			);
		}, 1000);

		console.log('componentDidMount in App.js');
	}

	componentDidUpdate() {
		// console.log('componentDidUpdate in App.js')
	}

	render() {
		// console.log('App.js', this)
		const { currentUser, title, emails, isLoading } = this.state;

		return (
			<div className="App">
				<Header title={title} user={currentUser} />
				<Sidebar />
				{isLoading && <Loader />}
				{!isLoading && <AppRoutes emails={emails} />}
				<Footer />
			</div>
		);
	}

	incrementAge = () => {
		const { currentUser } = this.state;
		const { age } = currentUser;

		this.setState({ currentUser: { ...currentUser, age: age + 1 } });
	};

	updateTitle = () => {
		const { title } = this.state;

		this.setState({ title: 'New title' });
	};
}

export default App;
