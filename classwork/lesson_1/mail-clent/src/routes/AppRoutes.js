import React, { PureComponent } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import Inbox from '../pages/Inbox/Inbox';
import Sent from '../pages/Sent/Sent';
import Favorites from '../pages/Favorites/Favorites';
// import Page404 from '../pages/Page404/Page404';
// import OneEmail from '../pages/OneEmail/OneEmail';

// class AppRoutes extends PureComponent {
// 	render() {
// 		const { emails } = this.props;

// 		return (
// 			<Switch>
// 				{/* <Route exact path="/" render={() => <Redirect to="/inbox" />} /> */}
// 				<Redirect exact from="/" to="/inbox" />
// 				<Route
// 					exact
// 					path="/inbox"
// 					render={routerProps => <Inbox emails={emails} {...routerProps} />}
// 				/>
// 				<Route
// 					exact
// 					path="/email/:emailId"
// 					render={routerProps => <OneEmail emails={emails} {...routerProps} />}
// 				/>
// 				<Route exact path="/sent" component={Sent} />
// 				<Route exact path="/favorites">
// 					<Favorites />
// 				</Route>
// 				<Route path="*" component={Page404} />
// 			</Switch>
// 		);
// 	}
// }

// export default AppRoutes;

import Page404 from '../pages/Page404/Page404';
import OneEmail from '../pages/OneEmail/OneEmail';

class AppRoutes extends PureComponent {
	render() {
		const { emails } = this.props;

		return (
			<Switch>
				<Redirect exact from="/" to="/inbox" />
				{/* <Route exact path='/' render={(routerProps) => <Inbox emails={emails} {...routerProps} />} /> */}
				<Route
					exact
					path="/inbox"
					render={routerProps => <Inbox emails={emails} {...routerProps} />}
				/>
				<Route
					exact
					path="/emails/:emailId"
					render={routerProps => <OneEmail emails={emails} {...routerProps} />}
				/>
				<Route exact path="/sent" component={Sent} />
				<Route exact path="/favorites">
					<Favorites />
				</Route>
				<Route path="*" component={Page404} />
			</Switch>
		);
	}
}

export default AppRoutes;
