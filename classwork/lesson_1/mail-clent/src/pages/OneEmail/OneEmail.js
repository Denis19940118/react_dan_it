import React, { PureComponent } from 'react';
import Email from '../../components/Email/Email';

class OneEmail extends PureComponent {
	render() {
		const { emails, match } = this.props;
		const emailId = +match.params.emailId;

		const email = emails.find(e => e.id === emailId);
		return (
			<div>
				<Email email={email} showFull />
			</div>
		);
	}
}

export default OneEmail;
// import React, { PureComponent } from 'react';
// import Email from '../../components/Email/Email';

// class OneEmail extends PureComponent {
// 	render() {
// 		const { emails, match } = this.props;
// 		const emailId = +match.params.emailId;

// 		const email = emails.find(e => e.id === emailId);

// 		return (
// 			<div>
// 				<Email email={email} showFull />
// 			</div>
// 		);
// 	}
// }
