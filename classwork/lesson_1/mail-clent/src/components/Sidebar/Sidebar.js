import React, { PureComponent } from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import Page404 from '../../pages/Page404/Page404';
import './Sidebar.scss';

class Sidebar extends PureComponent {
	render() {
		return (
			<ul>
				<li>
					<NavLink to="/inbox" className="link" activeClassName="link__active">
						Inbox
					</NavLink>
				</li>
				<li>
					<NavLink to="/sent" className="link" activeClassName="link__active">
						Sent
					</NavLink>
				</li>
				<li>
					<NavLink
						to="/favorites"
						className="link"
						activeClassName="link__active"
					>
						Favorites
					</NavLink>
				</li>
				<li>
					<NavLink
						to="/not-exist"
						className="link"
						activeClassName="link__active"
					>
						Not exist
					</NavLink>
				</li>
			</ul>
		);
	}
}

export default withRouter(Sidebar);
