// import React, { PureComponent } from 'react';
// import PropTypes from 'prop-types';

// class Email extends PureComponent {
// 	render() {
// 		const { topic, body, id } = this.props.email;

// 		return (
// 			<div>
// 				<div>{topic}</div>
// 				<div>{body}</div>
// 				<div>{id}</div>
// 			</div>
// 		);
// 	}
// 	// static propTypes = {}
// }

// //object, number, string, func, bool, array, symbol
// // isRequired
// // instanceOf(), shape(не строгая валид), exact(строгая валид), oneOfType()
// Email.propTypes = {
// 	// email: PropTypes.object.isRequired,
// 	// title: PropTypes.string.isRequired,
// 	email: PropTypes.shape({
// 		topic: PropTypes.number.isRequired,
// 		body: PropTypes.string.isRequired,
// 		id: PropTypes.string,
// 	}).isRequired,
// };
// export default Email;
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';

class Email extends PureComponent {
	render() {
		const { showFull, email } = this.props;
		const { topic, body, id } = this.props.email;

		return (
			<div>
				<Link to={`/emails/${id}`}>{topic}</Link>
				{showFull && <div>{body}</div>}
				{showFull && (
					<div>
						<button onClick={this.goBack}>Go back</button>
					</div>
				)}
			</div>
		);
	}
	goBack = () => {
		this.props.history.goBack();
	};

	// static propTypes = {}
}

//object, number, string, bool, func, array, symbol
// isRequired
// instanceOf(), shape(), exact(), oneOfType([])
Email.propTypes = {
	email: PropTypes.shape({
		id: PropTypes.number.isRequired,
		topic: PropTypes.string.isRequired,
		body: PropTypes.string,
	}).isRequired,
};

export default withRouter(Email);
