import Axios from 'axios';
import React, { PureComponent } from 'react';
import ErrorPage from '../ErrorPage/ErrorPage';

class ErrorBoundary extends PureComponent {
	state = {
		hasError: false,
	};

	componentDidCatch(error, errorInfo) {
		// axios.post()
		// console.log(errorInfo);
		// console.log(error);
		// this.setState({ hasError: true });
	}

	static getDerivedStateFromError(error) {
		return { hasError: true };
	}

	render() {
		const { children } = this.props;
		const { hasError } = this.state;

		if (hasError) {
			return (
				// <div>
				// 	<h2>An error has occured</h2>
				// 	<h3>Error details: </h3>
				// 	<button>Go home</button>
				// </div>
				<ErrorPage />
			);
		}

		return children;
	}
}

export default ErrorBoundary;
