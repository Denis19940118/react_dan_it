import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';

class Header extends PureComponent {
	render() {
		const { title, user } = this.props;
		const { name, age, email } = user;
		// console.log('Header ', this);

		const hasEmail = !!email;
		return (
			<Fragment>
				<h2>{title}</h2>
				<div>{user.name}</div>
				<div>{name}</div>
				<div>{age}</div>
				<div>{email}</div>
				<div>
					{/* hasEmail ? email : 'No email specified' */}
					{hasEmail && email}
					{!hasEmail && 'No email specified'}
				</div>
			</Fragment>
		);
	}
}

Header.propTypes = {
	title: PropTypes.string,
	user: PropTypes.shape({
		name: PropTypes.string.isRequired,
		age: PropTypes.number,
		email: PropTypes.string,
	}),
};

Header.defaultProps = {
	title: 'Mail Client',
	user: {
		name: 'Anonimous user',
	},
};

export default Header;
