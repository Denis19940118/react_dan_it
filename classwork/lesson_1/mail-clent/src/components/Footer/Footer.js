import React, { PureComponent } from 'react';

class Footer extends PureComponent {
	render() {
		return <h2> &copy; Dan.it Education 2020</h2>;
	}
}

export default Footer;
