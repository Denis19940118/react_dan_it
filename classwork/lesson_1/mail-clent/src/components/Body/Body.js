import React, { PureComponent } from 'react';
import Email from '../Email/Email';
import PropTypes from 'prop-types';
import Button from '../Button/Button';

class Body extends PureComponent {
	render() {
		const { incrementAge, updateTitle, emails } = this.props;
		// const numbers = [1,2,3,4,5,6];

		// const emailCards = emails.map(e => (
		// 	<div key={e.id}>
		// 		{e.topic}
		// 		<br></br>
		// 		{e.body}
		// 	</div>
		// ));
		const emailCards = emails.map(e => <Email key={e.id} email={e} />);

		return (
			<>
				<div>
					{true}
					{false}
					{null}
					{undefined}
				</div>
				<Button onClick={incrementAge}>Increment age</Button>
				<Button onClick={updateTitle}>Update title</Button>
				<div>{emailCards}</div>
			</>
		);
	}

	incrementAge = () => {
		const { currentUser } = this.state;
		const { age } = currentUser;

		this.setState({ currentUser: { ...currentUser, age: age + 1 } });
		// spread , rest  повторить
	};
}
Body.propTypes = {
	incrementAge: PropTypes.func.isRequired,
	updateTitle: PropTypes.func.isRequired,
	emails: PropTypes.array.isRequired,
};
export default Body;
