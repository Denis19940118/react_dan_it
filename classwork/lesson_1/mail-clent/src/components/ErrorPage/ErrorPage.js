import React, { PureComponent } from 'react';
class ErrorPage extends PureComponent {
	render() {
		return (
			<div className="error">
				<h2>An error has occured</h2>
				<h3>Error details: </h3>
				<button>Go home</button>
			</div>
		);
	}
}

export default ErrorPage;
