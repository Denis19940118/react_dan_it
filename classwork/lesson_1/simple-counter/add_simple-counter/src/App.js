import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
	state = {
		procent: 50,
		choiceProc: 5,
	};

	render() {
		const { procent, choiceProc } = this.state;

		return (
			<div>
				<div className="container">
					<button onClick={this.decrementProc}> - </button>
					<span>{procent} %</span>
					<button onClick={this.incrementProc}> + </button>
				</div>

				<div className="container">
					<button onClick={this.decrementChoi}> - </button>
					<span>{choiceProc} point</span>
					<button onClick={this.incrementChoi}> + </button>
				</div>
				<form>
					<input
						type="number"
						placeholder="Enter number"
						onChange={this.handelChange}
						value={this.state.procent}
					></input>
					<input
						type="number"
						placeholder="Enter number"
						onChange={this.handelChangePoint}
						value={this.state.choiceProc}
					></input>
				</form>
			</div>
		);
	}
	// - 50% +
	// - 5 +  add input type number;
	incrementProc = () => {
		const { procent, choiceProc } = this.state;

		if (procent >= 100) return;
		this.setState({ procent: Math.min(procent + choiceProc, 100) });
	};
	decrementProc = () => {
		const { procent, choiceProc } = this.state;

		if (procent <= 0) return;
		this.setState({ procent: Math.max(procent - choiceProc, 0) });
	};

	incrementChoi = () => {
		const { choiceProc } = this.state;

		if (choiceProc >= 10) return;
		this.setState({ choiceProc: choiceProc + 1 });
	};
	decrementChoi = () => {
		const { choiceProc } = this.state;

		if (choiceProc <= 1) return;
		this.setState({ choiceProc: choiceProc - 1 });
	};

	handelChange = ({ target }) => {
		this.setState({ procent: target.value });
	};
	handelChangePoint = ({ target }) => {
		this.setState({ choiceProc: target.value });
	};
}

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

export default App;
