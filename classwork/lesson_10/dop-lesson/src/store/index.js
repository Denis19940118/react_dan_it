import { createStore } from 'redux';

const initialState = {
	counter: 0,
	dummy: 'sd',
};

function reducer(state = initialState, action) {
	// if (action.type)
	console.log('reduser', action);
	switch (action.type) {
		case 'INCREMENT':
			return { counter: state.counter + 1 };
		default:
			return state;
	}
}

const store = createStore(reducer);
export default store;
// store.subescribe(() => console.log('subescribe', store.getState()));

// store.dispatch({ type: 'HELLO', dummy: 'World' });
// store.dispatch({ type: 'INCREMENT', dummy: 'World' });
