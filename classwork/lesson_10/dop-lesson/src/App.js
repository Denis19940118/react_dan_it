import React, { useState } from 'react';
import {
	BrowserRouter as Router,
	Route,
	Switch,
	Link,
	NavLink,
} from 'react-router-dom';
import Homepage from './pages/Homepage';
import About from './pages/About';
import Forbitten from './pages/Forbitten';
import { Provider } from 'react-redux';
import store from './store';

// const dummyData = [1, 2, 3, 4, 5, 6, 7];

function App() {
	const [auth, setAuth] = useState(false);
	return (
		<Provider store={store}>
			<Router>
				<NavLink exact to="/" activeClassName="ac">
					Home
				</NavLink>
				<NavLink to="/about" activeClassName="ac">
					About
				</NavLink>

				{/* {dummyData.map(id => (
				<Link key={id} to={`/product/${id}`} activeClassName="ac">
					Go to product{id}
				</Link>
			))} */}
				{/* {Date.now()} */}
				<Switch>
					{/* {auth && (
					<Route path="/about">
						<About />
					</Route>
				)} */}
					{/* <Route path="/about" component={About} /> */}
					{/* <Route path="/about" render={() => <h1>Hello</h1>} /> */}
					{/* <Route
						path="/about"
						render={RouterProps => <About data="Hello" {...RouterProps} />}
					/> */}
					<Route path="/about" exact>
						<About />
					</Route>
					<Route path="/:param" exact>
						<Homepage />
					</Route>
					<Route>
						<Forbitten />
					</Route>
				</Switch>

				<button onClick={() => setAuth(true)}>Go auth</button>
			</Router>
		</Provider>
	);
}

export default App;
