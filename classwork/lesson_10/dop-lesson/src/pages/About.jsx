import React from 'react';
import { connect, useSelector, useDispatch } from 'react-redux';

const mapStateToProps = state => ({
	counter: state.counter,
});

const About = connect(mapStateToProps)(props => {
	const counter = useSelector(state => state.counter);
	const dispatch = useDispatch();
	return (
		<div>
			<h2>Counter: {props.counter}</h2>
			<button onClick={() => props.dispatch({ type: 'INCREMENT' })}>+</button>
		</div>
	);
});

export default About;
