import React from 'react';
import { useParams, useLocation, useHistory } from 'react-router-dom';

const Homepage = () => {
	const history = useHistory();
	const location = useLocation();
	const params = useParams();
	console.log(params);

	return (
		<div>
			<h1>Homepage</h1>
			<button onClick={() => history.goBack()}>go back</button>
			<button onClick={() => history.goForward()}>go forward</button>
			<button onClick={() => history.push('/retrtyuyiuo')}>push</button>
		</div>
	);
};

export default Homepage;
