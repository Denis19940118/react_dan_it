import React, { useEffect, useState } from 'react'
import Email from '../../components/Email/Email';
import axios from 'axios';
import Loader from '../../components/Loader/Loader';
import Page404 from '../Page404/Page404';

const OneEmail = (props) => {
  const [email, setEmail] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const { match } = props;
  const emailId = +match.params.emailId;

  useEffect(() => {
    setIsLoading(true);
    axios(`/api/emails/${emailId}`)
      .then(res => {
        setEmail(res.data)
        setIsLoading(false);
      })
  }, [emailId])

  if (isLoading) {
    return <Loader />
  }

  if (!email) {
    return <Page404 />
  }

  return (
    <div>
      <Email email={email} showFull />
    </div>
  )
}

export default OneEmail