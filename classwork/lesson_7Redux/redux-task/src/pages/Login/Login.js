import React, { useRef } from 'react';
import { Redirect, withRouter } from 'react-router-dom';
import Button from '../../components/Button/Button';
import './Login.scss';

const Login = props => {
	const { isAuth, saveCurrentUser } = props;

	const loginRef = useRef();
	const passwordRef = useRef();

	const loginUser = e => {
		e.preventDefault();

		const email = loginRef.current.value;
		const password = passwordRef.current.value;

		saveCurrentUser({ email, password });
	};

	if (isAuth) {
		return <Redirect to="/" />;
	}

	return (
		<form className="login" onSubmit={loginUser}>
			<div>
				<label>
					Email
					<input type="text" placeholder="email" ref={loginRef} />
				</label>
			</div>
			<div>
				<label>
					Password
					<input type="password" placeholder="password" ref={passwordRef} />
				</label>
			</div>
			<div>
				<Button type="submit">Submit</Button>
			</div>
		</form>
	);
};

export default withRouter(Login);
// import React, { useRef } from 'react';
// import { Redirect } from 'react-router-dom';
// import Button from '../../components/Button/Button';
// import './Login.scss';
// import * as userActions from '../../store/types';
// import { connect } from 'react-redux';

// const Login = props => {
// 	const { user, saveUser } = props;

// 	const isAuth = !!user;

// 	const loginRef = useRef();
// 	const passwordRef = useRef();

// 	const loginUser = e => {
// 		e.preventDefault();

// 		const email = loginRef.current.value;
// 		const password = passwordRef.current.value;

// 		saveUser({ email, password });
// 	};

// 	if (isAuth) {
// 		return <Redirect to="/" />;
// 	}

// 	return (
// 		<form className="login" onSubmit={loginUser}>
// 			<div>
// 				<label>
// 					Email
// 					<input type="text" placeholder="email" ref={loginRef} />
// 				</label>
// 			</div>
// 			<div>
// 				<label>
// 					Password
// 					<input type="password" placeholder="password" ref={passwordRef} />
// 				</label>
// 			</div>
// 			<div>
// 				<Button type="submit">Submit</Button>
// 			</div>
// 		</form>
// 	);
// };

// const mapStateToProps = ({ user }) => ({
// 	user,
// });

// const mapDispatchToProps = dispatch => ({
// 	saveUser: user => dispatch({ type: userActions.SAVE_USER, payload: user }),
// });

// export default connect(mapStateToProps, mapDispatchToProps)(Login);
