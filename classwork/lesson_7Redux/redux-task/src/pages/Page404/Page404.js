import React, { PureComponent } from 'react'
import { withRouter } from 'react-router-dom'

class Page404 extends PureComponent {
  render() {
    return (
      <div className='page-404'>
        <h2>404</h2>
        <h3>Page not found</h3>
        <div>
          <button onClick={this.goHome}>
            Go to home page
          </button>
        </div>
      </div>
    )
  }

  goHome = () => {
    this.props.history.push('/')
  }
}

export default withRouter(Page404)