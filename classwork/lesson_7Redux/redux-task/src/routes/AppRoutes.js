import React, { PureComponent } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom';
import Inbox from '../pages/Inbox/Inbox';
import Sent from '../pages/Sent/Sent';
import Favorites from '../pages/Favorites/Favorites';
import Page404 from '../pages/Page404/Page404';
import OneEmail from '../pages/OneEmail/OneEmail';
import Login from '../pages/Login/Login';
import Sidebar from '../components/Sidebar/Sidebar';

const AppRoutes = (props) => {
  const { emails, saveCurrentUser, currentUser } = props;

  // const isAuth = !!currentUser
  const isAuth = true

  return (
    <Switch>
      <Route exact path='/login'><Login saveCurrentUser={saveCurrentUser} isAuth={isAuth} /></Route>
      <>
        <Sidebar />
        <Switch>
          <Redirect exact from='/' to='/inbox' />
          {/* <Route exact path='/' render={(routerProps) => <Inbox emails={emails} {...routerProps} />} /> */}
          <ProtectedRoute authenticated={isAuth} exact path='/inbox' render={(routerProps) => <Inbox emails={emails} {...routerProps} />} />
          <ProtectedRoute authenticated={isAuth} exact path='/emails/:emailId' component={OneEmail} />
          <ProtectedRoute authenticated={isAuth} exact path='/sent' component={Sent} />
          <ProtectedRoute authenticated={isAuth} exact path='/favorites'><Favorites /></ProtectedRoute>

          <Route path='*' component={Page404} />
        </Switch>
      </>
    </Switch>
  )
}

const ProtectedRoute = ({ authenticated, ...rest }) => {
  return authenticated ? <Route {...rest} /> : <Redirect to='/login' />
}

export default AppRoutes