import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import ErrorBoundary from './components/ErrorBoundary/ErrorBoundary';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store/store';
// const initialStore = {
// 	user: 'Andrew',
// 	age: 32,
// };

// // const reducer = (state, action) => {
// // 	switch (action.type) {
// // 		case 'CHANGE_USER_NAME':
// // 			return { ...state, user: action.payload };
// // 	}
// // 	return state;
// // };

// const store = createStore(
// 	reducer,
// 	initialStore,
// 	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
// );

ReactDOM.render(
	// <React.StrictMode>
	<Provider store={store}>
		<BrowserRouter>
			<ErrorBoundary>
				<App />
			</ErrorBoundary>
		</BrowserRouter>
	</Provider>,
	// </React.StrictMode>
	document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
