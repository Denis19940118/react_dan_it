import React, { useEffect, useState } from 'react';
import './App.scss';
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import Loader from './components/Loader/Loader';
import axios from 'axios';
import AppRoutes from './routes/AppRoutes';

const App = () => {
	const [currentUser, setCurrentUser] = useState(undefined);
	const [title, setTitle] = useState('Hello world');
	const [emails, setEmails] = useState([]);
	const [isLoading, setIsLoading] = useState(true);

	useEffect(() => {
		setTimeout(() => {
			axios('/api/emails').then(res => {
				setEmails(res.data);
				setIsLoading(false);
			});
		}, 1000);

		return () => {
			// componentWillUnmount()
		};
	}, []);

	return (
		<div className="App">
			<Header title={title} user={currentUser} />
			{isLoading && <Loader />}
			{!isLoading && (
				<AppRoutes
					emails={emails}
					saveCurrentUser={setCurrentUser}
					currentUser={currentUser}
				/>
			)}
			<Footer />
		</div>
	);
};

export default App;
