import * as emailActions from './types';

export const emailsLoadingAction = (isLoading) => ({
  type: emailActions.EMAILS_LOADING, 
  payload: isLoading
})

export const saveEmailsAction = (emails) => ({
  type: emailActions.SAVE_EMAILS, 
  payload: emails
})