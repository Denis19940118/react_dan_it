const initialStore = {
	user: 'Andrew',
	age: 32,
};

const reducer = (state = initialStore, action) => {
	switch (action.type) {
		case 'CHANGE_USER_NAME':
			return { ...state, user: action.payload };
	}
	return state;
};
export default reducer;
