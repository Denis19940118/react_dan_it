import React from 'react';
import { createStore } from 'redux';
import reducer from './reducer';

// const initialStore = {
// 	user: 'Andrew',
// 	age: 32,
// };

const store = createStore(
	reducer,
	// initialStore,
	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
export default store;
