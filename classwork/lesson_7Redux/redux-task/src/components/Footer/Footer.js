import { func } from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

const Footer = props => {
	const { userName, changeName } = props;
	// console.log('Rendering Footer.js')

	return (
		<div>
			<h2>&copy; Dan.IT Education 2020{userName}</h2>;<div></div>
			<div>{userName}</div>
			<div>
				<button onClick={changeName}>Change name</button>
			</div>
		</div>
	);
};

const mapStateToProps = state => {
	return {
		userName: state.user,
	};
};
const mapDispatchTOProps = dispatch => {
	return {
		changeName: () => dispatch({ type: 'CHANGE_USER_NAME', payload: 'AAA' }),
	};
};

export default connect(mapStateToProps, mapDispatchTOProps)(Footer);
