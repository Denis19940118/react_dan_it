import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link, useHistory, withRouter } from 'react-router-dom';
import Icon from '../Icon/Icon';

const Email = props => {
	const { showFull, email, history } = props;
	const { topic, body, id } = email;

	// const dispatch = useDispatch();
	// //  const history = useHistory();
	// const emails = useSelector(state => state.emails);
	// const user = useSelector(state => state.user.data);
	const goBack = () => {
		history.goBack();
	};
	return (
		<div>
			<Link to={`/emails/${id}`}>{topic}</Link>
			{/* {Icons.star()}
			{Icons.star('red')}
			{Icons.globe()} */}
			<Icon type="star" color="red" />
			{showFull && <div>{body}</div>}
			{showFull && (
				<div>
					<button onClick={goBack}>Go back</button>
				</div>
			)}
			{showFull && (
				<div>
					<Link to={`/emails/${id - 1}`}>Previous</Link>
					<Link to={`/emails/${id + 1}`}>Next</Link>
				</div>
			)}
		</div>
	);
};

//object, number, string, bool, func, array, symbol
// isRequired
// instanceOf(), shape(), exact(), oneOfType([])
Email.propTypes = {
	email: PropTypes.shape({
		id: PropTypes.number.isRequired,
		topic: PropTypes.string.isRequired,
		body: PropTypes.string,
	}).isRequired,
};

export default withRouter(Email);
