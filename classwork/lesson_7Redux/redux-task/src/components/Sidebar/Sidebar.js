import React, { PureComponent } from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import './Sidebar.scss';

class Sidebar extends PureComponent {
  render() {
    return (
      <ul>
        <li><NavLink exact to='/inbox' className='link' activeClassName='link__active'>Inbox</NavLink></li>
        <li><NavLink exact to='/sent' className='link' activeClassName='link__active'>Sent</NavLink></li>
        <li><NavLink exact to='/favorites' className='link' activeClassName='link__active'>Favorites</NavLink></li>
        <li><NavLink exact to='/login' className='link' activeClassName='link__active'>Login</NavLink></li>
        <li><NavLink to='/not-exist' className='link' activeClassName='link__active'>Not existing route</NavLink></li>
      </ul>
    )
  }
}

export default withRouter(Sidebar)