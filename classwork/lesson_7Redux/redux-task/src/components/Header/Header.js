import React, { memo } from 'react';
import PropTypes from 'prop-types';
// import starIcon from '../../theme/icons/star.svg';
// import iconsFolder from '../../theme/icons/globo.svg';

const Header = props => {
	const { title, user } = props;
	const { name, age, email } = user;

	const hasEmail = !!email;
	// console.log('Star logo', starIcon);

	return (
		<>
			{!!title && <h2>{title}</h2>}
			{!!name && <div>{name}</div>}
			{!!age && <div>{age}</div>}
			{/* <img src='/logo192.png'></img> */}
			{/* <img src={starIcon}></img> */}
			<div>
				{/* {hasEmail ? email : 'No email'} */}
				{hasEmail && email}
				{!hasEmail && 'No email specified'}
			</div>
		</>
	);
};

Header.propTypes = {
	title: PropTypes.string,
	user: PropTypes.shape({
		name: PropTypes.string,
		age: PropTypes.number,
		email: PropTypes.string,
	}),
};

Header.defaultProps = {
	title: 'Mail Client',
	user: {
		name: 'Anonimous user',
	},
};

export default memo(Header);
