import React, { PureComponent } from 'react'
import ErrorPage from '../ErrorPage/ErrorPage';

class ErrorBoundary extends PureComponent {
  state = {
    hasError: false
  }

  componentDidCatch(error, errorInfo) {
    // axios.post('/error', errorInfo)
    // console.log(error)
    // console.log(errorInfo)

    // this.setState({hasError: true})
  }

  static getDerivedStateFromError(error) {
    return { hasError: true }
  }

  render() {
    const { children } = this.props;
    const { hasError } = this.state;

    if (hasError) {
      return <ErrorPage />
    }

    return children
  }
}

export default ErrorBoundary