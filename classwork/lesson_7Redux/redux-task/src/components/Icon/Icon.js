import React from 'react';
import * as Icons from '../../theme/icons';

function Icon(props) {
	const { type, color, filled } = props;

	const iconJsx = Icons[type];

	if (!iconJsx) {
		return null;
	}

	return <div className={'icon'}>{iconJsx(color, filled)}</div>;
}

export default Icon;
