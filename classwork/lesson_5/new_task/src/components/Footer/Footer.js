import React, { PureComponent } from 'react'

class Footer extends PureComponent {
  render() {
    // console.log('Rendering Footer.js')

    return (
      <h2>&copy; Dan.IT Education 2020</h2>
    )
  }
}

export default Footer