import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

class Header extends PureComponent {
	render() {
		const { title, user } = this.props;
		const { name, age, email } = user;

		// console.log('Header.js', this)

		const hasEmail = !!email;

		return (
			<>
				{!!title && <h2>{title}</h2>}
				{!!name && <div>{name}</div>}
				{!!age && <div>{age}</div>}
				<div>
					{/* {hasEmail ? email : 'No email'} */}
					{hasEmail && email}
					{!hasEmail && 'No email specified'}
				</div>
			</>
		);
	}
}

Header.propTypes = {
	title: PropTypes.string,
	user: PropTypes.shape({
		name: PropTypes.string.isRequired,
		age: PropTypes.number,
		email: PropTypes.string,
	}),
};

Header.defaultProps = {
	title: 'Mail Client',
	user: {
		name: 'Anonimous user',
	},
};

export default Header;
