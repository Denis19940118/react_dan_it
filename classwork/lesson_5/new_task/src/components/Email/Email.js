import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {Link, withRouter} from 'react-router-dom';

class Email extends PureComponent {
  render() {
    const { showFull, email } = this.props
    const { topic, body, id } = email;

    return (
      <div>
        <Link to={`/emails/${id}`}>{topic}</Link>
        {showFull && <div>{body}</div>}
        {showFull && <div><button onClick={this.goBack}>Go back</button></div>}
      </div>
    )
  }

  goBack = () => {
    this.props.history.goBack();
  }

  // static propTypes = {}
}

//object, number, string, bool, func, array, symbol
// isRequired
// instanceOf(), shape(), exact(), oneOfType([])
Email.propTypes = {
  email: PropTypes.shape({
    id: PropTypes.number.isRequired,
    topic: PropTypes.string.isRequired,
    body: PropTypes.string
  }).isRequired
}

export default withRouter(Email)