import React, { PureComponent } from 'react'
import PropTypes from 'prop-types';

class Button extends PureComponent {
  render() {
    const { 
      children, onClick, type, size, color, backgroundColor, fontSize, className
    } = this.props;
 
    // const buttonClasses = `${className} ${size ? `size--${size}` : ''}`
 
    const style = {
      width: size,
      color,
      backgroundColor,
      fontSize
    }

    return (
      <button className={className} style={style} type={type} onClick={onClick}>{children}</button>
    )
  }
}

Button.propTypes = {
  children: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  type: PropTypes.string,
  size: PropTypes.number,
  color: PropTypes.string,
  backgroundColor: PropTypes.string,
  fontSize: PropTypes.number,
  className: PropTypes.string
}

Button.defaultProps = {
  onClick: undefined,
  type: 'button',
  size: 150,
  color: 'white',
  backgroundColor: 'black',
  fontSize: 18,
  className: ''
}

export default Button