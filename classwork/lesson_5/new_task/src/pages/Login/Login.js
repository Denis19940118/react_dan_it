import React, { useRef } from 'react';
import { Redirect, withRouter } from 'react-router-dom';
import Loader from '../../components/Loader/Loader';

const Login = props => {
	const { isAuth, saveCurrentUser } = props;

	this.loginRef = useRef();
	this.passwordRef = useRef();

	loginUser = e => {
		e.preventDefault();

		const { saveCurrentUser, isAuth } = this.props;

		const email = loginRef.current.value;
		const password = passwordRef.current.value;

		saveCurrentUser({ email, password });
	};

	if (this.props.isAuth) {
		return <Redirect to="/" />;
	}
	return (
		<form className="login" onSubmit={this.loginUser}>
			<div>
				<label>
					Email
					<input type="email" placeholder="Enter email" ref={this.loginRef} />
				</label>
			</div>
			<div>
				<label>
					Password
					<input
						type="password"
						placeholder="Enter password"
						ref={this.passwordRef}
					/>
				</label>
			</div>
			<div>
				<button type="submit">Submit</button>
			</div>
		</form>
	);
};
export default withRouter(Login);
