import React, { PureComponent } from 'react';
import Email from '../../components/Email/Email';

class Inbox extends PureComponent {
  render() {
    const { emails } = this.props;

    const emailCards = emails.map(e => <Email key={e.id} email={e} />)

    return (
      <div>
        {emailCards}
      </div>
    )
  }
}

export default Inbox