import React, { useRef } from 'react';
import './App.scss';
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import Loader from './components/Loader/Loader';
import axios from 'axios';
import Sidebar from './components/Sidebar/Sidebar';
import AppRoutes from './routes/AppRoutes';

const App = props => {
	// constructor (props) {
	//   super(props)

	//   console.log('constructor in App.js')
	// }

	const [currentUser, setCurrentUser] = useState(underfind);
	const [title, setTitle] = useState('Hello world');
	const [emails, setEmails] = useState([]);
	const [isLoading, setIsLoading] = useState(true);

	// saveCurrentUser = user => {
	// 	this.setState({ currentUser: user });
	// };

	useEffect(() => {
		setTimeout(() => {
			axios('/api/emails').then(res => {
				setEmails(res.data);
				setIsLoading(false);
			});
		}, 1000);

		return () => {
			// componentWillUnmount()
		};
	}, []);

	// console.log('App.js', this)

	return (
		<div className="App">
			<Header title={title} user={currentUser} />
			{/* <Sidebar /> */}
			{isLoading && <Loader />}
			{!isLoading && (
				<AppRoutes
					emails={emails}
					saveCurrentUser={this.saveCurrentUser}
					currentUser={currentUser}
				/>
			)}
			<Footer />
		</div>
	);
};

incrementAge = () => {
	const { currentUser } = this.state;
	const { age } = currentUser;

	this.setState({ currentUser: { ...currentUser, age: age + 1 } });
};

updateTitle = () => {
	const { title } = this.state;

	this.setState({ title: 'New title' });
};

export default App;
