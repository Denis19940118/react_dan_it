import React from 'react';
import { Redirect, Route } from 'react-router-dom';

const ProtectedRoute = ({isAuth, ...rest}) => {
  return isAuth ? <Route {...rest} /> : <Redirect to='/login' />
}

export default ProtectedRoute;