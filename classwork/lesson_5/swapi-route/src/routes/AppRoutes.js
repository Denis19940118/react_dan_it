import React, { PureComponent } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import FilmDetails from '../components/FilmDetails/FilmDetails'
import Films from '../components/Films/Films'
import Login from '../components/Login/Login';
import ProtectedRoute from './ProtectedRoute';

class AppRoutes extends PureComponent {
  render() {
    const {user, saveUser} = this.props;

    const isAuth = !!user

    return (
      <Switch>
        <Route exact path='/login'><Login isAuth={isAuth} saveUser={saveUser} /></Route>
        <Redirect exact from='/' to='/films' />
        <ProtectedRoute isAuth={isAuth} exact path='/films' component={Films} />
        <ProtectedRoute isAuth={isAuth} exact path='/films/:filmId' component={FilmDetails} />
      </Switch>
    )
  }
}



export default AppRoutes