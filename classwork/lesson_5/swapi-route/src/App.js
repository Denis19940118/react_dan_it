import React, { Component } from 'react';
import './App.css';
import Header from './components/Header/Header';
import AppRoutes from './routes/AppRoutes';

class App extends Component {
  state = {
    user: JSON.parse(localStorage.getItem('user'))
  }

  // componentDidMount() {
  //   axios.post('/users/current', localStorage.getItem('token'))
  //     .then(res => this.setState({ user: res.data, isLoading: false }))
  // }

  saveUser = (user) => {
    this.setState({ user })
    localStorage.setItem('user', JSON.stringify(user))
  }

  logout = () => {
    localStorage.removeItem('user')
    this.setState({ user: null })
  }

  render() {
    const { user } = this.state;

    return (
      <>
        <Header user={user} logout={this.logout} />
        <AppRoutes user={user} saveUser={this.saveUser} />
      </>
    );
  }
}

export default App;
