import React, { PureComponent } from 'react';
import { Redirect, withRouter } from "react-router-dom"
import './Login.scss';

class Login extends PureComponent {
  loginRef = React.createRef()
  passwordRef = React.createRef()

  loginUser = (e) => {
    e.preventDefault();
    const { saveUser } = this.props;

    const email = this.loginRef.current.value;
    const password = this.passwordRef.current.value;

    saveUser({ email, password })
  }

  render() {
    const { isAuth } = this.props

    if (isAuth) {
      return <Redirect to="/" />
    }

    return (
      <form className='login' onSubmit={this.loginUser}>
        <div>
          <label>Email
            <input type='text' placeholder='email' ref={this.loginRef} />
          </label>
        </div>
        <div>
          <label>Password
            <input type='password' placeholder='password' ref={this.passwordRef} />
          </label>
        </div>
        <div>
          <button type='submit'>Submit</button>
        </div>
      </form>
    )
  }
}

export default withRouter(Login)