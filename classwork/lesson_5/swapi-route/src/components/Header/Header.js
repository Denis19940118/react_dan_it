import React, { PureComponent } from 'react'

class Header extends PureComponent {
  render() {
    const { user, logout } = this.props;
    return (
      <div className='header'>
        {!!user && user.email}
        {!!user && <button onClick={logout}>Log out</button>}
      </div>
    )
  }
}

export default Header