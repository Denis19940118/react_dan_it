const express = require('express');

const app = express();

const port = process.env.PORT || 8085;

const emails = [
	{ id: 1, topic: 'Email 1', body: 'Lorem ipsum Email 1' },
	{ id: 2, topic: 'Email 2', body: 'Lorem ipsum Email 2' },
	{ id: 3, topic: 'Email 3', body: 'Lorem ipsum Email 3' },
];

app.get('/api/emails', (req, res) => {
	res.send(emails);
});

app.get('/api/emails/:emailsId', (req, res) => {
	const { emailsId } = req.params;
	res.send(emails.find(e => e.id === +emailsId));
});

app.listen(port, () => {
	console.log(`Server started on port ${port} `);
});
