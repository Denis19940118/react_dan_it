import React, { Fragment, PureComponent } from 'react';
import Button from '../Button/Button';

// class Numbers extends PureComponent {
// 	render() {
// 		const { numbers, deleteNum } = this.props;

// 		const numberItems = numbers.map((number, index) => (
// 			<li key={index}>
// 				{number} <Button title="X" onClick={() => deleteNum(index)} />
// 			</li>
// 		));

// 		return <ul>{numberItems}</ul>;
// 	}
// }

// export default Numbers;

class Numbers extends PureComponent {
	render() {
		const { numbers, deleteNum } = this.props;

		const numberItems = numbers.map((number, index) => (
			// console.log("Numbers -> render -> index", index)
			<li key={index}>
				{number} <Button title="X" onClick={() => deleteNum(index)} />
			</li>
		));

		return <ul>{numberItems}</ul>;
	}
}
export default Numbers;
