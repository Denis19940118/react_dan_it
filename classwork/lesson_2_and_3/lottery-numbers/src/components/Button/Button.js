import React, { Fragment, PureComponent } from 'react';

class Button extends PureComponent {
	render() {
		const { title, onClick } = this.props;

		return (
			<Fragment>
				<button onClick={onClick}> {title}</button>
			</Fragment>
		);
	}
}

export default Button;
