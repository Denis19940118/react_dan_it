import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Button from './components/Button/Button';
import Numbers from './components/Numbers/Numbers';

class App extends Component {
	state = {
		numbers: [],
	};

	render() {
		const { numbers } = this.state;

		return (
			<div className="App">
				<Button title="Generate" onClick={this.clickRandom} />
				<Numbers numbers={numbers} deleteNum={this.deleteNum} />
			</div>
		);
	}

	clickRandom = () => {
		const { numbers } = this.state;
		const number = Math.ceil(Math.random() * 36);
		this.setState({ numbers: [...numbers, number] });
	};

	deleteNum = index => {
		console.log('App -> index', index);
		const { numbers } = this.state;
		this.setState({ numbers: numbers.filter((n, i) => i !== index) });
	};
}

export default App;
