import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Loader from './components/Loader/Loader';
import Body from './components/Body/Body';
import Button from './components/Button/Button';
import axios from 'axios';
import Films from './components/Films/Films';

class App extends Component {
	state = {
		isLoading: true,
		films: [],
	};

	componentDidMount() {
		// const { isLoading, films } = this.state;
		// fetch('https://ajax.test-danit.com/api/swapi/films')
		// 	.then(response => response.json())
		// 	.then(items => {
		// 		console.log(items);
		// 		this.setState({
		// 			films: items,
		// 			isLoading: false,
		// 		});
		// 	});
		axios('https://ajax.test-danit.com/api/swapi/films')
			// .then(console.log)
			.then(item => {
				this.setState({
					films: item.data,
					isLoading: false,
				});
			});
	}

	render() {
		const { isLoading, films } = this.state;

		if (isLoading) {
			return <Loader />;
		}

		const filmItems = films.map(film => <Films key={film.id} film={film} />);

		return (
			<div className="App">
				<ol>{filmItems}</ol>
				{/* <Films films={films} /> */}
			</div>
		);
	}
}
export default App;
