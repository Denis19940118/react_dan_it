import React, { PureComponent } from 'react';

class Films extends PureComponent {
	state = {
		expanded: false,
	};

	render() {
		const { film } = this.props;
		const { expanded } = this.state;
		return (
			<li>
				<div>
					{film.name}
					<button onClick={this.showDetails}>More</button>
					{expanded && <div>Details</div>}
				</div>
			</li>
		);
	}

	showDetails = () => {
		const { expanded } = this.state;
		this.setState({ expanded: true });
	};
}

export default Films;
