import React, { PureComponent } from 'react'
import Characters from '../Characters/Characters';

class Film extends PureComponent {
  state = {
    expanded: false
  }

  render() {
    const { film } = this.props;
    const { expanded } = this.state;

    return (
      <li>
        <div>
          {film.name} <button onClick={this.showDetails}>Детальнее</button>
          {
            expanded && (
              <div>
                <div>Episode ID: {film.episodeId}</div>
                <div>Opening crawl: {film.openingCrawl}</div>
                <div>
                  <Characters characters={film.characters} />
                </div>
              </div>
            )
          }
        </div>
      </li>
    )
  }

  showDetails = () => {
    this.setState({ expanded: true })
  }
}

export default Film