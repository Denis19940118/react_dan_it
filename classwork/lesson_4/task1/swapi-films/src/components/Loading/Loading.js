import React, { PureComponent } from 'react'

class Loading extends PureComponent {
  render() {
    return 'Loading...'
  }
}

export default Loading