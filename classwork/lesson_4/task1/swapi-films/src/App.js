import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import Loading from './components/Loading/Loading';
import Film from './components/Film/Film';

class App extends Component {
  state = {
    films: [],
    isLoading: true
  }

  componentDidMount() {
    axios('https://ajax.test-danit.com/api/swapi/films')
      .then(res => this.setState({ films: res.data, isLoading: false }))
    // .then(console.log)
  }

  render() {
    const { isLoading, films } = this.state;

    if (isLoading) {
      return <Loading />
    }

    const filmItems = films.map(film => <Film key={film.id} film={film} />)

    return (
      <div>
        <ol>
          {filmItems}
        </ol>
      </div>
    );
  }
}

export default App;
