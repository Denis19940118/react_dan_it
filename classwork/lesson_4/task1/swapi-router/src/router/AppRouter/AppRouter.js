import React, { PureComponent } from 'react';
import { Redirect, Route } from 'react-router-dom';
import FilmDetails from '../components/FilmDetails/FilmDetails';
import Films from '../components/Films/Films';

class AppRoutes extends PureComponent {
	render() {
		return (
			<>
				<Redirect exact from="/" to="/films" />
				<Route exact path="/films" component={Films} />
				<Route exact path="/films/:filmId" component={FilmDetails} />
			</>
		);
	}
}

export default AppRoutes;
