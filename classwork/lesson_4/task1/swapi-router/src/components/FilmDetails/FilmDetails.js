import React, { PureComponent } from 'react';
import axios from 'axios';
import Loading from '../Loading/Loading';

class FilmDetails extends PureComponent {
	state = {
		film: undefined,
		isLoading: true,
	};

	componentDidMount() {
		const { filmId } = this.props.match.params;

		axios(`https://ajax.test-danit.com/api/swapi/films/${filmId}`).then(res =>
			this.setState({ film: res.data, isLoading: false })
		);
	}

	render() {
		const { isLoading, film } = this.state;

		if (isLoading) {
			return <Loading />;
		}

		return (
			<div>
				<div>Name: {film.name}</div>
				<div>Episode ID: {film.episodeId}</div>
				<div>Opening crawl: {film.openingCrawl}</div>
				<div>
					<button onClick={this.goToFilms}>Go to film list</button>
				</div>
			</div>
		);
	}

	goToFilms = () => {
		this.props.history.push('/films');
	};
}

export default FilmDetails;
