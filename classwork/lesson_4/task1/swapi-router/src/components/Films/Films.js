import React, { PureComponent } from 'react';
import axios from 'axios';
import Loading from '../Loading/Loading';
import Film from '../Film/Film';

class Films extends PureComponent {
	state = {
		films: [],
		isLoading: true,
	};

	componentDidMount() {
		axios('https://ajax.test-danit.com/api/swapi/films').then(res =>
			this.setState({ films: res.data, isLoading: false })
		);
	}

	render() {
		const { isLoading, films } = this.state;
		const { history } = this.props;

		if (isLoading) {
			return <Loading />;
		}

		const filmItems = films.map(film => (
			<Film key={film.id} film={film} history={history} />
		));

		return (
			<div>
				<ol>{filmItems}</ol>
			</div>
		);
	}
}

export default Films;
