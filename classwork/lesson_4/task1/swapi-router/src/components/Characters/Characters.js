// import React, { PureComponent } from 'react'
// import axios from 'axios'
// import Loading from '../Loading/Loading';

// class Characters extends PureComponent {
//   state = {
//     loadedCharacters: [],
//     isLoading: true
//   }

//   componentDidMount() {
//     const { characters } = this.props;

//     const characterRequests = characters.map(c => axios(c))

//     Promise.all(characterRequests).then(res => {
//       this.setState({ isLoading: false, loadedCharacters: res.map(r => r.data) })
//     })
//   }

//   render() {
//     const { isLoading, loadedCharacters } = this.state;

//     if (isLoading) {
//       return <Loading />
//     }

//     const characterItems = loadedCharacters.map(c => <li key={c.id}>{c.name}</li>)

//     return (
//       <ul>
//         {characterItems}
//       </ul>
//     )
//   }
// }

// export default Characters
