import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';

class Film extends PureComponent {
	render() {
		const { film } = this.props;

		return (
			<li>
				<div>
					{film.name} <Link to={`/films/${film.id}`}>Детальнее</Link>
				</div>
			</li>
		);
	}
}

export default Film;
