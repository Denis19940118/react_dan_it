import React, { PureComponent } from 'react';
import { NavLink } from 'react-router-dom';
import Characters from '../Characters/Characters';
import { withRouter } from 'react-router';
import axios from 'axios';

class Film extends PureComponent {
	// state = {
	// 	film: {},
	// };

	// componentDidMount() {
	// 	const id = this.props.match.params.id;
	// 	axios(`https://ajax.test-danit.com/api/swapi/films/${id}`).then(res =>
	// 		this.setState({ film: res.data, isLoading: false })
	// 	);
	// }

	render() {
		const { film } = this.props;
		// const { films } = this.props;
		// const { film } = this.state;
		// console.log('Film -> render -> film', film);

		return (
			<li>
				<div>
					{film.name} <NavLink to={`/${film.id}`}>Детальнее</NavLink>
				</div>
			</li>
			// <li>
			// 	<div>
			// 		<div>Episode Name: {film.name}</div>
			// 		<div>Episode ID: {film.episodeId}</div>
			// 		<div>Opening crawl: {film.openingCrawl}</div>
			// 		<div>
			// 			Characters: {film.characters}
			// 			<Characters characters={film.characters} />
			// 		</div>
			// 	</div>
			// </li>
		);
	}

	// showDetails = () => {
	// 	this.setState({ expanded: true });
	// };
}

export default withRouter(Film);
