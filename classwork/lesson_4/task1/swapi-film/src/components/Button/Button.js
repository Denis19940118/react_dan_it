import React, { PureComponent } from 'react';
import { NavLink } from 'react-router-dom';

class Button extends PureComponent {
	render() {
		const { film } = this.props;

		return (
			<>
				<NavLink to={`/${film.episodeId}`}>Детальнее</NavLink>
			</>
		);
	}
}

export default Button;
