import React, { PureComponent } from 'react';
import axios from 'axios';
import Loading from '../Loading/Loading';

class Characters extends PureComponent {
	state = {
		film: {},
		isLoading: true,
	};

	componentDidMount() {
		const { filmId } = this.props.match.params;

		axios(`https://ajax.test-danit.com/api/swapi/films/${filmId}`).thin(res => {
			this.setState({ film: res.data, isLoading: false });
		});

		// Promise.all(characterRequests).then(res => {
		// 	this.setState({
		// 		isLoading: false,
		// 		loadedCharacters: res.map(r => r.data),
		// 	});
		// });
	}

	render() {
		const { isLoading, film } = this.state;

		if (isLoading) {
			return <Loading />;
		}

		// const characterItems = loadedCharacters.map(c => (
		// 	<li key={c.id}>{c.name}</li>
		// ));

		return (
			<li>
				<div>
					<div>Episode Name: {film.name}</div>
					<div>Episode ID: {film.episodeId}</div>
					<div>Opening crawl: {film.openingCrawl}</div>
					<div>
						<button onClick={this.clicked}>Go on</button>
					</div>
				</div>
			</li>
		);
	}

	clicked = () => {
		this.props.history.push('/films');
	};
}

export default Characters;
