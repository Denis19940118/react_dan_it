import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import Loading from './components/Loading/Loading';
import Film from './components/Film/Film';
import { Route, Switch, NavLink } from 'react-router-dom';
import Button from './components/Button/Button';

class App extends Component {
	state = {
		films: [],
		isLoading: true,
		filmId: undefined,
	};

	componentDidMount() {
		axios('https://ajax.test-danit.com/api/swapi/films').then(res =>
			this.setState({ films: res.data, isLoading: false })
		);
	}

	render() {
		const { isLoading, films } = this.state;

		if (isLoading) {
			return <Loading />;
		}

		const filmItems = films.map(film => (
			<div>
				{film.name} <Button film={film} />
			</div>
		));

		return (
			<div>
				<NavLink to="/">Home</NavLink>
				<Switch>
					<Route exact path="/">
						<ol>{filmItems}</ol>
					</Route>
					<Route path="/:id">
						<Film films={films} />
					</Route>
				</Switch>
			</div>
		);
	}
}

export default App;
