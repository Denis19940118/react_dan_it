import React, { PureComponent } from 'react';
import { Redirect, Route } from 'react-router-dom';
import Characters from '../components/Characters/Characters';
import Films from '../components/Films/Films';

export default class AppRouter extends PureComponent {
	render() {
		return (
			<>
				<Redirect exact from="/" to="/films" />
				<Route exact from="/films" component={Films} />
				<Route exact from="/films/:filmId" component={Characters} />
			</>
		);
	}
}

export default AppRoutes;
