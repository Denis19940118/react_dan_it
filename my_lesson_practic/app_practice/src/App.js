import React from 'react';
import logo from './logo.svg';
import './App.css';
import Appi from './components/Appi/Appi';

function App() {
	return (
		<div className="App">
			<Appi />
		</div>
	);
}

export default App;
