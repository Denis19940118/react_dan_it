import React, { Component, PureComponent, useState } from 'react';
import articles from '../fixtures';
import ArticleList from '../ArticleList/ArticleList';
import 'bootstrap/dist/css/bootstrap.css';

class Appi extends PureComponent {
	state = { reverted: false };

	render() {
		console.log('Appi -> state', this.state);

		return (
			<div className="container">
				<div className="jumbotron">
					<h1> App name</h1>
					<button className="btn" onClick={this.revert}>
						Revert
					</button>
				</div>

				<ArticleList
					articles={this.state.reverted ? articles.slice().reverse() : articles}
				/>
			</div>
		);
	}
	revert = () => {
		const { reverted } = this.state;
		this.setState({ reverted: !reverted });
	};
}

export default Appi;
