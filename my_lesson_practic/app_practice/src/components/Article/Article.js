import React, { PureComponent } from 'react';

class Article extends PureComponent {
	state = {
		count: 0,
	};

	// componentWillMount() {}

	shouldComponentUpdate(nextProps, nextState) {
		return this.state.isOpen !== nextState.isOpen;
	}

	componentWillReceiveProps(nextProps) {
		const { defaultOpen } = this.props;
		if (nextProps.defaultOpen !== this.props.defaultOpen) {
			this.setState({
				isOpen: nextProps.defaultOpen,
			});
		}
	}

	render() {
		const { article, isOpen } = this.props;
		const style = { width: '50%' };

		const body = isOpen && (
			<section className="card-text">{article.text}</section>
		);

		return (
			<div className="card mx-auto" style={style}>
				<div className="card-header">
					<h2 onClick={this.incrementCounter}>
						{article.title}
						clicked {this.state.count}
						<button
							className="btn btn-primary btn-lg float-right"
							onClick={this.handleClick}
						>
							{isOpen ? 'close' : 'open'}
						</button>
					</h2>
				</div>
				<div className="card-body">
					<h6 className="card-subtitle text-muted">
						create date: {new Date(article.date).toDateString()}
					</h6>
					{body}
				</div>
			</div>
		);
	}

	incrementCounter = () => {
		this.setState({
			count: this.state.count + 1,
		});
	};

	handleClick = () => {
		// this.setState({
		// 	isOpen: !this.state.isOpen,
		// });
	};
}

// const handleClick = () => {
//     console.log('_____', 'clicked')
// }

// const Article = props => {
// 	const { article } = props;
// 	const body = <section>{article.text}</section>;

// 	return (
// 		<div>
// 			<h2>{article.title}
//             <button onClick={handleClick}>close</button>
//             </h2>
// 			{body}
// 			<h3>create date: {new Date(article.date).toDateString()}</h3>
// 		</div>
// 	);
// };

export default Article;
