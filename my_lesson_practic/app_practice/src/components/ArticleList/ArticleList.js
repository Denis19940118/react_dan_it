import React, { PureComponent } from 'react';
import Article from '../Article/Article';
import './ArticleList.scss';

class ArticleList extends PureComponent {
	state = {
		openArticleId: null,
	};
	render() {
		const { openArticleId } = this.state;
		const { articles } = this.props;

		const arrArtical = articles.map((article, index) => (
			<li
				key={article.id}
				className="article-list__li"
				onClick={this.handleClick(article.id)}
			>
				<Article article={article} defaultOpen={openArticleId === article.id} />
			</li>
		));

		return <ul>{arrArtical}</ul>;
	}

	handleClick = openArticleId => {
		this.setState({
			openArticleId:
				this.state.openArticleId === openArticleId ? null : openArticleId,
		});
	};
}

export default ArticleList;
