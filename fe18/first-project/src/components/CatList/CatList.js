import React, { Component } from 'react';
import Cat from '../Cat';

class CatList extends Component {
	render() {
		const cats = [
			{
				id: 2451,
				name: 'beautyfull',
				info: `LoСейчас предлагаю рассмотреть установку заголовка, так как загрузка
						фото требует выполнения асинхронного запроса, а чтобы добраться до
						этого, мы должны рассмотреть несколько интересных вещей. К тому же,
						установка заголовка отлично показывает на простом примере, как
						вращаются данные внутри redux-приложения, а именно`,
			},
			{
				id: 2452,
				name: 'beautyfull',
				info: `LoСейчас предлагаю рассмотреть установку заголовка, так как загрузка
						фото требует выполнения асинхронного запроса, а чтобы добраться до
						этого, мы должны рассмотреть несколько интересных вещей. К тому же,
						установка заголовка отлично показывает на простом примере, как
						вращаются данные внутри redux-приложения, а именно`,
			},
			{
				id: 2453,
				name: 'Bigger',
				info: `а отлично показывает на простом примере, как
						вращаются данные внутри redux-приложения, а именно`,
			},
			{
				id: 2454,
				name: 'beauty',
				info: `чно показывает на простом примере, как вращаются данные внутри
						redux-приложения, а именно`,
			},
			{
				id: 2455,
				name: 'full',
				info: `именно`,
			},
		];

		const catsItems = cats.map(singleCat => (
			<Cat key={singleCat.id} name={singleCat.name}>
				<p>{singleCat.info}</p>
			</Cat>
		));
		return <>{catsItems}</>;
	}
}
export default CatList;
