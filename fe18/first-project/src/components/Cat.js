import React, { Component } from 'react';

class Cat extends Component {
	state = {
		isOpen: false,
	};

	render() {
		const { name, children } = this.props;
		const { isOpen } = this.state;
		console.log(name);
		return (
			<div className="cat__item">
				<h3 className="cat__name">{name}</h3>
				<button
					className="cat__info-toggle"
					onClick={() => this.setState({ isOpen: !isOpen })}
				>
					Details
				</button>
				{isOpen && <p>{children}</p>}
				<div className="main__img-wrapper">
					<img src="./img/base.jpg" alt="cat" className="cat__img"></img>
				</div>
			</div>
		);
	}
}
export default Cat;
