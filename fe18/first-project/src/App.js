import './App.scss';
import { useEffect, useState } from 'react';
// import Cat from './components/Cat';
import CatList from './components/CatList/CatList';

function App() {
	const [isOpened, setIsOpened] = useState(false);
	// useEffect(() => !setIsOpened(false), [isOpened]);
	return (
		<div className="container">
			<button onClick={() => setIsOpened(!isOpened)}>More cats</button>

			<h1 className="main-title"> Trust me</h1>
			{isOpened && <CatList />}
		</div>
	);
}

export default App;
