import React, { Component } from 'react';
import Button from './components/Buttton/Button';
import Modal from './components/Modal/Modal';
import { modal } from './components/fields';

class App extends Component {
	render() {
		const allModal = modal.map(item => <Modal key={item.id} {...item} />);
		return (
			<div className="App">
				{allModal}
				{/* <Modal
					id="1"
					isOpen={this.state.isOpen}
					handlerModal={this.handlerModal}
					textBtn="open first modal"
					textModal="first modal"
					idBtn="1"
					actions={[<Button text="Ok" />, <Button text="Cancel" />]}
				/>
				<Modal
					id="2"
					idBtn="2"
					isOpen={this.state.isOpen}
					handlerModal={this.handlerModal}
					textBtn="open second modal"
					textModal="second modal"
					actions={[<Button text="Ok" />, <Button text="Cancel" />]}
				/>
				<Modal textBtn="open third modal" textModal="three modal" /> */}
			</div>
		);
	}
}
export default App;
