import React, { Component } from 'react';
import Button from '../Buttton/Button';

export default class Modal extends Component {
	state = {
		isOpen: false,
	};

	handlerModal = () => {
		this.setState({ isOpen: !this.state.isOpen });
	};

	render() {
		const { textBtn, textModal } = this.props;
		const { isOpen } = this.state;
		const { handlerModal } = this;

		return (
			<div>
				<Button text={textBtn} handle={handlerModal} />
				{isOpen && (
					<div>
						<button onClick={handlerModal}>x</button>
						<h1>{textModal}</h1>
						<Button text="Ok" handle={handlerModal} />
					</div>
				)}
			</div>
		);
	}
}
