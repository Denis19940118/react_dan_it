import React, { Component } from 'react';

export default class Button extends Component {
	render() {
		const { handle, text } = this.props;
		return <button onClick={handle}>{text}</button>;
	}
}
